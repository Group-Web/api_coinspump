<?php

namespace AppBundle\Payments;

use AppBundle\Services\Helper;

class UpholdPayment {

    private $BaseUrl;
    private $ClientId;
    private $ClientSecret;
    private $PaymentDestination;
    private $Helper;

    public function __construct() {
        $this->BaseUrl = 'https://api-sandbox.uphold.com/';
//        $this->BaseUrl = 'https://api.uphold.com/';
        $this->ClientId = 'ae23038c10a7a1c0c209ed05784f28015e2fcbfe';
        $this->ClientSecret = '532845a9ef30270e7900751adc06f848a6382684';
        $this->PaymentDestination = "betoprueba";
        $this->Helper = new Helper();
    }

    public function getToken($codeValdiate, $stateValidate = true) {
        // Armar informacion para enviar
        $url = $this->BaseUrl . "oauth2/token";
        $data = "code=" . $codeValdiate . "&grant_type=authorization_code";
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        $auth = $this->ClientId . ":" . $this->ClientSecret;

        // Sacar el token de lo que me retorna Uphoald
        $values = json_decode($this->Helper->ApiCall("POST", $url, $auth, $headers, $data));

        // Retorna Servicio
        return $values->access_token;
    }

    public function getAccountsUser($token) {
        // Armar informacion para enviar
        $url = $this->BaseUrl . "v0/me/cards";
        $headers[] = "Authorization: Bearer " . $token;
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        $auth = null;
        $data = null;

        // Sacar el token de lo que me retorna Uphoald
        $values = json_decode($this->Helper->ApiCall("GET", $url, $auth, $headers, $data));

        // retorna todo
        return $values;
    }

    public function setPayment($token, $OTP, $cuentaOrigin, $currency, $amount, $message) {
        // Armar informacion para enviar
        $url = $this->BaseUrl . "v0/me/cards/" . $cuentaOrigin . "/transactions?commit:true";
        $data = array(
            "denomination" => array("amount" => $amount, "currency" => $currency),
            "destination" => $this->PaymentDestination,
            "message" => $message,
        );
        $headers[] = "Authorization: Bearer " . $token;
        $headers[] = "OTP-Token: " . $OTP;
        $headers[] = "Content-Type: application/json";
        $auth = null;

        // Sacar el token de lo que me retorna Uphoald
        $values = json_decode($this->Helper->ApiCall("POST", $url, $auth, $headers, json_encode($data)));
        return $values;
    }

    public function setCommintPayment($token, $OTP, $cuentaOrigin, $idTransaction, $message) {
        // Armar informacion para enviar
        $url = $this->BaseUrl . "v0/me/cards/" . $cuentaOrigin . "/transactions/" . $idTransaction . "/commit";
        $data = array(
            "message" => $message
        );
        $headers[] = "Authorization: Bearer " . $token;
        $headers[] = "OTP-Token: " . $OTP;
        $headers[] = "Content-Type: application/json";

        // Sacar el token de lo que me retorna Uphoald
        $values = json_decode($this->Helper->ApiCall("POST", $url, null, $headers, json_encode($data)));
        return $values;
    }

    public function setCancelPayment($token, $OTP, $cuentaOrigin, $idTransaction) {
        // Armar informacion para enviar
        $url = $this->BaseUrl . "v0/me/cards/" . $cuentaOrigin . "/transactions/" . $idTransaction . "/cancel";
        $headers[] = "Authorization: Bearer " . $token;
        $headers[] = "OTP-Token: " . $OTP;
        $headers[] = "Content-Type: application/json";

        // Sacar el token de lo que me retorna Uphoald
        $values = json_decode($this->Helper->ApiCall("POST", $url, null, $headers, null));
        return $values;
    }

}
