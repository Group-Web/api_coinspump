<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Validation\ServicePaymentValidation;
use AppBundle\Services\Helper;
use AppBundle\Services\SendMail;
use AppBundle\Services\JwtAuth;
use DbBundle\Entity\TbCryptocurrency;
use DbBundle\Entity\TbServicePayment;
use DbBundle\Entity\TbPriceServicePayment;

class ServiceController extends Controller {

    private $Jwt;
    private $Helper;
    private $SendMail;

    public function __construct(JwtAuth $jwt_auth, Helper $helper, SendMail $sendMail) {
        $this->Jwt = $jwt_auth;
        $this->Helper = $helper;
        $this->SendMail = $sendMail;
    }

    public function indexAction(Request $request) {
        try {
            $db = $this->getDoctrine()->getManager();
            $repository = $db->getRepository('DbBundle:TbServicePayment');
            $service = $repository->findAll();

            $list = [];
            $price = [];
            foreach ($service as $service) {
                $priceService = $db->getRepository('DbBundle:TbPriceServicePayment')->findBy(['fkServicePayment' => $service->getIdServicePayment()]);
                $price = [];
                foreach ($priceService as $key => $prices) {
                    $price[] = [
                        'idPriceServicePayment' => $prices->getIdPriceServicePayment(),
                        'descriptionPriceServicePayment' => $prices->getDescriptionPriceServicePayment(),
                        'valuePriceServicePayment' => $prices->getValuePriceServicePayment(),
                        'cryptocurrency' => $prices->getFkCryptocurrency()->getIdCryptocurrency(),
                        'codeCryptocurrency' => $prices->getFkCryptocurrency()->getCodeCryptocurrency(),
                        'nameCryptocurrency' => $prices->getFkCryptocurrency()->getNameCryptocurrency(),
                    ];
                }
                $service_list = [
                    "idServicePayment" => $service->getIdServicePayment(),
                    "nameServicePayment" => $service->getNameServicePayment(),
                    "descriptionServicePayment" => $service->getDescriptionServicePayment(),
                    "timeServicePayment" => $service->getCostServicePayment(),
                    "priceServicePayment" => $price,
                ];
                $list[] = $service_list;
                unset($service_list);
            }
            return $this->json(array("message" => "Listado de Cryptocurrency", "data" => $list, "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function newAction(Request $request, \Swift_Mailer $mailer) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }

            // Valida si es un usuario valido
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);
            if ($session == false) {
                return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }

            // Es un Usuario Nuevo Web o por el Admin
            $validate = new ServicePaymentValidation();
            $datos = $validate->newAction($json);
            if (!empty($datos['error'])) {
                return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }

            // Conexion
            $db = $this->getDoctrine()->getManager();
            $db->getConnection()->beginTransaction();

            // Guadar Chat
            $servicePayment = new TbServicePayment();
            $servicePayment->setNameServicePayment($datos['value']['nameServicePayment']);
            $servicePayment->setDescriptionServicePayment($datos['value']['descriptionServicePayment']);
            //$servicePayment->setCostServicePayment(1);
            $db->persist($servicePayment);
            $db->flush();
            $servicePaymentId = $servicePayment->getIdServicePayment();

            // Guardar Pagos
            if ($datos['value']['priceServicePayment'] != null) {
                foreach ($datos['value']['priceServicePayment'] as $key => $valor) {
                    $priceServicePayment = new TbPriceServicePayment();
                    $priceServicePayment->setValuePriceServicePayment($valor["valuePriceServicePayment"]);
                    $priceServicePayment->setDescriptionPriceServicePayment($valor["descriptionPriceServicePayment"]);
                    $priceServicePayment->setCreatedPriceServicePayment(new \DateTime());
                    $priceServicePayment->setFkCryptocurrency($db->find(TbCryptocurrency::class, $valor["cryptocurrency"]));
                    $priceServicePayment->setFkServicePayment($servicePayment);
                    $db->persist($priceServicePayment);
                    $db->flush();
                }
            }
            $db->getConnection()->commit();
            return $this->json(array("message" => "Service Creado"), 200);
        } catch (Exception $e) {
            $db->getConnection()->rollback();
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EC001"), 400);
        }
    }

    public function editAction(Request $request, \Swift_Mailer $mailer) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }

            // Valida si es un usuario valido
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);
            if ($session == false) {
                 return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }

            // Es un Usuario Nuevo Web o por el Admin
            $validate = new ServicePaymentValidation();
            $datos = $validate->newAction($json);
            if (!empty($datos['error'])) {
                return $this->json(array("message" => $datos['error'], "code" => "EC002"), 400);
            }
            // Conexion
            $db = $this->getDoctrine()->getManager();
            $db->getConnection()->beginTransaction();
            $repository = $db->getRepository('DbBundle:TbServicePayment');
            $servicePayment = $repository->findOneBy(['idServicePayment' => $datos['value']['idServicePayment']]);

            if (!empty($servicePayment)) {
                // Guadar Service
                /* $servicePayment->setNameServicePayment($datos['value']['nameServicePayment']);
                  $servicePayment->setDescriptionServicePayment($datos['value']['descriptionServicePayment']);
                  $servicePayment->setCostServicePayment(0);
                  $db->persist($servicePayment);
                  $db->flush(); */
                $servicePaymentId = $servicePayment->getIdServicePayment();
            } else {
                return $this->json(array("message" => "El registro solicitado no existe", "code" => "EC002"), 400);
            }
            // Guardar Pagos
            if ($datos['value']['priceServicePayment'] != null) {
                $repository = $db->getRepository('DbBundle:TbPriceServicePayment');
                $priceServicePayment = $repository->findBy(['fkServicePayment' => $servicePaymentId]);
                foreach ($priceServicePayment as $key => $value) {
                    $db->remove($value);
                    $db->flush();
                }
                foreach ($datos['value']['priceServicePayment'] as $key => $valor) {
                    $priceServicePayment = new TbPriceServicePayment();
                    $priceServicePayment->setValuePriceServicePayment($valor["valuePriceServicePayment"]);
                    $priceServicePayment->setDescriptionPriceServicePayment($valor["descriptionPriceServicePayment"]);
                    $priceServicePayment->setCreatedPriceServicePayment(new \DateTime());
                    $priceServicePayment->setFkCryptocurrency($db->find(TbCryptocurrency::class, $valor["cryptocurrency"]));
                    $priceServicePayment->setFkServicePayment($servicePayment);
                    $db->persist($priceServicePayment);
                    $db->flush();
                }
            }
            $db->getConnection()->commit();
            return $this->json(array("message" => "Service Creado"), 200);
        } catch (Exception $e) {
            $db->getConnection()->rollback();
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EC001"), 400);
        }
    }

}
