<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Validation\WalletValidation;
use AppBundle\Validation\CryptocurrencyValidation;
use AppBundle\Validation\ProviderPaymentValidation;
use AppBundle\Services\Helper;
use AppBundle\Services\SendMail;
use AppBundle\Services\JwtAuth;

use DbBundle\Entity\TbTypePayment;
use DbBundle\Entity\TbCryptocurrency;
use DbBundle\Entity\TbProviderPayment;

class CryptocurrencyController extends Controller {

    private $Jwt;
    private $Helper;
    private $SendMail;

    public function __construct(JwtAuth $jwt_auth, Helper $helper, SendMail $sendMail) {
        $this->Jwt = $jwt_auth;
        $this->Helper = $helper;
        $this->SendMail = $sendMail;
    }

    public function indexAction(Request $request) {
        try {
            $db = $this->getDoctrine()->getManager();
            $repository = $db->getRepository('DbBundle:TbCryptocurrency');
            if ($request->get('_route') == 'cryptocurrency_list') {
                $cryptos = $repository->findAll();
            }else{
                $cryptos = $repository->findBy(array('statusCryptocurrency' => 1));
            }
            $list = [];
            foreach ($cryptos as $crypto) {
                $crypto_list = [
                    "idCryptocurrency" => $crypto->getIdCryptocurrency(),
                    "nameCryptocurrency" => $crypto->getNameCryptocurrency(),
                    "codeCryptocurrency" => $crypto->getCodeCryptocurrency(),
                    "isPaymentCryptocurrency" => $crypto->getIsPaymentCryptocurrency(),
                    "imageCryptocurrency" => $crypto->getImageCryptocurrency(),
                    "webCryptocurrency" => $crypto->getWebCryptocurrency(),
                    "createdCryptocurrency" => $crypto->getCreatedCryptocurrency(),
                    "statusCryptocurrency" => $crypto->getStatusCryptocurrency(),
                ];
                $list[] = $crypto_list;
                unset($crypto_list);
            }
            return $this->json(array("message" => "Listado de Cryptocurrency", "data" => $list, "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function createAction(Request $request) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }

            // Valida si es un usuario Valido Admin o de la web
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);

            // Es un Usuario Nuevo Web o por el Admin
            $validate = new CryptocurrencyValidation();
            if ($session != false) {
                $datos = $validate->newAction($json);
            }else{
                return $this->json(array("message" => 'No posee privilegios', "code" => "EU002"), 400);
            } 
            if (!empty($datos['error'])) {
                return $this->json(array("message" => $datos['error'], "code" => "EU002"), 400);
            }

            // Conexion
            $db = $this->getDoctrine()->getManager();

            //Instanciar Cryptocurrency
            $crypto = new TbCryptocurrency();
            $crypto->setNameCryptocurrency($datos['value']['nameCryptocurrency']);
            $crypto->setCodeCryptocurrency($datos['value']['codeCryptocurrency']);
            $crypto->setImageCryptocurrency($datos['value']['imageCryptocurrency']);
            $crypto->setWebCryptocurrency($datos['value']['webCryptocurrency']);
            $crypto->setCreatedCryptocurrency(new \DateTime());
            $crypto->setStatusCryptocurrency(1);
            $db->persist($crypto);
            $db->flush();

            return $this->json(array("message" => "Provider List", "data" => 'Cryptocurrency Creada', "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function editAction(Request $request) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }
            // Valida si es un usuario Valido Admin o de la web
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);
            // Es un Usuario Nuevo Web o por el Admin
            if ($session == false) {
                return $this->json(array("message" => 'No posee privilegios', "code" => "EU002"), 400);
            } 
            // Conexion
            $db = $this->getDoctrine()->getManager();
            $crypto = $db->getRepository('DbBundle:TbCryptocurrency')->findOneBy(["idCryptocurrency" => $json['id']]);
            if (empty($crypto)) {
                return $this->json(array("message" => 'No encontrado', "code" => "EU002"), 400);
            }

            // SI SE ENVIA EL PARAMETRO 'changeStatus' ES SOLO PARA CAMBIAR EL ESTATUS
            if (isset($json['changeStatus']) && $json['changeStatus']) {
                if ($crypto->getStatusCryptocurrency() == 1) {
                    $crypto->setStatusCryptocurrency(0);
                }else{
                    $crypto->setStatusCryptocurrency(1);
                }
            }else{ 
                $crypto->setCodeCryptocurrency($json['codeCryptocurrency']);
                $crypto->setWebCryptocurrency($json['webCryptocurrency']);
                $crypto->setImageCryptocurrency($json['imageCryptocurrency']);
            }



            $db->persist($crypto);
            $db->flush();

            return $this->json(array("message" => "Provider List", "data" => 'Currency Actualizado', "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function walletListAction(Request $request) {
        try {
            $db = $this->getDoctrine()->getManager();
            $repository = $db->getRepository('DbBundle:TbTypePayment');
            if ($request->get('_route') == 'admin_wallet_list') {
                $wallets = $repository->findAll();
            }else{
                $wallets = $repository->findBy(array('statusTypePayment' => 1));
            }
            
            $list = [];
            foreach ($wallets as $crypto) {
                $crypto_list = [
                    "idCryptocurrency" => $crypto->getIdTypePayment(),
                    "nameCryptocurrency" => $crypto->getNameTypePayment(),
                    "uriWallet" => $crypto->getUriTypePayment(),
                    "codeCryptocurrency" => $crypto->getFkCryptocurrency()->getCodeCryptocurrency(),
                    "isPaymentCryptocurrency" => 1,
                    "imageCryptocurrency" => $crypto->getFkCryptocurrency()->getImageCryptocurrency(),
                    "webCryptocurrency" => $crypto->getFkCryptocurrency()->getWebCryptocurrency(),
                    "createdCryptocurrency" => $crypto->getFkCryptocurrency()->getCreatedCryptocurrency(),
                    "statusCryptocurrency" => $crypto->getStatusTypePayment(),
                    "nameProvider" => $crypto->getFkProviderPayment()->getNameProviderPayment(),
                    "idProvider" => $crypto->getFkProviderPayment()->getIdProviderPayment(),
                ];
                $list[] = $crypto_list;
                unset($crypto_list);
            }
            return $this->json(array("message" => "Listado de Cryptocurrency", "data" => $list, "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function providerListAction(Request $request) {
        try {
            $db = $this->getDoctrine()->getManager();
            $repository = $db->getRepository('DbBundle:TbProviderPayment');
            $provider = $repository->findAll();
            $list = [];
            foreach ($provider as $provider) {
                $provider_list = [
                    "idProviderPayment" => $provider->getIdProviderPayment(),
                    "nameProviderPayment" => $provider->getNameProviderPayment(),
                    "descriptionProviderPayment" => $provider->getDescriptionProviderPayment(),
                    "uriProviderPayment" => $provider->getUriProviderPayment(),
                    "statusProviderPayment" => $provider->getStatusProviderPayment(),
                ];
                $list[] = $provider_list;
                unset($provider_list);
            }
            return $this->json(array("message" => "Provider List", "data" => $list, "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function providerAddAction(Request $request) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }

            // Valida si es un usuario Valido Admin o de la web
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);

            // Es un Usuario Nuevo Web o por el Admin
            $validate = new ProviderPaymentValidation();
            if ($session != false) {
                $datos = $validate->newAction($json);
            }else{
                return $this->json(array("message" => 'No posee privilegios', "code" => "EU002"), 400);
            } 
            if (!empty($datos['error'])) {
                return $this->json(array("message" => $datos['error'], "code" => "EU002"), 400);
            }

            // Conexion
            $db = $this->getDoctrine()->getManager();

            //Instanciar Cryptocurrency
            $provider = new TbProviderPayment();
            $provider->setNameProviderPayment($datos['value']['nameProviderPayment']);
            $provider->setDescriptionProviderPayment($datos['value']['descriptionProviderPayment']);
            $provider->setUriProviderPayment($datos['value']['urlProviderPayment']);
            $provider->setStatusProviderPayment(1);
            $db->persist($provider);
            $db->flush();

            return $this->json(array("message" => "Provider List", "data" => 'Cryptocurrency Creada', "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function walletCreateAction(Request $request) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }

            // Valida si es un usuario Valido Admin o de la web
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);

            // Es un Usuario Nuevo Web o por el Admin
            $validate = new WalletValidation();
            if ($session != false) {
                $datos = $validate->newAction($json);
            }else{
                return $this->json(array("message" => 'No posee privilegios', "code" => "EU002"), 400);
            } 
            if (!empty($datos['error'])) {
                return $this->json(array("message" => $datos['error'], "code" => "EU002"), 400);
            }

            // Conexion
            $db = $this->getDoctrine()->getManager();

            //Instanciar TypePayment
            $wallet = new TbTypePayment();
            $wallet->setNameTypePayment($datos['value']['nameWallet']);
            $wallet->setDescriptionTypePayment($datos['value']['descriptionWallet']);
            $wallet->setStatusTypePayment(1);
            $wallet->setUriTypePayment($datos['value']['uriWallet']);
            $wallet->setFkCryptocurrency($db->find(TbCryptocurrency::class, $datos['value']['cryptocurrencyWallet']));
            $wallet->setFkProviderPayment($db->find(TbProviderPayment::class, $datos['value']['providerWallet']));
            $db->persist($wallet);
            $db->flush();

            return $this->json(array("message" => "Provider List", "data" => true, "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function walletStatusAction(Request $request) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }
            // Valida si es un usuario Valido Admin o de la web
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);
            // Es un Usuario Nuevo Web o por el Admin
            if ($session == false) {
                return $this->json(array("message" => 'No posee privilegios', "code" => "EU002"), 400);
            } 
             // Conexion
            $db = $this->getDoctrine()->getManager();
            $wallet = $db->getRepository('DbBundle:TbTypePayment')->findOneBy(["idTypePayment" => $json['id']]);
            if (empty($wallet)) {
                return $this->json(array("message" => 'No encontrado', "code" => "EU002"), 400);
            }

            if ($wallet->getStatusTypePayment() == 1) {
                $wallet->setStatusTypePayment(0);
            }else{
                $wallet->setStatusTypePayment(1);
            }
            $db->persist($wallet);
            $db->flush();

            return $this->json(array("message" => "Provider List", "data" => 'Status Cambiado', "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    public function providerEditAction(Request $request) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }
            // Valida si es un usuario Valido Admin o de la web
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, true, true);
            // Es un Usuario Nuevo Web o por el Admin
            if ($session == false) {
                return $this->json(array("message" => 'No posee privilegios', "code" => "EU002"), 400);
            } 
            // Conexion
            $db = $this->getDoctrine()->getManager();
            $provider = $db->getRepository('DbBundle:TbProviderPayment')->findOneBy(["idProviderPayment" => $json['id']]);
            if (empty($provider)) {
                return $this->json(array("message" => 'No encontrado', "code" => "EU002"), 400);
            }

            // SI SE ENVIA EL PARAMETRO 'changeStatus' ES SOLO PARA CAMBIAR EL ESTATUS
            if (isset($json['changeStatus']) && $json['changeStatus']) {
                if ($provider->getStatusProviderPayment() == 1) {
                    $provider->setStatusProviderPayment(0);
                }else{
                    $provider->setStatusProviderPayment(1);
                }
            }else{ 
                $provider->setNameProviderPayment($json['nameProviderPayment']);
                $provider->setDescriptionProviderPayment($json['descriptionProviderPayment']);
                $provider->setUriProviderPayment($json['uriProviderPayment']);
            }

            $db->persist($provider);
            $db->flush();

            return $this->json(array("message" => "Provider", "data" => 'Provider Actualizado', "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }

    /*public function tableAction(Request $request) {
        try {

            $data = 'https://www.cryptocompare.com/api/data/coinlist/';

            $data = json_decode($data);
            $baseUrlImg = $data->BaseImageUrl;
            $baseUrlWeb = $data->BaseLinkUrl;
            $crypto; $i = 0;
            $db = $this->getDoctrine()->getManager();
            foreach ($data->Data as $key => $value) {
                $i++;
                $crypto = new TbCryptocurrency();
                $crypto->setNameCryptocurrency($value->CoinName);
                if (!empty($value->ImageUrl)) {
                    $crypto->setImageCryptocurrency($baseUrlImg.$value->ImageUrl);
                }
                $crypto->setWebCryptocurrency($baseUrlWeb.$value->Url);
                $crypto->setStatusCryptocurrency(1);
                $crypto->setCodeCryptocurrency($value->Name);
                $crypto->setIsPaymentCryptocurrency(0);
                $crypto->setCreatedCryptocurrency(new \DateTime());
                $db->persist($crypto);
            }
            $db->flush();

            return $this->json(array("message" => "Listado de Cryptocurrency", "data" => true, "code" => "SCR001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
        }
    }*/

}
