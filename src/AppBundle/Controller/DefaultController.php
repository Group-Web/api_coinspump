<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;

class DefaultController extends Controller {

   /**
    * @Route("/", name="homepage") 
    */
   public function indexAction(Request $request) {
      // replace this example code with whatever you need
      return $this->render('default/index.html.twig', [
                  'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
      ]);
   }

   // Listar Paises Activos
   public function countriesAction() {
      try {
         $db = $this->getDoctrine()->getManager();
         $repository = $db->getRepository('DbBundle:TbCountry');
         $countries = $repository->findBy(array('statusCountry' => 1));
         $list = [];
         foreach ($countries as $country) {
            $country_list = [
                "idCountry" => $country->getIdCountry(),
                "nameCountry" => $country->getNameCountry(),
                "codeCountry" => $country->getCodeCountry(),
            ];
            $list[] = $country_list;
            unset($country_list);
         }
         return $this->json(array("message" => "Listado de paises", "data" => $list, "code" => "SD001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "ED001"), 400);
      }
   }

   // Listar Generos Activos
   public function gendersAction() {
      try {

         $db = $this->getDoctrine()->getManager();
         $repository = $db->getRepository('DbBundle:TbGender');
         $genders = $repository->findBy(array('statusGender' => 1));
         $list = [];
         foreach ($genders as $gender) {
            $gender_list = [
                "idGender" => $gender->getIdGender(),
                "nameGender" => $gender->getNameGender(),
            ];
            $list[] = $gender_list;
            unset($gender_list);
         }
         return $this->json(array("message" => "Listado de generos", "data" => $list, "code" => "SD001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "ED001"), 400);
      }
   }

   // Listar Tipos de chat
   public function typeChatAction() {
      try {
         $db = $this->getDoctrine()->getManager();
         $repository = $db->getRepository('DbBundle:TbTypeChat');
         $type_chats = $repository->findBy(array('statusChat' => 1));
         $list = [];
         foreach ($type_chats as $type_chat) {
            $type_chat_list = [
                "idTypeChat" => $type_chat->getIdTypeChat(),
                "nameTypeChat" => $type_chat->getNameTypeChat(),
            ];
            $list[] = $type_chat_list;
            unset($type_chat_list);
         }
         return $this->json(array("message" => "Listado de tipos de chat", "data" => $list, "code" => "SD001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "ED001"), 400);
      }
   }

   // Listar Roles Activos
   public function rolesAction() {
      try {
         $db = $this->getDoctrine()->getManager();
         $repository = $db->getRepository('DbBundle:TbRole');
         $roles = $repository->findBy(array('statusRole' => 1));
         $list = [];
         foreach ($roles as $role) {
            $role_list = [
                "idRole" => $role->getIdRole(),
                "nameRole" => $role->getNameRole(),
            ];
            $list[] = $role_list;
            unset($role_list);
         }
         return $this->json(array("message" => "Listado de roles", "data" => $list, "code" => "SD001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "ED001"), 400);
      }
   }

   // DASHBOARD
   public function dashboardAction() {
      try {
         $list = [];
         $db = $this->getDoctrine()->getManager();

         // Usuarios
         $users = $db->getRepository('DbBundle:TbUser')->findAll();
         $list['totalUser'] = 0;
         $list['totalUserValid'] = 0;
         $list['totalUserByEmail'] = 0;
         foreach ($users as $user) {
            $list['totalUser'] ++;
            if ($user->getValidUser() == 1 && $user->getStatusUser() == 1) {
               $list['totalUserValid'] ++;
            } else if ($user->getValidUser() == 0) {
               $list['totalUserByEmail'] ++;
            }
         }
         $connection = $db->getConnection();
         $statement = $connection->prepare("SELECT c.id_country, c.name_country, count(u.id_user) as user FROM tb_user u
                                    JOIN tb_country c ON c.id_country = u.fk_country
                                    WHERE u.valid_user = 1 AND u.status_user = 1
                                    group by u.fk_country;");
         $statement->execute();
         $list['userByCountry'] = $statement->fetchAll();



//         $user = $db->createQueryBuilder();
//         $user->select('count(u.idUser)')
//                 ->from('DbBundle:TbUser', 'u')
//                 ->where('u.statusUser = :status')
//                 ->setParameter('status', 1);
//         $list['totalUser'] = $user->getQuery()->getSingleScalarResult();

         $chat = $db->createQueryBuilder();
         $chat->select('count(u.idChat)')
                 ->from('DbBundle:TbChat', 'u')
                 ->where('u.fkStatusChat = :status')
                 ->setParameter('status', 3);
         $list['totalChatActive'] = $chat->getQuery()->getSingleScalarResult();

         $chatA = $db->createQueryBuilder();
         $chatA->select('count(u.idChat)')
                 ->from('DbBundle:TbChat', 'u');
         $list['totalChat'] = $chatA->getQuery()->getSingleScalarResult();

         $payA = $db->createQueryBuilder();
         $payA->select('count(p.idPayment)')
                 ->from('DbBundle:TbPayment', 'p')
                 ->where('p.validPayment = :status')
                 ->setParameter('status', 0);
         $list['totalPaymentValidate'] = $payA->getQuery()->getSingleScalarResult();

         $pay = $db->createQueryBuilder();
         $pay->select('count(p.idPayment)')
                 ->from('DbBundle:TbPayment', 'p');
         $list['totalPayment'] = $chatA->getQuery()->getSingleScalarResult();

         return $this->json(array("message" => "Listado de roles", "data" => $list, "code" => "SD001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "ED001"), 400);
      }
   }

   // Get Videos User
   public function videosUserAction() {
      try {
         $db = $this->getDoctrine()->getManager();
         $videos = $db->getRepository('DbBundle:TbVideoUser')->findBy(array("statusVideoUser" => 1), array('idVideoUser' => 'DESC'));
         $videoList = [];
         foreach ($videos as $video) {
            $videoList[] = array(
                "id" => $video->getIdVideoUser(),
                "code" => $video->getCodeVideoUser(),
                "idUser" => $video->getFkUser()->getIdUser(),
                "loginUser" => $video->getFkUser()->getLoginUser(),
            );
         }
         return $this->json(array("message" => "Listado de roles", "data" => $videoList, "code" => "SD001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "ED001"), 400);
      }
   }

   //  tumbar ws
   public function stopWsAction() {
      $process = new Process('fuser 9000/tcp;kill 26825;');
      $process->run();

      // executes after the command finishes
      if (!$process->isSuccessful()) {
         return $this->json(array("message" => $process->getErrorOutput(), "code" => "SD001"), 200);
      }
      return $this->json(array("message" => $process->getOutput(), "code" => "SD001"), 200);
   }

   public function startWsAction() {
      $process = new Process('cd ..;php bin/console gos:websocket:server -e "prod"');
      $process->run();
      if (!$process->isSuccessful()) {
         return $this->json(array("message" => $process->getErrorOutput(), "code" => "SD001"), 200);
      }
      return $this->json(array("message" => $process->getOutput(), "code" => "SD001"), 200);
   }

   public function gitPullAction($raiz) {
      if (($raiz != null) && (!empty($raiz))) {
         if ($raiz == "api") {
            $process = new Process('cd ..;git pull https://beto2102@bitbucket.org/Group-Web/api_coinspump.git master');
         } else if ($raiz == "web") {
            $process = new Process('cd ..;cd ..;cd coins2pump;git pull https://beto2102@bitbucket.org/Group-Web/coins2pump.git master');
         } else if ($raiz == "admin") {
            $process = new Process('cd ..;cd ..;cd admin_coinspump;git pull https://beto2102@bitbucket.org/Group-Web/admin_coinspump.git master');
         }
         $process->run();
         if (!$process->isSuccessful()) {
            return $this->json(array("message" => $process->getErrorOutput(), "code" => "SD001"), 200);
         }
         return $this->json(array("message" => $process->getOutput(), "code" => "SD001"), 200);
      }
      return $this->json(array("message" => "ERROR NO ENTRO", "code" => "SD001"), 200);
   }

}
