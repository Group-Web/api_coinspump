<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Validation\UserValidation;
//use Symfony\Component\Validator\Constraints as Assert;
//use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use AppBundle\Services\Helper;
use AppBundle\Services\SendMail;
use AppBundle\Services\JwtAuth;
use DbBundle\Entity\TbUser;
use DbBundle\Entity\TbVideoUser;
use DbBundle\Entity\TbGender;
use DbBundle\Entity\TbCountry;
use DbBundle\Entity\TbRole;

class UserController extends Controller {

   private $Jwt;
   private $Helper;
   private $SendMail;

   public function __construct(JwtAuth $jwt_auth, Helper $helper, SendMail $sendMail) {
      $this->Jwt = $jwt_auth;
      $this->Helper = $helper;
      $this->SendMail = $sendMail;
   }

   public function loginAction(Request $request, \Swift_Mailer $mailer) {
      try {
         // Captar Json
         $json = json_decode($request->getContent(), true);

         // Validar que tenga datos
         if (empty($json)) {
            return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
         }

         $db = $this->getDoctrine()->getManager();
         $validate = new UserValidation($db);
         $datos = $validate->loginAction($json);

         if (!empty($datos['error'])) {
            return $this->json(array("message" => "Algunos datos estan incompletos o errados", "validation" => $datos['error'], "code" => "EU002"), 400);
         }

         $signup = $this->Jwt->SignUpUser($datos['value']['loginUser'], $datos['value']['passwordUser'], $datos["value"]["decodeToken"]);

         if (empty($signup)) {
            $val['passwordUser'] = "¡La Contraseña es incorrecta!";
            return $this->json(array("message" => "¡La Contraseña es incorrecta!", "validation" => $val, "code" => "EU002"), 400);
         }

         //Decodifico el token para sacar los datos para reenviar el correo
         $decoded = $this->Jwt->DecodeToken($signup);

         if (empty($decoded->validUser)) {
            $this->SendMail->confirmUserMail($decoded->emailUser, $decoded->nameUser, $decoded->codeUser, $mailer);
            return $this->json(array("message" => "¡Usuario no valido, se ha enviado un correo para validar su usuario!", "code" => "EU002"), 400);
         }

         return $this->json(array("message" => "Bienvenido al portal", "data" => array("token" => $signup), "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function indexAction(Request $request) {
      try {
         $hash = $request->headers->get('Authorization');

         $session = $this->Jwt->ValidateToken($hash, true, 1);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "EU002"), 401);
         }

         $db = $this->getDoctrine()->getManager();
         $repository = $db->getRepository('DbBundle:TbUser');
         $users = $repository->findAll();
         $list = [];
         foreach ($users as $user) {
            $user_list = [
                "idUser" => $user->getIdUser(),
                "nameUser" => $user->getNameUser(),
                "lastnameUser" => $user->getLastnameUser(),
                "loginUser" => $user->getLoginUser(),
                "emailUser" => $user->getEmailUser(),
                "birthdayUser" => $user->getBirthdayUser()->format('Y-m-d'),
                "addressUser" => $user->getAddressUser(),
                "celUser" => $user->getCelUser(),
                "phoneUser" => $user->getPhoneUser(),
                "referenceUser" => $user->getReferenceUser(),
                "idRoleUser" => $user->getFkRole()->getIdRole(),
                "nameRoleUser" => $user->getFkRole()->getNameRole(),
                "idCountryUser" => $user->getFkCountry()->getIdCountry(),
                "nameCountryUser" => $user->getFkCountry()->getNameCountry(),
                "idGenderUser" => $user->getFkGender()->getIdGender(),
                "nameGenderUser" => $user->getFkGender()->getNameGender(),
                "fkGenderUser" => $user->getFkGender()->getNameGender(),
                "validUser" => $user->getValidUser(),
                "codeUser" => $user->getCodeUser(),
                "idStatusUser" => $user->getStatusUser(),
            ];
            $list[] = $user_list;
            unset($user_list);
         }
         return $this->json(array("message" => "Listado Usuarios", "data" => $list, "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function newAction(Request $request, \Swift_Mailer $mailer) {
      try {
         // Captar Json
         $json = json_decode($request->getContent(), true);

         // Validar que tenga datos
         if (empty($json)) {
            return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
         }

         // Valida si es un usuario Valido Admin o de la web
         $hash = $request->headers->get('Authorization');
         $session = $this->Jwt->ValidateToken($hash, true, true);

         // Es un Usuario Nuevo Web o por el Admin
         $db = $this->getDoctrine()->getManager();
         $validate = new UserValidation($db);
         if ($session == false) {
            $datos = $validate->newAction($json);
         } else {
            $datos = $validate->newAdminAction($json);
         }

         if (!empty($datos['error'])) {
            return $this->json(array("message" => "Algunos datos estan incompletos o errados", "validation" => $datos['error'], "code" => "EU002"), 400);
         }

         //Instanciar Usuario
         $user = new TbUser();
         $user->setNameUser($datos['value']['nameUser']);
         $user->setLastnameUser($datos['value']['lastnameUser']);
         $user->setLoginUser($datos['value']['loginUser']);
         $user->setEmailUser($datos['value']['emailUser']);
         $user->setBirthdayUser(new \DateTime($datos['value']['birthdayUser']));
         $user->setCreatedUser(new \DateTime());
         $user->setAddressUser($datos['value']['addressUser']);
         $user->setCelUser($datos['value']['celUser']);
         $user->setPhoneUser($datos['value']['phoneUser']);
         $user->setReferenceUser($datos['value']['referenceUser']);
         $user->setPasswordUser(hash('sha256', $datos['value']['passwordUser']));
         $user->setStatusUser(0);

         // Validar Usuario Por correo
         $user->setValidUser(0);
         $codeUser = hash('ripemd160', strtotime(date("Y-m-d H:m:s")));
         $user->setCodeUser($codeUser);

         // Foraneas
         $user->setFkRole($db->find(TbRole::class, $datos['value']['fkRoleUser']));
         $user->setFkCountry($db->find(TbCountry::class, $datos['value']['fkCountryUser']));
         $user->setFkGender($db->find(TbGender::class, $datos['value']['fkGenderUser']));

         // Guardar
         $db->persist($user);
         $db->flush();
         // Enviar Correo
         $this->SendMail->confirmUserMail($datos['value']['emailUser'], $datos['value']['nameUser'], $codeUser, $mailer);

         return $this->json(array("message" => "Bien"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function editAction(Request $request) {
      // Captar Json
      $json = json_decode($request->getContent(), true);
      try {
         $hash = $request->headers->get('Authorization');
         $session = $this->Jwt->ValidateToken($hash, false, true);
         $db = $this->getDoctrine()->getManager();
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "EU002"), 401);
         } else if (($session['idRoleUser'] != 1) && (!empty($json['idUser']))) {
            return $this->json(array("message" => "¡No posee privilegios!", "code" => "EU002"), 401);
         } else if (($session['idRoleUser'] == 1) && (!empty($json['idUser']))) {
            $json['idUser'] = $json['idUser'];

            $user = $db->getRepository('DbBundle:TbUser')->findOneBy(['loginUser' => $json['username'], 'idUser' => $json['idUser']]);
            if (count($user) == 1) {
               $user->setLoginUser($json['username']);
               $user->setEmailUser($json['email']);
               $user->setNameUser($json['name']);
               $user->setLastnameUser($json['lastname']);
               $user->setBirthdayUser(new \DateTime($json['birthday']));
               $user->setFkCountry($db->getRepository('DbBundle:TbCountry')->findOneBy(['idCountry' => $json['country']]));
               $user->setDescriptionUser($json['description']);
               $user->setFkGender($db->getRepository('DbBundle:TbGender')->findOneBy(['idGender' => $json['gender']]));
               $user->setFkRole($db->getRepository('DbBundle:TbRole')->findOneBy(['idRole' => $json['role']]));
               $db->persist($user);
               $db->flush();
            } else {
               return $this->json(array("message" => "Nombre de usuario ya existe", "data" => '', "code" => "EU001"), 400);
            }
         } else {
            $json['idUser'] = $session['idUser'];
            $user = $db->getRepository('DbBundle:TbUser')->findOneBy(['idUser' => $json['idUser']]);
            if (count($user) == 1) {
               $user->setNameUser($json['name']);
               $user->setLastnameUser($json['lastname']);
               //$user->setLoginUser($json['username']);
               $user->setEmailUser($json['email']);
               $user->setFkCountry($db->getRepository('DbBundle:TbCountry')->findOneBy(['idCountry' => $json['country']]));
               $user->setDescriptionUser($json['description']);
               $db->persist($user);
               $db->flush();
            } else {
               return $this->json(array("message" => "Nombre de usuario ya existe", "data" => '', "code" => "EU001"), 400);
            }
         }



         return $this->json(array("message" => "¡Usuario Actualizado!", "data" => '', "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function confirmUserAction($code, \Swift_Mailer $mailer) {
      try {
         if (($code != null) && (!empty($code))) {
            $db = $this->getDoctrine()->getManager();
            $user = $db->getRepository("DbBundle:TbUser")->findOneBy(["codeUser" => $code, "validUser" => 0]);
            if (!empty($user)) {
               $user->setValidUser(1);
               $user->setStatusUser(1);
               $user->setcodeUser('');
               $db->persist($user);
               $db->flush();
               $data = 'Tu cuenta ha sido activada con éxito. En breve,  recibirás un correo de ¡Bienvenida! ';

               // Enviar correo de bienvenida
               $this->SendMail->welcomeUserMail($user->getEmailUser(), $user->getNameUser(), $mailer);
               return $this->json(array("message" => $data, "code" => "SU001"), 200);
            } else {
               $data = '¡Lo siento! Este codigo ya fue usado o no es valido.';
            }
         } else {
            $data = '¡Lo siento! Este codigo ya fue usado o no es valido.';
         }
         return $this->json(array("message" => $data, "code" => "SU001"), 400);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function profileAction($user = null, Request $request) {
      try {
         $hash = $request->headers->get('Authorization');
         $session = $this->Jwt->ValidateToken($hash, false, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "EC002"), 401);
         }
         if ($request->attributes->get('user') != null) {
            $login = $request->attributes->get('user');
         } else {
            $login = $session['loginUser'];
         }
         $db = $this->getDoctrine()->getManager();
         $profile = $db->getRepository('DbBundle:TbUser')->findOneBy(["loginUser" => $login]);
         if (!empty($profile)) {
            $videos = $db->getRepository('DbBundle:TbVideoUser')->findBy(["fkUser" => $profile->getIdUser()]);
            $videoList = [];
            foreach ($videos as $video) {
               $videoList[$video->getIdVideoUser()] = $video->getCodeVideoUser();
            }
            $data = [
                "idUser" => $profile->getIdUser(),
                "userphoto" => "http://www.thehindu.com/sci-tech/technology/internet/article17759222.ece/alternates/FREE_660/02th-egg-person",
                "nameUser" => $profile->getNameUser(),
                "descriptionUser" => $profile->getDescriptionUser(),
                "lastnameUser" => $profile->getLastnameUser(),
                "loginUser" => $profile->getLoginUser(),
                "emailUser" => $profile->getEmailUser(),
                "birthdayUser" => $profile->getBirthdayUser()->format('Y-m-d'),
                "addressUser" => $profile->getAddressUser(),
                "celUser" => $profile->getCelUser(),
                "phoneUser" => $profile->getPhoneUser(),
                "referenceUser" => $profile->getReferenceUser(),
                "idRoleUser" => $profile->getFkRole()->getIdRole(),
                "nameRoleUser" => $profile->getFkRole()->getNameRole(),
                "idCountryUser" => $profile->getFkCountry()->getIdCountry(),
                "nameCountryUser" => $profile->getFkCountry()->getNameCountry(),
                "idGenderUser" => $profile->getFkGender()->getIdGender(),
                "nameGenderUser" => $profile->getFkGender()->getNameGender(),
                "fkGenderUser" => $profile->getFkGender()->getNameGender(),
                "validUser" => $profile->getValidUser(),
                "videos" => $videoList,
                    //"codeUser" => $profile->getCodeUser(),
            ];
            return $this->json(array("message" => "Listado Usuarios", "data" => $data, "code" => "SU001"), 200);
         }
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function byidAction(Request $request) {
      try {
         $json = json_decode($request->getContent(), true);
         $hash = $request->headers->get('Authorization');
         $session = $this->Jwt->ValidateToken($hash, true, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "EC002"), 401);
         }
         $db = $this->getDoctrine()->getManager();
         $profile = $db->getRepository('DbBundle:TbUser')->findOneBy(["idUser" => $json['id']]);
         if (!empty($profile)) {
            $videos = $db->getRepository('DbBundle:TbVideoUser')->findBy(["fkUser" => $profile->getIdUser()]);
            $videoList = [];
            foreach ($videos as $video) {
               $videoList[$video->getIdVideoUser()] = $video->getCodeVideoUser();
            }
            $data = [
                "idUser" => (int) $profile->getIdUser(),
                "userphoto" => "http://www.thehindu.com/sci-tech/technology/internet/article17759222.ece/alternates/FREE_660/02th-egg-person",
                "nameUser" => $profile->getNameUser(),
                "descriptionUser" => $profile->getDescriptionUser(),
                "lastnameUser" => $profile->getLastnameUser(),
                "loginUser" => $profile->getLoginUser(),
                "emailUser" => $profile->getEmailUser(),
                "birthdayUser" => $profile->getBirthdayUser()->format('Y-m-d'),
                "addressUser" => $profile->getAddressUser(),
                "celUser" => $profile->getCelUser(),
                "phoneUser" => $profile->getPhoneUser(),
                "referenceUser" => $profile->getReferenceUser(),
                "idRoleUser" => $profile->getFkRole()->getIdRole(),
                "nameRoleUser" => $profile->getFkRole()->getNameRole(),
                "idCountryUser" => $profile->getFkCountry()->getIdCountry(),
                "nameCountryUser" => $profile->getFkCountry()->getNameCountry(),
                "idGenderUser" => $profile->getFkGender()->getIdGender(),
                "nameGenderUser" => $profile->getFkGender()->getNameGender(),
                "fkGenderUser" => $profile->getFkGender()->getNameGender(),
                "validUser" => $profile->getValidUser(),
                "videos" => $videoList,
                    //"codeUser" => $profile->getCodeUser(),
            ];
            return $this->json(array("message" => "Listado Usuarios", "data" => $data, "code" => "SU001"), 200);
         }
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function addVideoAction(Request $request) {
      // Captar Json
      $json = json_decode($request->getContent(), true);
      try {
         $hash = $request->headers->get('Authorization');
         $session = $this->Jwt->ValidateToken($hash, false, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "EU002"), 401);
         } else if (($session['idRoleUser'] != 1) && (!empty($json['idUser']))) {
            return $this->json(array("message" => "¡No posee privilegios!", "code" => "EU002"), 401);
         } else if (($session['idRoleUser'] == 1) && (!empty($json['idUser']))) {
            $json['idUser'] = $json['idUser'];
         } else {
            $json['idUser'] = $session['idUser'];
         }

         $db = $this->getDoctrine()->getManager();

         $user = $db->getRepository('DbBundle:TbUser')->findOneBy(['idUser' => $json['idUser']]);
         if (count($user) == 1) {
            $video = new TbVideoUser();
            $video->setCodeVideoUser($json['id']);
            $video->setFkUser($user);
            $video->setStatusVideoUser(1);

            $db->persist($video);
            $db->flush();
         } else {
            return $this->json(array("message" => "Usuario no existe", "data" => '', "code" => "EU001"), 400);
         }

         return $this->json(array("message" => "¡Actualizado!", "data" => '', "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function deleteVideoAction(Request $request) {
      // Captar Json
      $json = json_decode($request->getContent(), true);
      try {
         $hash = $request->headers->get('Authorization');
         $session = $this->Jwt->ValidateToken($hash, false, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "EU002"), 401);
         } else if (($session['idRoleUser'] != 1) && (!empty($json['idUser']))) {
            return $this->json(array("message" => "¡No posee privilegios!", "code" => "EU002"), 401);
         } else if (($session['idRoleUser'] == 1) && (!empty($json['idUser']))) {
            $json['idUser'] = $json['idUser'];
         } else {
            $json['idUser'] = $session['idUser'];
         }

         $db = $this->getDoctrine()->getManager();

         $video = $db->getRepository('DbBundle:TbVideoUser')->findOneBy(['fkUser' => $json['idUser'], 'codeVideoUser' => $json['id']]);
         if (count($video) != 0) {
            $db->remove($video);
            $db->flush();
         } else {
            return $this->json(array("message" => "Video no existe", "data" => '', "code" => "EU001"), 400);
         }

         return $this->json(array("message" => "Video Eliminado!", "data" => '', "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

}
