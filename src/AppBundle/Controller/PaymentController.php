<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Validation\PaymentValidation;
use AppBundle\Payments\UpholdPayment;
use AppBundle\Services\Helper;
use AppBundle\Services\SendMail;
use AppBundle\Services\JwtAuth;
use DbBundle\Entity\TbUser;
use DbBundle\Entity\TbChat;
use DbBundle\Entity\TbChatUser;
use DbBundle\Entity\TbPayment;
use DbBundle\Entity\TbServicePayment;
use DbBundle\Entity\TbPriceServicePayment;
use DbBundle\Entity\TbTypePayment;
use DbBundle\Entity\TbCryptocurrency;
use DbBundle\Entity\TbStatusChat;

class PaymentController extends Controller {

   private $Jwt;
   private $Helper;
   private $SendMail;

   public function __construct(JwtAuth $jwt_auth, Helper $helper, SendMail $sendMail) {
      $this->Jwt = $jwt_auth;
      $this->Helper = $helper;
      $this->SendMail = $sendMail;
   }

//
//   public function newChatAction(Request $request, \Swift_Mailer $mailer) {
//      try {
//         // Captar Json
//         $json = json_decode($request->getContent(), true);
//
//         // Validar que tenga datos
//         if (empty($json)) {
//            return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EP002"), 400);
//         }
//
//         // Valida si es un usuario Valido Admin o de la web
//         $hash = $request->headers->get('Authorization');
//
//         $session = $this->Jwt->ValidateToken($hash, false, true);
//         if ($session == false) {
//            return $this->json(array("message" => "¡Token Invalido!", "code" => "EU002"), 400);
//         }
//
//         // Es un Usuario Nuevo Web o por el Admin
//         $validate = new PaymentValidation();
//         $datos = $validate->newChatAction($json);
//
//         if (!empty($datos['error'])) {
//            return $this->json(array("message" => $datos['error'], "code" => "EU002"), 400);
//         }
//
//         // Llevar a cabo la compra
//         if ($datos['value']['typePayment'] == 1) {
////                $payment = New UpholdPayment();
////                $commit = $payment->setCommintPayment($datos['value']['tokenUP'], $datos['value']['otpUP'], $datos['value']['cuentaOriginUP'], $datos['value']['idTransactionUP'], $datos['value']['messageUP']);
////                $transactionID = $commit->x;
//            $transactionID = "ESTA CABLEADO";
//         }
//
//         // Conexion
//         $db = $this->getDoctrine()->getManager();
//         $service_pay = $db->find(TbServicePayment::class, $datos['value']['fkServicePayment']);
//         $payments = $db->getRepository('DbBundle:TbPriceServicePayment')->findBy(["fkServicePayment" => $service_pay->getId, "statusPriceServicePayment" => 1]);
//
//         // GUARDAR PAGO
//         $payment = new TbPayment();
//         $payment->setAmountPayment($datos['value']['ammountPayment']);
//         $payment->setReferencePayment($transactionID);
//         $payment->setdescriptionPayment($datos['value']['descriptionPayment']);
//         $payment->setFkChatUser($db->find(TbChatUser::class, $datos['value']['idChat']));
//         $payment->getFkTypePayment($db->find(TbTypePayment::class, $datos['value']['typePayment']));
//         $payment->getFkServicePayment($db->find(TbServicePayment::class, $datos['value']['fkServicePayment']));
//         $db->persist($payment);
//         $db->flush();
//
//         // Enviar Correo Comprobante de pago
//         // $this->SendMail->confirmUserMail($datos['value']['emailUser'], $datos['value']['nameUser'], $codeUser, $mailer);
//         return $this->json(array("message" => "Bien"), 200);
//      } catch (Exception $e) {
//         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EP001"), 400);
//      }
//   }
//   
   public function listAction(Request $request) {
      try {
         $hash = $request->headers->get('Authorization');

         $session = $this->Jwt->ValidateToken($hash, true, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
         }

         $db = $this->getDoctrine()->getManager();
         $payment = $db->getRepository('DbBundle:TbPayment')->findAll();

         foreach ($payment as $pay) {
            $_list = [
                "idPayment" => $pay->getIdPayment(),
                "ammountPayment" => $pay->getAmountPayment(),
                "referencePayment" => $pay->getReferencePayment(),
                "descriptionPayment" => $pay->getDescriptionPayment(),
                "typePayment" => $pay->getFkTypePayment()->getNameTypePayment(),
                "cryptocurrencyPayment" => $pay->getFkTypePayment()->getFkCryptocurrency()->getNameCryptocurrency(),
                "servicePayment" => $pay->getFkServicePayment()->getNameServicePayment(),
                "idUser" => $pay->getFkUser()->getIdUser(),
                "nameUser" => $pay->getFkUser()->getLoginUser(),
                "idChat" => ($pay->getFkChat() != null) ? $pay->getFkChat()->getIdChat() : null,
                "nameChat" => ($pay->getFkChat() != null) ? $pay->getFkChat()->getNameChat() : null,
                "validPayment" => $pay->getValidPayment(),
                "createdPayment" => $pay->getCreatedPayment()->format('Y-m-d'),
//                "idChatPayment" => $pay->getDescriptionPayment(),
//                "descriptionPayment" => $pay->getDescriptionPayment(),
//                "nameRoleUser" => $user->getFkRole()->getNameRole(),
//                "idCountryUser" => $user->getFkCountry()->getIdCountry(),
//                "nameCountryUser" => $user->getFkCountry()->getNameCountry(),
//                "idGenderUser" => $user->getFkGender()->getIdGender(),
//                "nameGenderUser" => $user->getFkGender()->getNameGender(),
//                "fkGenderUser" => $user->getFkGender()->getNameGender(),
//                "validUser" => $user->getValidUser(),
//                "codeUser" => $user->getCodeUser(),
            ];
            $list[] = $_list;
            unset($_list);
         }
         return $this->json(array("message" => "Listado Usuarios", "data" => $list, "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function newChatAction(Request $request, \Swift_Mailer $mailer) {
      try {
         // Captar Json
         $json = json_decode($request->getContent(), true);

         // Validar que tenga datos
         if (empty($json)) {
            return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EP002"), 400);
         }
         // Valida si es un usuario Valido Admin o de la web
         $hash = $request->headers->get('Authorization');
         $session = $this->Jwt->ValidateToken($hash, false, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
         }

         // Es un Usuario Nuevo Web o por el Admin
//         $validate = new PaymentValidation();
//         $datos = $validate->newChatAction($json);

         if (!empty($datos['error'])) {
            return $this->json(array("message" => $datos['error'], "code" => "EU002"), 400);
         }

         // Conexion
         $db = $this->getDoctrine()->getManager();
         $TypePayment = $db->find(TbTypePayment::class, $json['typePayment']);
         $priceServicePayment = $db->getRepository('DbBundle:TbPriceServicePayment')->findOneBy(
                 [
                     "fkCryptocurrency" => $json['typePayment'],
                     "fkServicePayment" => $json['fkServicePayment']
                 ]
         );

         // GUARDAR PAGO
         $payment = new TbPayment();
         $payment->setAmountPayment($priceServicePayment->getValuePriceServicePayment());
         $payment->setReferencePayment($json['referencePayment']);
         $payment->setdescriptionPayment($json['descriptionPayment']);
         $payment->setCreatedPayment(new \DateTime());
         if ($json['fkServicePayment'] < 3) {
            $chat = $db->find(TbChat::class, $json["idChat"]);
            $payment->setFkChat($chat);
         }
         $payment->setFkUser($db->find(TbUser::class, $session['idUser']));
         $payment->setFkTypePayment($TypePayment);
         $payment->setFkServicePayment($priceServicePayment->getFkServicePayment());
         $db->persist($payment);
         $db->flush();

         // Si es crear un chat (cambia el estatus a por revision)
         if ($json['fkServicePayment'] == 1) {
            $chat->setFkStatusChat($db->find(TbStatusChat::class, 2));
            $db->persist($chat);
            $db->flush();
         }

         // Activa el Usuario en el Chat User
         if ($json['fkServicePayment'] < 3) {
            $chatUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $json["idChat"], "fkUser" => $session['idUser']]);
            $chatUser->setValidChatUser(0);
            $db->persist($chatUser);
            $db->flush();
         }

         return $this->json(array("message" => "Bien"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EP001"), 400);
      }
   }

   public function listByChatAction(Request $request) {
      try {
         $hash = $request->headers->get('Authorization');
         $json = json_decode($request->getContent(), true);

         $session = $this->Jwt->ValidateToken($hash, false, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
         }

         $db = $this->getDoctrine()->getManager();
         $list = [];

         $ServicePay = $db->find(TbServicePayment::class, $json['idServicePayment']);
         $list["idServicePayment"] = $ServicePay->getIdServicePayment();
         $list["nameServicePayment"] = $ServicePay->getNameServicePayment();
         $list["descriptionServicePayment"] = $ServicePay->getDescriptionServicePayment();

         if ($json["idServicePayment"] < 3) {
            $chat = $db->find(TbChat::class, $json['idChat']);
            $list["idChat"] = $chat->getIdChat();
            $list["nameChat"] = $chat->getNameChat();
            $list["descriptionChat"] = $chat->getDescriptionChat();
         }

         if ($json["idServicePayment"] == 2) {
            $payments = $db->getRepository('DbBundle:TbPriceChat')->findBy(["fkChat" => $json["idChat"]]);
            foreach ($payments as $payment) {
               // OJO ESTA URL HAY Q CAMBIARA SI SON VARIAS DE BITCOIN
               $uri = $db->getRepository('DbBundle:TbTypePayment')->findOneBy(["fkCryptocurrency" => $payment->getfkCryptocurrency()->getIdCryptocurrency()]);
               $payment_list = [
                   "idCryptocurrency" => $payment->getfkCryptocurrency()->getIdCryptocurrency(),
                   "uriCryptocurrency" => $uri->getUriTypePayment(),
                   "nameCryptocurrency" => $payment->getfkCryptocurrency()->getNameCryptocurrency(),
                   "codeCryptocurrency" => $payment->getfkCryptocurrency()->getCodeCryptocurrency(),
                   "imageCryptocurrency" => $payment->getfkCryptocurrency()->getImageCryptocurrency(),
                   "valuePayment" => $payment->getValuePriceChat(),
                   "DescriptionPayment" => $payment->getDescriptionPriceChat(),
               ];
               $list['priceChat'][] = $payment_list;
               unset($payment_list);
            }
         } else {
            $payments = $db->getRepository('DbBundle:TbPriceServicePayment')->findBy(["fkServicePayment" => $json["idServicePayment"], "statusPriceServicePayment" => 1]);
            foreach ($payments as $payment) {
               $uri = $db->getRepository('DbBundle:TbTypePayment')->findOneBy(["fkCryptocurrency" => $payment->getfkCryptocurrency()->getIdCryptocurrency()]);
               $payment_list = [
                   "idCryptocurrency" => $payment->getfkCryptocurrency()->getIdCryptocurrency(),
                   "uriCryptocurrency" => $uri->getUriTypePayment(),
                   "nameCryptocurrency" => $payment->getfkCryptocurrency()->getNameCryptocurrency(),
                   "codeCryptocurrency" => $payment->getfkCryptocurrency()->getCodeCryptocurrency(),
                   "imageCryptocurrency" => $payment->getfkCryptocurrency()->getImageCryptocurrency(),
                   "valuePayment" => $payment->getValuePriceServicePayment(),
                   "DescriptionPayment" => $payment->getDescriptionPriceServicePayment(),
               ];
               $list['priceChat'][] = $payment_list;
               unset($payment_list);
            }
         }
         return $this->json(array("message" => "Listado Usuarios", "data" => $list, "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function adminValidateAction(Request $request) {
      try {
         $hash = $request->headers->get('Authorization');
         $json = json_decode($request->getContent(), true);

         $session = $this->Jwt->ValidateToken($hash, true, true);
         if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
         }

         $db = $this->getDoctrine()->getManager();
         $pay = $db->getRepository('DbBundle:TbPayment')->findOneBy(["idPayment" => $json['idPayment']]);
         if ($pay->getValidPayment()) {
            // si es crear chat
            if ($pay->getFkServicePayment()->getIdServicePayment() == 1) {
               $chat = $db->getRepository('DbBundle:TbChat')->findOneBy(["idChat" => $pay->getFkChat()]);
               $chat->setValidChat(0);
               $chat->setFkStatusChat($db->find(TbStatusChat::class, 2));
               $db->persist($chat);
               $db->flush();

               $chatUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $pay->getFkChat(), "fkUser" => $pay->getFkUser()]);
               $chatUser->setValidChatUser(0);
               $db->persist($chatUser);
               $db->flush();
            } else if ($pay->getFkServicePayment()->getIdServicePayment() == 2) {
               $chatUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $pay->getFkChat(), "fkUser" => $pay->getFkUser()]);
               $chatUser->setValidChatUser(1);
               $db->persist($chatUser);
               $db->flush();
            } else {
               
            }
            $pay->setValidPayment(0);
         } else {
            if ($pay->getFkServicePayment()->getIdServicePayment() == 1) {
               $chat = $db->getRepository('DbBundle:TbChat')->findOneBy(["idChat" => $pay->getFkChat()]);
               $chat->setValidChat(1);
               $chat->setFkStatusChat($db->find(TbStatusChat::class, 3));
               $db->persist($chat);
               $db->flush();

               $chatUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $pay->getFkChat(), "fkUser" => $pay->getFkUser()]);
               $chatUser->setValidChatUser(1);
               $db->persist($chatUser);
               $db->flush();
            } else if ($pay->getFkServicePayment()->getIdServicePayment() == 2) {
               $chatUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $pay->getFkChat(), "fkUser" => $pay->getFkUser()]);
               $chatUser->setValidChatUser(1);
               $db->persist($chatUser);
               $db->flush();
            } else {
               
            }
            $pay->setValidPayment(1);
         }
         $db->persist($pay);
         $db->flush();

         return $this->json(array("message" => "Actualizado", "code" => "SU001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
      }
   }

   public function typePaymentAction(Request $request) {
      try {
         $db = $this->getDoctrine()->getManager();
         $repository = $db->getRepository('DbBundle:TbTypePayment');
         $cryptos = $repository->findBy(array('statusTypePayment' => 1));
         /* $cryptos = $db->createQueryBuilder();
           $cryptos->select('p')
           ->from('DbBundle:TbTypePayment', 'p')
           ->where('p.statusTypePayment = 1')
           ->groupby('p.fkCryptocurrency'); */
         $list = [];
         foreach ($cryptos as $crypto) {
            $crypto_list = [
                "idTypePayment" => $crypto->getIdTypePayment(),
                "nameTypePayment" => $crypto->getNameTypePayment(),
                "providerTypePayment" => $crypto->getFkProviderPayment()->getNameProviderPayment(),
                "uriTypePayment" => $crypto->getUriTypePayment(),
                "idCryptocurrency" => $crypto->getFkCryptocurrency()->getIdCryptocurrency(),
                "nameCryptocurrency" => $crypto->getFkCryptocurrency()->getNameCryptocurrency(),
                "codeCryptocurrency" => $crypto->getFkCryptocurrency()->getCodeCryptocurrency(),
            ];
            $list[] = $crypto_list;
            unset($crypto_list);
         }
         return $this->json(array("message" => "Listado de Cryptocurrency", "data" => $list, "code" => "SCR001"), 200);
      } catch (Exception $e) {
         return $this->json(array("message" => "¡Error Desconocido!", "code" => "ECR001"), 400);
      }
   }

}
