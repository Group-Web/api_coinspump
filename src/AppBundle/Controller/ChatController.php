<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Validation\ChatValidation;
use AppBundle\Services\Helper;
use AppBundle\Services\SendMail;
use AppBundle\Services\JwtAuth;
use DbBundle\Entity\TbUser;
use DbBundle\Entity\TbChat;
use DbBundle\Entity\TbChatUser;
use DbBundle\Entity\TbPriceChat;
use DbBundle\Entity\TbTypeChat;
use DbBundle\Entity\TbCryptocurrency;
use DbBundle\Entity\TbStatusChat;

class ChatController extends Controller {

    private $Jwt;
    private $Helper;
    private $SendMail;

    public function __construct(JwtAuth $jwt_auth, Helper $helper, SendMail $sendMail) {
        $this->Jwt = $jwt_auth;
        $this->Helper = $helper;
        $this->SendMail = $sendMail;
    }

    public function newAction(Request $request, \Swift_Mailer $mailer) {
        try {
            // Captar Json
            $json = json_decode($request->getContent(), true);

            // Validar que tenga datos
            if (empty($json)) {
                return $this->json(array("message" => "¡Se han enviado datos inválidos o inexistentes.!", "code" => "EU002"), 400);
            }

            // Valida si es un usuario valido
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, false, true);
            if ($session == false) {
                return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }

            // Es un Usuario Nuevo Web o por el Admin
            $validate = new ChatValidation();
            $datos = $validate->newChatAction($json, $session['idRoleUser']);
            if (!empty($datos['error'])) {
                return $this->json(array("message" => $datos['error'], "code" => "EC002"), 400);
            }

            // Conexion
            $db = $this->getDoctrine()->getManager();

            // Guadar Chat
            $chat = new TbChat();
            $chat->setNameChat($datos['value']['nameChat']);
            $chat->setStartDateChat(new \DateTime($datos['value']['startDateChat']));
            $chat->setEndDateChat(new \DateTime($datos['value']['endDateChat']));
            $chat->setDescriptionChat($datos['value']['descriptionChat']);
            $chat->setConclusionChat($datos['value']['conclusionChat']);
            $chat->setCreatedChat(new \DateTime());
            $chat->setValidChat($datos['value']['validChat']);
            $chat->setFkTypeChat($db->find(TbTypeChat::class, $datos['value']['typeChat']));
            $chat->setFkCryptocurrency($db->find(TbCryptocurrency::class, $datos['value']['cryptocurrency']));
            $chat->setFkStatusChat($db->find(TbStatusChat::class, $datos['value']['statusChat']));
            $db->persist($chat);
            $db->flush();

            // Guardar Pagos
            if ($datos['value']['priceChat'] != null) {
                foreach ($datos['value']['priceChat'] as $key => $valor) {
                    $priceChat = new TbPriceChat();
                    $priceChat->setValuePriceChat($valor["valuePriceChat"]);
                    $priceChat->setDescriptionPriceChat($valor["descriptionPriceChat"]);
                    $priceChat->setDescriptionPriceChat($valor["statusPriceChat"]);
                    $priceChat->setCreatedPriceChat(new \DateTime());
                    $priceChat->setFkChat($db->find(TbChat::class, $chat->getIdChat()));
                    $priceChat->setFkCryptocurrency($db->find(TbCryptocurrency::class, $valor["cryptocurrency"]));
                    $db->persist($priceChat);
                    $db->flush();
                }
            }

            //Guradar Chat User
            $chatuser = new TbChatUser();
            $chatuser->setOwnerChatUser(1);
            $chatuser->setCreatedChatUser(new \DateTime());
            $chatuser->setFkChat($db->find(TbChat::class, $chat->getIdChat()));
            $chatuser->setFkUser($db->find(TbUser::class, $session['idUser']));
            $chatuser->setValidChatUser(1);
            $db->persist($chatuser);
            $db->flush();

            return $this->json(array("message" => "Chat Creado", "data" => $chat->getIdChat()), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EC001"), 400);
        }
    }

    public function listAction(Request $request) {
        try {
            $hash = $request->headers->get('Authorization');

            $session = $this->Jwt->ValidateToken($hash, false, true);
            if ($session == false) {
                return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }
            $db = $this->getDoctrine()->getManager();
            if (($session['idRoleUser'] == 1) && ($request->get('_route') == 'admin_chat_list')) {
                $users = $db->getRepository('DbBundle:TbChatUser')->findBy(['ownerChatUser' => '1']);
            } else {
                $users = $db->getRepository('DbBundle:TbChatUser')->findBy(['fkUser' => $session['idUser'], 'ownerChatUser' => '1']);
            }

            $list = [];
            foreach ($users as $user) {
                $user_list = [
                    "idChat" => $user->getfkChat()->getIdChat(),
                    "nameChat" => $user->getfkChat()->getNameChat(),
                    "idCryptocurrency" => $user->getfkChat()->getFkCryptocurrency()->getIdCryptocurrency(),
                    "nameCryptocurrency" => $user->getfkChat()->getFkCryptocurrency()->getNameCryptocurrency(),
                    "codeCryptocurrency" => $user->getfkChat()->getFkCryptocurrency()->getCodeCryptocurrency(),
                    "startChat" => $user->getfkChat()->getStartDateChat()->format('Y-m-d'),
                    "endChat" => $user->getfkChat()->getEndDateChat()->format('Y-m-d'),
                    "nameStatusChat" => $user->getfkChat()->getfkStatusChat()->getNameStatusChat(),
                    "idStatusChat" => $user->getfkChat()->getfkStatusChat()->getIDStatusChat(),
                ];
                $list[] = $user_list;
                unset($user_list);
            }
            return $this->json(array("message" => "Listado Usuarios", "data" => $list, "code" => "SU001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
        }
    }

    public function IndexAction(Request $request) {
        // Captar Json
        $json = json_decode($request->getContent(), true);

        // Valida si es un usuario valido
        $hash = $request->headers->get('Authorization');
        $session = $this->Jwt->ValidateToken($hash, false, true);
        //var_dump($session);
        if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
        }
        $db = $this->getDoctrine()->getManager();
        // GET USER VALID CHATS
        $queryChatUser = $db->createQueryBuilder();
        $queryChatUser->select('cu')
                ->from('DbBundle:TbChatUser', 'cu')
                ->where('cu.fkUser = :id AND (cu.validChatUser = 1 OR cu.ownerChatUser = 1)')
                ->setParameter('id', (int) $session['idUser']);
        // ********************
        $chatUser = [];
        foreach ($queryChatUser->getQuery()->getResult() as $key => $query) {
            $time = 0;
            $checkTime = $db->createQueryBuilder();
            $checkTime->select('p')
                    ->from('DbBundle:TbPayment', 'p')
                    ->where('p.fkChat = :idChat AND p.fkUser = :idUser AND p.fkServicePayment > 2')
                    ->setParameter('idChat', (int) $query->getFkChat()->getIdChat())
                    ->setParameter('idUser', (int) $session['idUser']);
            $checkTime = $checkTime->getQuery()->getResult();
            foreach ($checkTime as $checkTime) {
              $time = (int)$checkTime->getFkServicePayment()->getCostServicePayment();
              //var_dump($query->getFkChat()->getIdChat());
              //var_dump($time);
            }

            // GET ALL USERS REGISTERED ON THIS CHAT
            $chatUserList = $db->getRepository('DbBundle:TbChatUser')->findBy(["fkChat" => $query->getFkChat()->getIdChat()]);
            if (count($chatUserList)) {
                $userList = [];
                foreach ($chatUserList as $user) {
                    if ($user->getValidChatUser() || $user->getOwnerChatUser()) {
                        $userList[$user->getIdChatUser()] = [
                            'idUser' => $user->getFkUser()->getIdUser(),
                            'username' => $user->getFkUser()->getLoginUser(),
                            'isowner' => $user->getOwnerChatUser(),
                            'banned' => $user->getBanChatUser(),
                            'banDescription' => $user->getDescriptionBanChatUser(),
                            'created' => $user->getCreatedChatUser()
                        ];
                    }
                }
            }
            // *************************************
            // GET PRICE CHAT
            $queryPriceChat = $db->createQueryBuilder();
            $queryPriceChat->select('p')
                    ->from('DbBundle:TbPriceChat', 'p')
                    ->where('p.fkChat = :id')
                    ->setParameter('id', (int) $query->getFkChat()->getIdChat());
            $priceArr = [];
            foreach ($queryPriceChat->getQuery()->getResult() as $key => $price) {
                $priceArr[$price->getIdPriceChat()] = [
                    'id' => $price->getIdPriceChat(),
                    'value' => $price->getValuePriceChat(),
                    'description' => $price->getDescriptionPriceChat(),
                    'status' => $price->getStatusPriceChat(),
                    'created' => $price->getCreatedPriceChat()->format('Y-m-d H:i:s'),
                    'currency' => $price->getFkCryptocurrency()->getNameCryptocurrency(),
                ];
            }
            // **************
            $payments = $db->createQueryBuilder();
            $payments->select('p')
                    ->from('DbBundle:TbPayment', 'p')
                    ->where('p.fkUser = :id')
                    ->setParameter('id', (int) $session['idUser']);
            $payments = $payments->getQuery()->getResult();
            $paymentList = [];
            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($payment->getFkServicePayment()->getCostServicePayment() != 0) {
                        $paymentList[$payment->getIdPayment()]['nameServicePayment'] = $payment->getFkServicePayment()->getNameServicePayment();
                        $paymentList[$payment->getIdPayment()]['timeServicePayment'] = $payment->getFkServicePayment()->getCostServicePayment();
                        $paymentList[$payment->getIdPayment()]['idServicePayment'] = $payment->getFkServicePayment()->getIdServicePayment();
                        $paymentList[$payment->getIdPayment()]['idChat'] = (!empty($payment->getFkChat())) ? $payment->getFkChat()->getIdChat() : false;
                    }
                }
            } else {
                $paymentList = false;
            }
            // GET INFO TBCHAT
            $ownerUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $query->getFkChat()->getIdChat(), "ownerChatUser" => 1]);
            $conclusion['currency'] = $query->getFkChat()->getFkCryptocurrency()->getNameCryptocurrency();
            $conclusion['currencyCode'] = $query->getFkChat()->getFkCryptocurrency()->getCodeCryptocurrency();
            $conclusion['currencyImg'] = $query->getFkChat()->getFkCryptocurrency()->getImageCryptocurrency();
            $conclusion['currencyWeb'] = $query->getFkChat()->getFkCryptocurrency()->getWebCryptocurrency();
            $conclusion['conclusion'] = $query->getFkChat()->getConclusionChat();
            // *************** 
            $endDate = (int)strtotime($query->getFkChat()->getEndDateChat()->modify('-'.$time.' seconds')->format('Y-m-d H:i:s'));
            $chatUser[$query->getFkChat()->getIdChat()] = [
                "id" => $query->getFkChat()->getIdChat(),
                "user" => $ownerUser->getFkUser()->getLoginUser(),
                "userphoto" => ($ownerUser->getFkUser()->getFkGender()->getIdGender() == 1) ? "https://bootdey.com/img/Content/user_1.jpg" : "https://bootdey.com/img/Content/user_2.jpg",
                "descriptionUser" => ($ownerUser->getFkUser()->getDescriptionUser() != null) ? $ownerUser->getFkUser()->getDescriptionUser() : '',
                "created" => $query->getCreatedChatUser()->format('Y-m-d H:i:s'),
                "start" => $query->getFkChat()->getStartDateChat()->format('Y-m-d H:i:s'),
                "end" => $query->getFkChat()->getEndDateChat()->modify('-'.$time.' seconds')->format('Y-m-d H:i:s'),
                "name" => $query->getFkChat()->getNameChat(),
                "price" => $priceArr,
                "description" => $query->getFkChat()->getDescriptionChat(),
                "conclusion" => ((strtotime(date('Y-m-d H:i:s'))) > $endDate) ? $conclusion : null,
                "status" => $query->getFkChat()->getFkStatusChat()->getNameStatusChat(),
                "type" => strtolower($query->getFkChat()->getFkTypeChat()->getNameTypeChat()),
                "ban" => $query->getBanChatUser(),
                "isvalidChat" => $query->getFkChat()->getValidChat(),
                "isvalid" => ($query->getValidChatUser()) ? 1 : 0,
                "isowner" => ($query->getOwnerChatUser()) ? 1 : 0,
                "userList" => $userList,
                "coupon" => $paymentList,
                "time" => date('Y-m-d H:i:s'),
            ];
        }
        return $this->json(array('message' => 'User Chats', 'data' => $chatUser, 'login' => $session['loginUser'], 'code' => 'SU001'), 200);
    }

    public function SearchAction(Request $request) {
        // Captar Json
        $json = json_decode($request->getContent(), true);

        // Valida si es un usuario valido
        $hash = $request->headers->get('Authorization');
        $session = $this->Jwt->ValidateToken($hash, false, true);
        if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
        }
        if (($request->attributes->get('search') == null) || ($request->attributes->get('search') == '*')) {
            $search = '';
        } else {
            $search = $request->attributes->get('search');
        }

        $db = $this->getDoctrine()->getManager();
        $user = $db->getRepository('DbBundle:TbUser')->findOneBy(["idUser" => $session['idUser']]);
        $queryChatUser = $db->createQueryBuilder();
        $queryChatUser->select('cu')
                ->from('DbBundle:TbChatUser', 'cu')
                ->where('cu.fkUser = :id')
                ->setParameter('id', (int) $session['idUser']);

        $chatUser = [];
        foreach ($queryChatUser->getQuery()->getResult() as $key => $query) {
            $chatUser[$query->getFkChat()->getIdChat()] = [
                "id" => $query->getFkChat()->getIdChat(),
                "user" => $query->getFkUser()->getLoginUser(),
                "ban" => $query->getBanChatUser(),
                "isvalid" => $query->getValidChatUser(),
                "isowner" => $query->getOwnerChatUser(),
                "userphoto" => ($query->getFkUser()->getFkGender()->getIdGender() == 1) ? "https://bootdey.com/img/Content/user_1.jpg" : "https://bootdey.com/img/Content/user_2.jpg",
            ];
        }
        $queryChatSearch = $db->createQueryBuilder();
        $queryChatSearch->select('c')
                ->from('DbBundle:TbChat', 'c')
                ->where('(c.endDateChat > CURRENT_TIMESTAMP()) AND (c.validChat = :valid AND c.fkStatusChat = :status) AND (c.nameChat LIKE :search OR c.descriptionChat LIKE :search)')
                ->setParameter('search', '%' . $search . '%')
                ->setParameter('valid', 1)
                ->setParameter('status', 3);


        $chatSearch = [];
        if (empty($queryChatSearch->getQuery()->getResult())) {
            $chatSearch = false;
        }
        foreach ($queryChatSearch->getQuery()->getResult() as $key => $query) {

            $queryPriceChat = $db->createQueryBuilder();
            $queryPriceChat->select('p')
                    ->from('DbBundle:TbPriceChat', 'p')
                    ->where('p.fkChat = :id')
                    ->setParameter('id', (int) $query->getIdChat());
            $priceArr = [];
            foreach ($queryPriceChat->getQuery()->getResult() as $key => $price) {
                $priceArr[$price->getIdPriceChat()] = [
                    'id' => $price->getIdPriceChat(),
                    'value' => $price->getValuePriceChat(),
                    'description' => $price->getDescriptionPriceChat(),
                    'status' => $price->getStatusPriceChat(),
                    'created' => $price->getCreatedPriceChat()->format('Y-m-d H:i:s'),
                    'currency' => $price->getFkCryptocurrency()->getNameCryptocurrency(),
                ];
            }

            $ownerUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $query->getIdChat(), "ownerChatUser" => 1]);
            $chatSearch[$query->getIdChat()] = [
                "id" => $query->getIdChat(),
                "user" => strtoupper($ownerUser->getFkUser()->getLoginUser()),
                "descriptionUser" => ($ownerUser->getFkUser()->getDescriptionUser() != null) ? $ownerUser->getFkUser()->getDescriptionUser() : '',
                "userphoto" => ($ownerUser->getFkUser()->getFkGender()->getIdGender() == 1) ? "https://bootdey.com/img/Content/user_1.jpg" : "https://bootdey.com/img/Content/user_2.jpg",
                "created" => $query->getCreatedChat()->format('Y-m-d H:i:s'),
                "start" => $query->getStartDateChat()->format('Y-m-d H:i:s'),
                "end" => $query->getEndDateChat()->format('Y-m-d H:i:s'),
                "name" => $query->getNameChat(),
                "price" => $priceArr,
                "description" => $query->getDescriptionChat(),
                "conclusion" => $query->getConclusionChat(),
                "status" => $query->getFkStatusChat()->getNameStatusChat(),
                "currency" => $query->getFkCryptocurrency()->getNameCryptocurrency(),
                "type" => strtolower($query->getFkTypeChat()->getNameTypeChat()),
                "isowner" => ((!empty($chatUser[$query->getIdChat()]['isowner'])) && ($chatUser[$query->getIdChat()]['isowner']) && (count($chatUser[$query->getIdChat()]) != 0)) ? 1 : 0,
                "isvalid" => ((!empty($chatUser[$query->getIdChat()]['isvalid'])) && ($chatUser[$query->getIdChat()]['isvalid']) && (count($chatUser[$query->getIdChat()]) != 0)) ? 1 : 0,
                "time" => date('Y-m-d H:i:s'),
            ];
        }
        return $this->json(array('message' => 'Chat Search', 'data' => $chatSearch, 'code' => 'SU001'), 200);
    }

    public function GetInAction(Request $request) {
        // Captar Json
        $json = json_decode($request->getContent(), true);
        /// Valida si es un usuario valido
        $hash = $request->headers->get('Authorization');
        $session = $this->Jwt->ValidateToken($hash, false, true);
        if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
        }

        $db = $this->getDoctrine()->getManager();
        $user = $db->getRepository('DbBundle:TbUser')->findOneBy(["idUser" => $session['idUser']]);

        $queryChatUser = $db->createQueryBuilder();
        $queryChatUser->select('cu')
                ->from('DbBundle:TbChatUser', 'cu')
                ->where('cu.fkUser = :id AND cu.fkChat = :idChat')
                ->setParameter('id', (int) $session['idUser'])
                ->setParameter('idChat', (int) $json['id']);
        $queryChatUser = $queryChatUser->getQuery()->getResult();

        //$queryChat = $db->getRepository('DbBundle:TbChat')->findOneBy(["idChat" => $json['id']]);
        $queryChat = $db->createQueryBuilder();
        $queryChat->select('c')
                ->from('DbBundle:TbChat', 'c')
                ->where('(c.validChat = :valid AND c.fkStatusChat = :status) AND (c.idChat = :id)')
                ->setParameter('id', (int) $json['id'])
                ->setParameter('valid', 1)
                ->setParameter('status', 3);
        $queryChat = $queryChat->getQuery()->getResult();
        $queryChat = $queryChat[0];
        if ((count($queryChatUser) == 0) && (!empty($queryChat)) && (!empty($queryChat))) {
            // Guadar Chat
            $chatUser = new TbChatUser();
            $chatUser->setFkUser($user);
            $chatUser->setFkChat($queryChat);
            $chatUser->setCreatedChatUser(new \DateTime());
            $chatUser->setValidChatUser((($queryChat->getFkTypeChat()->getIdTypeChat() == 2) || ($user->getFkRole()->getIdRole() == 1)) ? 1 : 0 );
            $db->persist($chatUser);
            $db->flush();

            /* return $this->render('chat/index.html.twig', [
              'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
              ]); */
            return $this->json(array('message' => 'Chat Added', 'data' => '', 'code' => 'SU001'), 200);
        } else {
            return $this->json(array('message' => 'Chat Added', 'data' => '', 'code' => 'SU001'), 200);
        }
    }

    public function GetInListAction($chat, Request $request) {
        try {
            $hash = $request->headers->get('Authorization');
            $session = $this->Jwt->ValidateToken($hash, false, true);
            if ($session == false) {
                return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }


            $db = $this->getDoctrine()->getManager();
            $chatUser = $db->getRepository('DbBundle:TbChatUser')->findBy(["fkChat" => $chat]);
            if (count($chatUser)) {
                $userList = [];
                foreach ($chatUser as $user) {
                    if ($user->getValidChatUser() || $user->getOwnerChatUser()) {
                        $userList[$user->getIdChatUser()] = [
                            'idUser' => $user->getFkUser()->getIdUser(),
                            'username' => $user->getFkUser()->getLoginUser(),
                            'isowner' => $user->getOwnerChatUser(),
                            'banned' => $user->getBanChatUser(),
                            'banDescription' => $user->getDescriptionBanChatUser(),
                            'created' => $user->getCreatedChatUser()
                        ];
                    }
                }
                return $this->json(array('message' => 'Chat User List', 'data' => $userList, 'code' => 'SU001'), 200);
            }
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
        }
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
    }

    public function ByIdAction(Request $request) {
        // Captar Json
        $json = json_decode($request->getContent(), true);

        // Valida si es un usuario valido
        $hash = $request->headers->get('Authorization');
        $session = $this->Jwt->ValidateToken($hash, false, true);
        if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
        }

        $idChat = $request->attributes->get('id');

        $db = $this->getDoctrine()->getManager();
        $user = $db->getRepository('DbBundle:TbUser')->findOneBy(["idUser" => $session['idUser']]);

        $queryChatUser = $db->createQueryBuilder();
        $queryChatUser->select('cu')
                ->from('DbBundle:TbChatUser', 'cu')
                ->where('cu.fkUser = :id AND cu.fkChat = :idchat AND (cu.validChatUser = 1 OR cu.ownerChatUser = 1 OR cu.banChatUser = 0)')
                ->setParameter('id', (int) $session['idUser'])
                ->setParameter('idchat', (int) $idChat);        

        $chatUser = [];
        if (count($queryChatUser->getQuery()->getResult()) > 0) {
          foreach ($queryChatUser->getQuery()->getResult() as $key => $query) {

              $time = 0;
              $checkTime = $db->createQueryBuilder();
              $checkTime->select('p')
                      ->from('DbBundle:TbPayment', 'p')
                      ->where('p.fkChat = :idChat AND p.fkUser = :idUser AND p.fkServicePayment > 2')
                      ->setParameter('idChat', (int) $query->getFkChat()->getIdChat())
                      ->setParameter('idUser', (int) $session['idUser']);
              $checkTime = $checkTime->getQuery()->getResult();
              foreach ($checkTime as $checkTime) {
                $time = (int)$checkTime->getFkServicePayment()->getCostServicePayment();
              }
              // GET ALL USERS REGISTERED ON THIS CHAT
              $chatUserList = $db->getRepository('DbBundle:TbChatUser')->findBy(["fkChat" => $query->getFkChat()->getIdChat()]);
              if (count($chatUserList)) {
                  $userList = [];
                  foreach ($chatUserList as $user) {
                      if ($user->getValidChatUser() || $user->getOwnerChatUser()) {
                          $userList[$user->getIdChatUser()] = [
                              'idUser' => $user->getFkUser()->getIdUser(),
                              'username' => $user->getFkUser()->getLoginUser(),
                              'isowner' => $user->getOwnerChatUser(),
                              'banned' => $user->getBanChatUser(),
                              'banDescription' => $user->getDescriptionBanChatUser(),
                              'created' => $user->getCreatedChatUser()
                          ];
                      }
                  }
              }
              // *************************************
              // GET PRICE CHAT
              $queryPriceChat = $db->createQueryBuilder();
              $queryPriceChat->select('p')
                      ->from('DbBundle:TbPriceChat', 'p')
                      ->where('p.fkChat = :id')
                      ->setParameter('id', (int) $query->getFkChat()->getIdChat());
              $priceArr = [];
              foreach ($queryPriceChat->getQuery()->getResult() as $key => $price) {
                  $priceArr[$price->getIdPriceChat()] = [
                      'id' => $price->getIdPriceChat(),
                      'value' => $price->getValuePriceChat(),
                      'description' => $price->getDescriptionPriceChat(),
                      'status' => $price->getStatusPriceChat(),
                      'created' => $price->getCreatedPriceChat()->format('Y-m-d H:i:s'),
                      'currency' => $price->getFkCryptocurrency()->getNameCryptocurrency(),
                  ];
              }
              // **************
              $payments = $db->createQueryBuilder();
              $payments->select('p')
                      ->from('DbBundle:TbPayment', 'p')
                      ->where('p.fkUser = :id')
                      ->setParameter('id', (int) $session['idUser']);
              $payments = $payments->getQuery()->getResult();
              $paymentList = [];
              if (!empty($payments)) {
                  foreach ($payments as $payment) {
                      if ($payment->getFkServicePayment()->getCostServicePayment() != 0) {
                          $paymentList[$payment->getIdPayment()]['nameServicePayment'] = $payment->getFkServicePayment()->getNameServicePayment();
                          $paymentList[$payment->getIdPayment()]['timeServicePayment'] = $payment->getFkServicePayment()->getCostServicePayment();
                          $paymentList[$payment->getIdPayment()]['idServicePayment'] = $payment->getFkServicePayment()->getIdServicePayment();
                          $paymentList[$payment->getIdPayment()]['idChat'] = (!empty($payment->getFkChat())) ? $payment->getFkChat()->getIdChat() : false;
                      }
                  }
              } else {
                  $paymentList = false;
              }
              // GET INFO TBCHAT
              $ownerUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $query->getFkChat()->getIdChat(), "ownerChatUser" => 1]);
              $conclusion['currency'] = $query->getFkChat()->getFkCryptocurrency()->getNameCryptocurrency();
              $conclusion['currencyCode'] = $query->getFkChat()->getFkCryptocurrency()->getCodeCryptocurrency();
              $conclusion['currencyImg'] = $query->getFkChat()->getFkCryptocurrency()->getImageCryptocurrency();
              $conclusion['currencyWeb'] = $query->getFkChat()->getFkCryptocurrency()->getWebCryptocurrency();
              $conclusion['conclusion'] = $query->getFkChat()->getConclusionChat();
              // *************** 

              $endDate = (int)strtotime($query->getFkChat()->getEndDateChat()->modify('-'.$time.' seconds')->format('Y-m-d H:i:s'));
              $chatUser[$query->getFkChat()->getIdChat()] = [
                  "id" => $query->getFkChat()->getIdChat(),
                  "user" => $ownerUser->getFkUser()->getLoginUser(),
                  "userphoto" => ($ownerUser->getFkUser()->getFkGender()->getIdGender() == 1) ? "https://bootdey.com/img/Content/user_1.jpg" : "https://bootdey.com/img/Content/user_2.jpg",
                  "descriptionUser" => ($ownerUser->getFkUser()->getDescriptionUser() != null) ? $ownerUser->getFkUser()->getDescriptionUser() : '',
                  "created" => $query->getCreatedChatUser()->format('Y-m-d H:i:s'),
                  "start" => $query->getFkChat()->getStartDateChat()->format('Y-m-d H:i:s'),
                  "end" => $query->getFkChat()->getEndDateChat()->modify('-'.$time.' seconds')->format('Y-m-d H:i:s'),
                  "name" => $query->getFkChat()->getNameChat(),
                  "price" => $priceArr,
                  "description" => $query->getFkChat()->getDescriptionChat(),
                  "conclusion" => ((strtotime(date('Y-m-d H:i:s'))) > $endDate) ? $conclusion : null,
                  "status" => $query->getFkChat()->getFkStatusChat()->getNameStatusChat(),
                  "type" => strtolower($query->getFkChat()->getFkTypeChat()->getNameTypeChat()),
                  "ban" => $query->getBanChatUser(),
                  "isvalidChat" => $query->getFkChat()->getValidChat(),
                  "isvalid" => ($query->getValidChatUser()) ? 1 : 0,
                  "isowner" => ($query->getOwnerChatUser()) ? 1 : 0,
                  "userList" => $userList,
                  "coupon" => $paymentList,
                  "time" => date('Y-m-d H:i:s'),
              ];
          }

          return $this->json(array('message' => 'User Chats', 'data' => $chatUser, 'login' => $session['loginUser'], 'code' => 'SU001'), 200);
        }

        return $this->json(array('message' => 'Chat Search', 'data' => $chatUser, 'code' => 'SU001'), 200);
    }

    public function IsValidAction(Request $request) {
        // Captar Json
        $json = json_decode($request->getContent(), true);

        // Valida si es un usuario valido
        $hash = $request->headers->get('Authorization');
        $session = $this->Jwt->ValidateToken($hash, false, true);
        if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
        }

        $idChat = $request->attributes->get('id');

        $db = $this->getDoctrine()->getManager();
        $user = $db->getRepository('DbBundle:TbUser')->findOneBy(["idUser" => $session['idUser']]);

        $queryChatUser = $db->createQueryBuilder();
        $queryChatUser->select('cu')
                ->from('DbBundle:TbChatUser', 'cu')
                ->where('cu.fkUser = :id AND cu.fkChat = :idchat AND (cu.validChatUser = 1 OR cu.ownerChatUser = 1)')
                ->setParameter('id', (int) $session['idUser'])
                ->setParameter('idchat', (int) $idChat);
        $chatSearch = [];
        if ((count($queryChatUser->getQuery()->getResult()) > 0) && (!empty($user))) {
            return $this->json(array('message' => 'Validated', 'data' => true, 'code' => 'SU001'), 200);
        } else {
            return $this->json(array('message' => 'Validated', 'data' => false, 'code' => 'SU001'), 200);
        }
    }

    public function BannUserAction(Request $request) {
        // Captar Json
        $json = json_decode($request->getContent(), true);
        $db = $this->getDoctrine()->getManager();

        /// Valida si es un usuario valido
        $hash = $request->headers->get('Authorization');
        $session = $this->Jwt->ValidateToken($hash, false, true);
        if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
        }

        $chatUserAdmin = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkChat" => $json['idChat'], "fkUser" => $session['idUser'], "ownerChatUser" => 1]);
        if (empty($chatUserAdmin)) {
            return $this->json(array("message" => "¡No Posee Privilegios!", "code" => "EC002"), 400);
        }

        $user = $db->getRepository('DbBundle:TbUser')->findOneBy(["loginUser" => $json['loginUser']]);
        if (empty($user)) {
            return $this->json(array("message" => "¡Usuario no encontrado!", "code" => "EC002"), 400);
        }

        $BannChatUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkUser" => (int) $user->getIdUser(), "fkChat" => (int) $chatUserAdmin->getFkChat()->getIdChat()]);
        if ($BannChatUser->getBanChatUser() == 1) {
            $BannChatUser->setBanChatUser(0);
            $data = false;
        } else {
            $BannChatUser->setBanChatUser(1);
            $data = true;
        }

        $db->persist($BannChatUser);
        $db->flush();
        return $this->json(array('message' => 'User Banned', 'data' => $data, 'code' => 'SU001'), 200);
    }

    public function UseCouponAction(Request $request) {
        // Captar Json
        $json = json_decode($request->getContent(), true);
        $db = $this->getDoctrine()->getManager();

        /// Valida si es un usuario valido
        $hash = $request->headers->get('Authorization');
        $session = $this->Jwt->ValidateToken($hash, false, true);
        if ($session == false) {
            return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
        }

        // VERIFICAR SI YA HAY UN CUPON ACTIVO EN ESTE CHAT
        //$payment = $db->getRepository('DbBundle:TbPayment')->findOneBy(["fkChat" => (int) $json['idChat'], "fkUser" => (int) $session['idUser']]);
        $db = $this->getDoctrine()->getManager();
        $payment = $db->createQueryBuilder();
        $payment->select('p')
                ->from('DbBundle:TbPayment', 'p')
                ->where('p.fkChat = :idChat AND p.fkUser = :idUser AND p.fkServicePayment > 2')
                ->setParameter('idChat', (int) $json['idChat'])
                ->setParameter('idUser', (int) $session['idUser']);
        $payment = $payment->getQuery()->getResult();
        if (!empty($payment)) {
            return $this->json(array("message" => "Este chat ya posee un Cupon activo", 'data' => false, "code" => "EC002"), 400);
        }

        $chatUser = $db->getRepository('DbBundle:TbChatUser')->findOneBy(["fkUser" => (int) $session['idUser'], "fkChat" => (int) $json['idChat'], "validChatUser" => (int) 1, "banChatUser" => (int) 0]);
        $payment = $db->getRepository('DbBundle:TbPayment')->findOneBy(["fkServicePayment" => (int) $json['idServicePayment'], "fkUser" => (int) $session['idUser'], "fkChat" => null]);

        if (!empty($chatUser) && !empty($payment)) {
            $payment->setFkChat($chatUser->getFkChat());
            $db->persist($chatUser);
            $db->flush();
            return $this->json(array('message' => 'Coupon Active', 'data' => true, 'time' => $payment->getFkServicePayment()->getCostServicePayment(), 'code' => 'SU001'), 200);
        } else {
            return $this->json(array("message" => "No es posible utilizar este cupon", 'data' => false, "code" => "EC002"), 400);
        }
    }

    public function listCouponBuyAction(Request $request) {
        try {
            $hash = $request->headers->get('Authorization');

            $session = $this->Jwt->ValidateToken($hash, false, true);
            if ($session == false) {
                return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }

            $db = $this->getDoctrine()->getManager();
            $vars = $db->getRepository('DbBundle:TbServicePayment')->findBy(['costServicePayment' => '1', 'statusServicePayment' => 1]);
            $list = [];
            foreach ($vars as $var) {
                $user_list = [
                    "idServicePayment" => $var->getIdServicePayment(),
                    "nameServicePayment" => $var->getNameServicePayment(),
                    "descriptionServicePayment" => $var->getDescriptionServicePayment(),
//                "costServicePayment" => $var->getfkChat()->getFkCryptocurrency()->getNameCryptocurrency(),
//                "statusServicePayment" => $var->getfkChat()->getfkStatusChat()->getNameStatusChat(),
                ];
                $list[] = $user_list;
                unset($user_list);
            }
            return $this->json(array("message" => "Listado Cupones", "data" => $list, "code" => "SU001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
        }
    }

    public function listMyCouponAction(Request $request) {
        try {
            $hash = $request->headers->get('Authorization');

            $session = $this->Jwt->ValidateToken($hash, false, true);
            if ($session == false) {
                return $this->json(array("message" => "¡Token Invalido!", "code" => "TKN"), 401);
            }

            $db = $this->getDoctrine()->getManager();
            $payments = $db->createQueryBuilder();
            $payments->select('p')
                    ->from('DbBundle:TbPayment', 'p')
                    ->where('p.fkUser = :id')
                    ->setParameter('id', (int) $session['idUser']);
            $payments = $payments->getQuery()->getResult();
            $paymentList = [];
            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($payment->getFkServicePayment()->getCostServicePayment() != 0) {
                        $paymentList[] = [
                            'nameServicePayment' => $payment->getFkServicePayment()->getNameServicePayment(),
                            'descriptionServicePayment' => $payment->getFkServicePayment()->getDescriptionServicePayment(),
                            "timeServicePayment" => $payment->getFkServicePayment()->getCostServicePayment(),
                            'idServicePayment' => $payment->getFkServicePayment()->getIdServicePayment(),
                            'validPayment' => $payment->getValidPayment(),
                            'createdPayment' => $payment->getCreatedPayment()->format('Y-m-d H:i:s'),
                            'idChat' => (!empty($payment->getFkChat())) ? $payment->getFkChat()->getIdChat() : false,
                            'nameChat' => (!empty($payment->getFkChat())) ? $payment->getFkChat()->getNameChat() : false
                        ];
                    }
                }
            } else {
                $paymentList = false;
            }
            return $this->json(array("message" => "Listado Mis Cupones", "data" => $paymentList, "code" => "SU001"), 200);
        } catch (Exception $e) {
            return $this->json(array("message" => "¡Error Desconocido!", "code" => "EU001"), 400);
        }
    }

}
