<?php

namespace AppBundle\Topic;

use Doctrine\ORM\EntityManager;

use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;

use AppBundle\Services\Helper;
use AppBundle\Services\JwtAuth;

class ChatChannelTopic implements TopicInterface
{
    protected $clientManipulator;
    private $manager;

    private $Jwt;
    private $Helper;

    public $user;
    public $userChat;
    public $token;
    private $chatUser;
    private $Chat;

    private $aux;
    public function __construct(ClientManipulatorInterface $clientManipulator, EntityManager $manager, JwtAuth $jwt_auth, Helper $helper)
    {
        $this->clientManipulator = $clientManipulator;
        $this->manager = $manager;
        $this->user = [];
        $this->chatUser = [];
        $this->Chat;
        $this->token;
        $this->userChat = null;

        $this->aux = 'Instanced!!!';


        $this->Jwt = $jwt_auth;
        $this->Helper = $helper;
    }

    public function validateMember (ConnectionInterface $connection, Topic $topic, WampRequest $request) {
            //$this->manager->getConnection()->close();
            //$this->manager->getConnection()->connect();
            $routeMatched = explode('/',$topic->getId());
            $chat = $routeMatched[2];
            $data = $connection->WebSocket->request->getQuery()->toArray();
            $this->token = $data['t'];
            /*try {
                $session = $this->Jwt->ValidateToken($this->token, false, true);
            } catch (Exception $e) {
                $db = $this->manager;
                $this->manager->getConnection()->close();
                $this->manager->getConnection()->connect();
            }*/
            
            $session = $this->Jwt->ValidateToken($this->token, false, true);
            if ($session == false) {
                return false;
            }


            $chatQuery = $this->manager->createQueryBuilder();
            $chatQuery->select('c')
                  ->from('DbBundle:TbChat', 'c')
                  ->where('(c.validChat = :valid AND c.fkStatusChat = :status) AND (c.idChat = :id)')
                  ->setParameter('id', (int) $chat)
                  ->setParameter('valid', 1)
                  ->setParameter('status', 3);
            $chatQuery = $chatQuery->getQuery()->getResult();

            if (count($chatQuery)) {
                foreach ($chatQuery as $chat) {
                    $this->Chat = $chat;
                }

                $userQuery = $this->manager->getRepository('DbBundle:TbUser')->findBy(["idUser" => $session['idUser']]);
                foreach ($userQuery as $userQ) {
                    $this->user = [
                        "id" => $userQ->getIdUser(),
                        "name" => $userQ->getNameUser(),
                        "login" => $userQ->getLoginUser(),
                    ];
                }   
                if (isset($this->user)) {
                    $this->userChat = $this->manager->getRepository('DbBundle:TbChatUser')->findBy(["fkUser" => $this->user['id'] , "fkChat" => $chat]);
                    if (count($this->userChat)) {
                        foreach ($this->userChat as $chat) {
                            $this->userChat = [
                                "id" => $chat->getIdChatUser(),
                                "login" => $chat->getFkUser()->getLoginUser(),
                                "user" => $chat->getFkUser()->getNameUser(),
                                "chat" => $chat->getFkChat()->getNameChat(),
                                "created_date" => $chat->getCreatedChatUser(),
                                "ban" => $chat->getBanChatUser(),
                                "description_ban" => $chat->getDescriptionBanChatUser(),
                            ];
                        }
                    }else{
                        $this->userChat = false;
                    }  
                } else {
                    $this->user = false;
                }
            }else{
                $this->userChat = false;
            }  
            $this->manager->clear();
            $this->manager->getConnection()->close();
            if ( (count($this->user)) && ($this->userChat != false) && ($this->userChat['ban']==false) ) {
                return true;
            }else{
                return false;
            }
        
    }
    public function validateMessage ($msg, $channel, $type, $username, $history = false) {
        
        $fileSystem = new Filesystem();
        $time = strtotime(date('d-m-Y h:m:s'));
        if ($history == true) {
            if (!$fileSystem->exists('web/chats/'.$channel.'.txt')) {
                return false;
            }else{
                //var_dump(json_decode('{'.file_get_contents(__DIR__.'/../../../web/chats/'.$channel.'.txt').'}'));
                return '{'.file_get_contents(__DIR__.'/../../../web/chats/'.$channel.'.txt').'}';
            }
        }
        if ($type!='connection') {
            if (!$fileSystem->exists('web/chats/'.$channel.'.txt')) {
                $fileSystem->dumpFile('web/chats/'.$channel.'.txt', '');
                $fileSystem->appendToFile('web/chats/'.$channel.'.txt','"0":{"channel": "'.$channel.'","type": "'.$type.'","username": "'.$username.'","body": "'.strip_tags($msg).'"}');
            }else{
                $key = json_decode('{'.file_get_contents(__DIR__.'/../../../web/chats/'.$channel.'.txt').'}');
                $key = count((array)$key);
                $fileSystem->appendToFile('web/chats/'.$channel.'.txt',',"'.$key.'":{"channel": "'.$channel.'","type": "'.$type.'","username": "'.$username.'","body": "'.strip_tags($msg).'"}');
            }
        }

        return strip_tags($msg);
    }

    /**
     * This will receive any Subscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */

    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        $userInChat = $topic->count();
        $routeMatched = explode('/',$topic->getId());
        $chat = $routeMatched[2];
        if ( $this->validateMember($connection, $topic, $request) ) {

            $msgFormat = '%s is connected';
            $args = [
                'type' => 'connection',
                'token' => $this->token,
                'ban' => $this->userChat['ban'],
                'channel' => $this->Chat->getIdChat(),
                'username' => $this->user['login'],
                'oldMsg' => false,//$this->validateMessage(sprintf($msgFormat, $this->user['login']), $this->Chat->getIdChat(), 'connection', $this->user['login'], true),
                'body' => $this->validateMessage(sprintf($msgFormat, $this->user['login']), $this->Chat->getIdChat(), 'connection', $this->user['login'])
            ];
            $topic->broadcast(
                $args,
                array(), // EXCLUDE CHANNEL 
                array() // ELEGIBLE CHANNEL
            );

        }else{
            // SOMETHING TO DISCONNECT
            $topic->remove($connection);
        }

    }

    /**
     * This will receive any UnSubscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        /*$user = $this->clientManipulator->getClient($connection);
        $username = '';
        $msgFormat = '%s is disconnected';
        if ($user instanceof User) {
            $username = $user->getUsername();
        } else {
            $username = $user;
        }*/
        if ( $this->validateMember($connection, $topic, $request) ) {
            //this will broadcast the message to ALL subscribers of this topic.
            $topic->broadcast([
                'type' => 'disconnection',
                'username' => $this->user['login'],
                //'body' => sprintf($msgFormat, $username)
            ]);
        }else{
            //this will broadcast the message to ALL subscribers of this topic.
            $topic->broadcast([
                'type' => 'disconnection',
                //'body' => sprintf($msgFormat, $username)
            ]);
        }
        
        $topic->remove($connection);

    }


    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param $event
     * @param array $exclude
     * @param array $eligible
     * @return mixed|void
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {
        
        if ( $this->validateMember($connection, $topic, $request) ) {

            $args = [
                'type' => 'message',
                'token' => $this->token,
                'channel' => $this->Chat->getIdChat(),
                'username' => $this->user['login'],
                'body' => $this->validateMessage($event, $this->Chat->getIdChat(), 'message', $this->user['login'])
            ];

            $topic->broadcast(
                $args,
                array(), // EXCLUDE CHANNEL 
                array() // ELEGIBLE CHANNEL
            );
        }else{
            // SOMETHING TO DISCONNECT
            $routeMatched = explode('/',$topic->getId());
            $chat = $routeMatched[2];
            $topic->remove($connection);
            $args = [
                'type' => 'message',
                'token' => $this->token,
                'channel' => $chat,
                'body' => 'Error Channel'
            ];   

        }

    }

    /**
    * Like RPC is will use to prefix the channel
    * @return string
    */
    public function getName()
    {
        return 'chat.channel.topic';
    }
}