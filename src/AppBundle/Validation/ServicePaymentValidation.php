<?php

namespace AppBundle\Validation;

//use Symfony\Component\Validator\Validation;
//use Symfony\Component\Validator\Constraints\Length;
//use Symfony\Component\Validator\Constraints\NotBlank;

class ServicePaymentValidation {

    public $array_message;

    public function __construct() {
//        $this->array_message = null;
    }

    private function descriptionServicePayment($descriptionServicePayment) {
        $error = [];
        if (!isset($descriptionServicePayment) or $descriptionServicePayment == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["descriptionServicePayment"] = $error;
        }
        return trim($descriptionServicePayment);
    }
    private function nameServicePayment($nameServicePayment) {
        $error = [];
        if (!isset($nameServicePayment) or $nameServicePayment == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["nameServicePayment"] = $error;
        }
        return trim($nameServicePayment);
    }

    private function descriptionPriceServicePayment($descriptionPriceServicePayment) {
      $error = [];
      if (!empty($error)) {
         $this->array_message ["error"]["descriptionPriceServicePayment"] = $error;
      }
      return trim($descriptionPriceServicePayment);
   }

   private function valuePriceServicePayment($valuePriceServicePayment) {
      $error = [];
      if (!isset($valuePriceServicePayment) or $valuePriceServicePayment == "") {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["valuePriceServicePayment"] = $error;
      }
      return trim($valuePriceServicePayment);
   }

   private function cryptocurrency($cryptocurrency) {
      $error = [];
      if (!isset($cryptocurrency) or $cryptocurrency == "") {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["cryptocurrency"] = $error;
      }
      return trim($cryptocurrency);
   }

    // Funciones
    public function newAction($valores) {

        // Verificar si los campos estan seteados
        $descriptionServicePayment = (isset($valores["descriptionServicePayment"])) ? $valores["descriptionServicePayment"] : null;
        $nameServicePayment = (isset($valores["nameServicePayment"])) ? $valores["nameServicePayment"] : null;
        // Validar
        $this->array_message ["value"]["descriptionServicePayment"] = $this->descriptionServicePayment($descriptionServicePayment);
        $this->array_message ["value"]["nameServicePayment"] = $this->nameServicePayment($nameServicePayment);
        if (isset($valores["idServicePayment"])) {
            $this->array_message ["value"]["idServicePayment"] = $valores["idServicePayment"];
        }

            // - Datos de Pago
          if (!empty($valores["priceServicePayment"]) > 0) {
             foreach ($valores["priceServicePayment"] as $key => $value) {
                $valuePriceServicePayment = (isset($value['valuePriceServicePayment'])) ? $value['valuePriceServicePayment'] : null;
                $descriptionPriceServicePayment = (isset($value['descriptionPriceServicePayment'])) ? $value['descriptionPriceServicePayment'] : null;
                $cryptocurrency_ = (isset($value['cryptocurrency'])) ? $value['cryptocurrency'] : null;
                $this->array_message ["value"]["priceServicePayment"][$key]["valuePriceServicePayment"] = $this->valuePriceServicePayment($valuePriceServicePayment);
                $this->array_message ["value"]["priceServicePayment"][$key]["descriptionPriceServicePayment"] = $this->descriptionPriceServicePayment($descriptionPriceServicePayment);
                $this->array_message ["value"]["priceServicePayment"][$key]["statusPriceServicePayment"] = 1;
                $this->array_message ["value"]["priceServicePayment"][$key]["cryptocurrency"] = (int) $this->cryptocurrency($cryptocurrency_);
             }
          } else {
             $this->array_message ["value"]["priceServicePayment"] = null;
          }

        return $this->array_message;
    }

}
