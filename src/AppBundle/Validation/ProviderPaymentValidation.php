<?php

namespace AppBundle\Validation;

//use Symfony\Component\Validator\Validation;
//use Symfony\Component\Validator\Constraints\Length;
//use Symfony\Component\Validator\Constraints\NotBlank;

class ProviderPaymentValidation {

    public $array_message;

    public function __construct() {
//        $this->array_message = null;
    }

    private function descriptionProviderPayment($descriptionProviderPayment) {
        $error = [];
        if (!isset($descriptionProviderPayment) or $descriptionProviderPayment == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["descriptionProviderPayment"] = $error;
        }
        return trim($descriptionProviderPayment);
    }
    private function nameProviderPayment($nameProviderPayment) {
        $error = [];
        if (!isset($nameProviderPayment) or $nameProviderPayment == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["nameProviderPayment"] = $error;
        }
        return trim($nameProviderPayment);
    }
    private function urlProviderPayment($urlProviderPayment) {
        $error = [];
        if (!isset($urlProviderPayment) or $urlProviderPayment == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["urlProviderPayment"] = $error;
        }
        return trim($urlProviderPayment);
    }


    // Funciones
    public function newAction($valores) {

        // Verificar si los campos estan seteados
        $descriptionProviderPayment = (isset($valores["descriptionProviderPayment"])) ? $valores["descriptionProviderPayment"] : null;
        $nameProviderPayment = (isset($valores["nameProviderPayment"])) ? $valores["nameProviderPayment"] : null;
        $urlProviderPayment = (isset($valores["urlProviderPayment"])) ? $valores["urlProviderPayment"] : null;
        // Validar
        $this->array_message ["value"]["descriptionProviderPayment"] = $this->descriptionProviderPayment($descriptionProviderPayment);
        $this->array_message ["value"]["nameProviderPayment"] = $this->nameProviderPayment($nameProviderPayment);
        $this->array_message ["value"]["urlProviderPayment"] = $this->urlProviderPayment($urlProviderPayment);

        return $this->array_message;
    }

}
