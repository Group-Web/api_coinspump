<?php

namespace AppBundle\Validation;

use Doctrine\ORM\EntityManager;

class ChatValidation {

   public $array_message;
   public $manager;

   public function __construct(EntityManager $manager) {
      $this->manager = $manager;
   }

   private function nameChat($nameChat) {
      $error = [];
      if (!isset($nameChat) or $nameChat == "") {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["nameChat"] = $error;
      }
      return trim($nameChat);
   }

   private function startDateChat($startDateChat) {
      $error = [];
      if (!isset($startDateChat) or $startDateChat == "") {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["startDateChat"] = $error;
      }
      return trim($startDateChat);
   }

   private function endDateChat($endDateChat) {
      $error = [];
      if (!isset($endDateChat) or $endDateChat == "") {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["endDateChat"] = $error;
      }
      return trim($endDateChat);
   }

   private function descriptionChat($descriptionChat) {
      $error = [];
//        if (!isset($descriptionChat) or $descriptionChat == "") {
//            $error[] = "Campo Vacio";
//        }
      if (!empty($error)) {
         $this->array_message ["error"]["descriptionChat"] = $error;
      }
      return trim($descriptionChat);
   }

   private function conclusionChat($conclusionChat) {
      $error = [];
//        if (!isset($conclusionChat) or $conclusionChat == "") {
//            $error[] = "Campo Vacio";
//        }
      if (!empty($error)) {
         $this->array_message ["error"]["conclusionChat"] = $error;
      }
      return trim($conclusionChat);
   }

   private function typeChat($typeChat) {
      $error = [];
      $db = $this->manager->getRepository('DbBundle:TbTypeChat')->findBy(["idTypeChat" => $typeChat]);
      if (count($db) < 1) {
         $error = "Este tipo de chat no existe";
      }
      if (!isset($typeChat) or $typeChat == "" or $typeChat < 1) {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["typeChat"] = $error;
      }
      return trim($typeChat);
   }

   private function cryptocurrency($cryptocurrency) {
      $error = [];
      $db = $this->manager->getRepository('DbBundle:TbCryptocurrency')->findBy(["idCryptocurrency" => $cryptocurrency]);
      if (count($db) < 1) {
         $error = "no existe esta moneda";
      }
      if (!isset($cryptocurrency) or $cryptocurrency == "" or $cryptocurrency < 1) {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["cryptocurrency"] = $error;
      }
      return trim($cryptocurrency);
   }

   private function valuePriceChat($valuePriceChat) {
      $error = [];
      if (!isset($valuePriceChat) or $valuePriceChat == "") {
         $error[] = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["valuePriceChat"] = $error;
      }
      return trim($valuePriceChat);
   }

   private function descriptionPriceChat($descriptionPriceChat) {
      $error = [];
//        if (!isset($descriptionPriceChat) or $descriptionPriceChat == "") {
//            $error[] = "Campo Vacio";
//        }
      if (!empty($error)) {
         $this->array_message ["error"]["descriptionPriceChat"] = $error;
      }
      return trim($descriptionPriceChat);
   }

   // Funciones
   public function newChatAction($valores, $admin) {
      // - Datos Generales del chat
      // - - Verificar si los campos estan seteados
      $nameChat = (isset($valores["nameChat"])) ? $valores["nameChat"] : null;
      $startDateChat = (isset($valores["startDateChat"])) ? $valores["startDateChat"] : null;
      $endDateChat = (isset($valores["endDateChat"])) ? $valores["endDateChat"] : null;
      $descriptionChat = (isset($valores["descriptionChat"])) ? $valores["descriptionChat"] : null;
      $conclusionChat = (isset($valores["conclusionChat"])) ? $valores["conclusionChat"] : null;
      $cryptocurrency = (isset($valores["cryptocurrency"])) ? $valores["cryptocurrency"] : null;
      // - - Si es un usuario comun siempre sera un chat premium, en cambio el admin si puede escoger
      if ($admin == 1) {
         $typeChat = (isset($valores["typeChat"])) ? $valores["typeChat"] : 1;
         $statusChat = 3;
      } else {
         $typeChat = 1;
         $statusChat = 1;
      }
      // - - Validar datos y formato
      $this->array_message ["value"]["nameChat"] = $this->nameChat($nameChat);
      $this->array_message ["value"]["endDateChat"] = $this->endDateChat($endDateChat);
      $this->array_message ["value"]["startDateChat"] = $this->startDateChat($startDateChat);
      $this->array_message ["value"]["descriptionChat"] = $this->descriptionChat($descriptionChat);
      $this->array_message ["value"]["conclusionChat"] = $this->conclusionChat($conclusionChat);
      $this->array_message ["value"]["typeChat"] = (int) $this->typeChat($typeChat);
      $this->array_message ["value"]["cryptocurrency"] = (int) $this->cryptocurrency($cryptocurrency);
      $this->array_message ["value"]["statusChat"] = $statusChat;
      $this->array_message ["value"]["validChat"] = ($admin == 1) ? 1 : 0;
      // - Datos de Pagos chat
      if (!empty($valores["priceChat"]) > 0) {
         foreach ($valores["priceChat"] as $key => $value) {
            $valuePriceChat = (isset($value['valuePriceChat'])) ? $value['valuePriceChat'] : null;
            $descriptionPriceChat = (isset($value['descriptionPriceChat'])) ? $value['descriptionPriceChat'] : null;
            $cryptocurrency_ = (isset($value['cryptocurrency'])) ? $value['cryptocurrency'] : null;
            $this->array_message ["value"]["priceChat"][$key]["valuePriceChat"] = $this->valuePriceChat($valuePriceChat);
            $this->array_message ["value"]["priceChat"][$key]["descriptionPriceChat"] = $this->descriptionPriceChat($descriptionPriceChat);
            $this->array_message ["value"]["priceChat"][$key]["statusPriceChat"] = 1;
            $this->array_message ["value"]["priceChat"][$key]["cryptocurrency"] = (int) $this->cryptocurrency($cryptocurrency_);
         }
      } else {
         $this->array_message ["value"]["priceChat"] = null;
      }
      // - Retornar Valores validados
      return $this->array_message;
   }

}
