<?php

namespace AppBundle\Validation;

//use Symfony\Component\Validator\Validation;
//use Symfony\Component\Validator\Constraints\Length;
//use Symfony\Component\Validator\Constraints\NotBlank;

class PaymentValidation {

    public $array_message;

    public function __construct() {
//        $this->array_message = null;
    }

    private function tokenUP($tokenUP) {
        $error = [];
        if (!isset($tokenUP) or $tokenUP == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["tokenUP"] = $error;
        }
        return trim($tokenUP);
    }

    private function otpUP($otpUP) {
        $error = [];
        if (!isset($otpUP) or $otpUP == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["otpUP"] = $error;
        }
        return trim($otpUP);
    }

    private function cuentaOriginUP($cuentaOriginUP) {
        $error = [];
        if (!isset($cuentaOriginUP) or $cuentaOriginUP == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["cuentaOriginUP"] = $error;
        }
        return trim($cuentaOriginUP);
    }

    private function idTransactionUP($idTransactionUP) {
        $error = [];
        if (!isset($idTransactionUP) or $idTransactionUP == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["idTransactionUP"] = $error;
        }
        return trim($idTransactionUP);
    }

    private function messageUP($messageUP) {
        $error = [];
//        if (!isset($messageUP) or $messageUP == "") {
//            $error[] = "Campo Vacio";
//        }
        if (!empty($error)) {
            $this->array_message ["error"]["messageUP"] = $error;
        }
        return trim($messageUP);
    }

    private function idChat($idChat) {
        $error = [];
        if (!isset($idChat) or $idChat == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["idChat"] = $error;
        }
        return trim($idChat);
    }

    private function ammountPayment($ammountPayment) {
        $error = [];
        if (!isset($ammountPayment) or $ammountPayment == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["ammountPayment"] = $error;
        }
        return trim($ammountPayment);
    }

    private function descriptionPayment($descriptionPayment) {
        $error = [];
//        if (!isset($descriptionPayment) or $descriptionPayment == "") {
//            $error[] = "Campo Vacio";
//        }
        if (!empty($error)) {
            $this->array_message ["error"]["descriptionPayment"] = $error;
        }
        return trim($descriptionPayment);
    }

    private function typePayment($typePayment) {
        $error = [];
        if (!isset($typePayment) or $typePayment == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["typePayment"] = $error;
        }
        return trim($typePayment);
    }

    // Funciones
    public function newChatAction($valores) {

        // Verificar si los campos estan seteados
        $tokenUP = (isset($valores["tokenUP"])) ? $valores["tokenUP"] : null;
        $otpUP = (isset($valores["otpUP"])) ? $valores["otpUP"] : null;
        $cuentaOriginUP = (isset($valores["cuentaOriginUP"])) ? $valores["cuentaOriginUP"] : null;
        $idTransactionUP = (isset($valores["idTransactionUP"])) ? $valores["idTransactionUP"] : null;
        $messageUP = (isset($valores["messageUP"])) ? $valores["messageUP"] : null;
        $idChat = (isset($valores["idChat"])) ? $valores["idChat"] : null;
        $ammountPayment = (isset($valores["ammountPayment"])) ? $valores["ammountPayment"] : null;
        $descriptionPayment = (isset($valores["descriptionPayment"])) ? $valores["descriptionPayment"] : null;
        $typePayment = (isset($valores["typePayment"])) ? $valores["typePayment"] : null;
        $validChatUser = 1; // Ya que siempre que se crea es valido el chat
        $fkServicePayment = 1; // Ya que creamos es un chat nuevo y siempre es 1 xq no tiene costo y esta prefeinido
        // Validar
        $this->array_message ["value"]["tokenUP"] = $this->tokenUP($tokenUP);
        $this->array_message ["value"]["otpUP"] = $this->otpUP($otpUP);
        $this->array_message ["value"]["cuentaOriginUP"] = $this->cuentaOriginUP($cuentaOriginUP);
        $this->array_message ["value"]["idTransactionUP"] = $this->idTransactionUP($idTransactionUP);
        $this->array_message ["value"]["messageUP"] = $this->messageUP($messageUP);
        $this->array_message ["value"]["idChat"] = (int) $this->idChat($idChat);
        $this->array_message ["value"]["ammountPayment"] = $this->ammountPayment($ammountPayment);
        $this->array_message ["value"]["descriptionPayment"] = $this->descriptionPayment($descriptionPayment);
        $this->array_message ["value"]["typePayment"] = (int) $this->typePayment($typePayment);
        $this->array_message ["value"]["validChatUser"] = (int) $validChatUser;
        $this->array_message ["value"]["fkServicePayment"] = (int) $fkServicePayment;

        return $this->array_message;
    }

}
