<?php

namespace AppBundle\Validation;

//use Symfony\Component\Validator\Validation;
//use Symfony\Component\Validator\Constraints\Length;
//use Symfony\Component\Validator\Constraints\NotBlank;

class CryptocurrencyValidation {

    public $array_message;

    public function __construct() {
//        $this->array_message = null;
    }

    private function nameCryptocurrency($nameCryptocurrency) {
        $error = [];
        if (!isset($nameCryptocurrency) or $nameCryptocurrency == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["nameCryptocurrency"] = $error;
        }
        return trim($nameCryptocurrency);
    }
    private function codeCryptocurrency($codeCryptocurrency) {
        $error = [];
        if (!isset($codeCryptocurrency) or $codeCryptocurrency == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["codeCryptocurrency"] = $error;
        }
        return trim($codeCryptocurrency);
    }
    private function imageCryptocurrency($imageCryptocurrency) {
        $error = [];
        if (!isset($imageCryptocurrency) or $imageCryptocurrency == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["imageCryptocurrency"] = $error;
        }
        return trim($imageCryptocurrency);
    }
    private function webCryptocurrency($webCryptocurrency) {
        $error = [];
        if (!isset($webCryptocurrency) or $webCryptocurrency == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["webCryptocurrency"] = $error;
        }
        return trim($webCryptocurrency);
    }


    // Funciones
    public function newAction($valores) {

        // Verificar si los campos estan seteados
        $nameCryptocurrency = (isset($valores["nameCryptocurrency"])) ? $valores["nameCryptocurrency"] : null;
        $codeCryptocurrency = (isset($valores["codeCryptocurrency"])) ? $valores["codeCryptocurrency"] : null;
        $imageCryptocurrency = (isset($valores["imageCryptocurrency"])) ? $valores["imageCryptocurrency"] : null;
        $webCryptocurrency = (isset($valores["webCryptocurrency"])) ? $valores["webCryptocurrency"] : null;
        // Validar
        $this->array_message ["value"]["nameCryptocurrency"] = $this->nameCryptocurrency($nameCryptocurrency);
        $this->array_message ["value"]["codeCryptocurrency"] = $this->codeCryptocurrency($codeCryptocurrency);
        $this->array_message ["value"]["imageCryptocurrency"] = $this->imageCryptocurrency($imageCryptocurrency);
        $this->array_message ["value"]["webCryptocurrency"] = $this->webCryptocurrency($webCryptocurrency);

        return $this->array_message;
    }

}
