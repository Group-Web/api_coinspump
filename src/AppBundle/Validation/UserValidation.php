<?php

namespace AppBundle\Validation;

use Doctrine\ORM\EntityManager;

class UserValidation {

   public $manager;
   public $array_message;

   public function __construct(EntityManager $manager) {
      $this->manager = $manager;
   }

   private function nameUser($nameUser) {

      if (!isset($nameUser) or $nameUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["nameUser"] = $error;
      }
      return trim($nameUser);
   }

   private function lastnameUser($lastnameUser) {

      if (!isset($lastnameUser) or $lastnameUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["lastnameUser"] = $error;
      }
      return trim($lastnameUser);
   }

   private function loginUser($loginUser) {
      $users = $this->manager->getRepository('DbBundle:TbUser')->findBy(["loginUser" => $loginUser]);
      if (count($users) > 0) {
         $error = "Ya existe este nombre";
      }
      if (!isset($loginUser) or $loginUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["loginUser"] = $error;
      }
      return trim($loginUser);
   }

   private function emailUser($emailUser) {
      $users = $this->manager->getRepository('DbBundle:TbUser')->findBy(["emailUser" => $emailUser]);
      if (count($users) > 0) {
         $error = "Ya existe este email";
      }
      if (!isset($emailUser) or $emailUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["emailUser"] = $error;
      }
      return trim($emailUser);
   }

   private function birthdayUser($birthdayUser) {

      if (!isset($birthdayUser) or $birthdayUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["birthdayUser"] = $error;
      }
      return trim($birthdayUser);
   }

   private function addressUser($addressUser) {

//        if (!isset($addressUser) or $addressUser == "") {
//           $error = "Campo Vacio";
//        }
      if (!empty($error)) {
         $this->array_message ["error"]["addressUser"] = $error;
      }
      return trim($addressUser);
   }

   private function celUser($celUser) {

      if (!isset($celUser) or $celUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["celUser"] = $error;
      }
      return trim($celUser);
   }

   private function phoneUser($phoneUser) {

//        if (!isset($addressUser) or $addressUser == "") {
//           $error = "Campo Vacio";
//        }
      if (!empty($error)) {
         $this->array_message ["error"]["phoneUser"] = $error;
      }
      return trim($phoneUser);
   }

   private function referenceUser($referenceUser) {


      if (!empty($error)) {
         $this->array_message ["error"]["referenceUser"] = $error;
      }
      return trim($referenceUser);
   }

   private function fkRoleUser($fkRoleUser) {
      $db = $this->manager->getRepository('DbBundle:TbRole')->findBy(["idRole" => $fkRoleUser]);
      if (count($db) < 1) {
         $error = "Este Role no existe";
      }
      if (!isset($fkRoleUser) or $fkRoleUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["fkRoleUser"] = $error;
      }
      return trim($fkRoleUser);
   }

   private function fkCountryUser($fkCountryUser) {
      $country = $this->manager->getRepository('DbBundle:TbCountry')->findBy(["idCountry" => $fkCountryUser]);
      if (count($country) < 1) {
         $error = "Este Pais no es correcto";
      }
      if (!isset($fkCountryUser) or $fkCountryUser == "" or $fkCountryUser == "0") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["countryUser"] = $error;
      }
      return trim($fkCountryUser);
   }

   private function fkGenderUser($fkGenderUser) {
      $db = $this->manager->getRepository('DbBundle:TbGender')->findBy(["idGender" => $fkGenderUser]);
      if (count($db) < 1) {
         $error = "Este genero no existe";
      }
      if (!isset($fkGenderUser) or $fkGenderUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["fkGenderUser"] = $error;
      }
      return trim($fkGenderUser);
   }

   private function passwordUser($passwordUser) {
      if (!isset($passwordUser) or $passwordUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["passwordUser"] = $error;
      }
      return trim($passwordUser);
   }

   private function loginEmailUser($loginUser) {
      $login = $this->manager->getRepository('DbBundle:TbUser')->findBy(["loginUser" => $loginUser]);
      if (count($login) < 1) {
         $email = $this->manager->getRepository('DbBundle:TbUser')->findBy(["emailUser" => $loginUser]);
         if (count($email) < 1) {
            $error = "No existe este login de usuario";
         }
      }
      if (!isset($loginUser) or $loginUser == "") {
         $error = "Campo Vacio";
      }
      if (!empty($error)) {
         $this->array_message ["error"]["loginUser"] = $error;
      }
      return trim($loginUser);
   }

   // Funciones
   public function newAction($valores) {

      // Verificar si los campos estan seteados
      $nameUser = (isset($valores["nameUser"])) ? $valores["nameUser"] : null;
      $lastnameUser = (isset($valores["lastnameUser"])) ? $valores["lastnameUser"] : null;
      $loginUser = (isset($valores["loginUser"])) ? $valores["loginUser"] : null;
      $emailUser = (isset($valores["emailUser"])) ? $valores["emailUser"] : null;
      $birthdayUser = (isset($valores["birthdayUser"])) ? $valores["birthdayUser"] : null;
      $addressUser = (isset($valores["addressUser"])) ? $valores["addressUser"] : null;
      $celUser = (isset($valores["celUser"])) ? $valores["celUser"] : null;
      $phoneUser = (isset($valores["phoneUser"])) ? $valores["phoneUser"] : null;
      $referenceUser = (isset($valores["referenceUser"])) ? $valores["referenceUser"] : 1;
      $fkRoleUser = 2;
      $fkCountryUser = (isset($valores["fkCountryUser"])) ? $valores["fkCountryUser"] : null;
      $fkGenderUser = (isset($valores["fkGenderUser"])) ? $valores["fkGenderUser"] : null;
      $passwordUser = (isset($valores["passwordUser"])) ? $valores["passwordUser"] : null;

      // Validar
      $this->array_message ["value"]["nameUser"] = $this->nameUser($nameUser);
      $this->array_message ["value"]["lastnameUser"] = $this->lastnameUser($lastnameUser);
      $this->array_message ["value"]["loginUser"] = $this->loginUser($loginUser);
      $this->array_message ["value"]["emailUser"] = $this->emailUser($emailUser);
      $this->array_message ["value"]["birthdayUser"] = $this->birthdayUser($birthdayUser);
      $this->array_message ["value"]["addressUser"] = $this->addressUser($addressUser);
      $this->array_message ["value"]["celUser"] = $this->celUser($celUser);
      $this->array_message ["value"]["phoneUser"] = $this->phoneUser($phoneUser);
      $this->array_message ["value"]["referenceUser"] = (int) $this->referenceUser($referenceUser);
      $this->array_message ["value"]["fkRoleUser"] = (int) $this->fkRoleUser($fkRoleUser);
      $this->array_message ["value"]["fkCountryUser"] = (int) $this->fkCountryUser($fkCountryUser);
      $this->array_message ["value"]["fkGenderUser"] = (int) $this->fkGenderUser($fkGenderUser);
      $this->array_message ["value"]["passwordUser"] = $this->passwordUser($passwordUser);

      return $this->array_message;
   }

   public function newAdminAction($valores) {
      // Verificar si los campos estan seteados
      $nameUser = (isset($valores["nameUser"])) ? $valores["nameUser"] : null;
      $lastnameUser = (isset($valores["lastnameUser"])) ? $valores["lastnameUser"] : null;
      $loginUser = (isset($valores["loginUser"])) ? $valores["loginUser"] : null;
      $emailUser = (isset($valores["emailUser"])) ? $valores["emailUser"] : null;
      $birthdayUser = (isset($valores["birthdayUser"])) ? $valores["birthdayUser"] : null;
      $addressUser = (isset($valores["addressUser"])) ? $valores["addressUser"] : null;
      $celUser = (isset($valores["celUser"])) ? $valores["celUser"] : null;
      $phoneUser = (isset($valores["phoneUser"])) ? $valores["phoneUser"] : null;
      $referenceUser = (isset($valores["referenceUser"])) ? $valores["referenceUser"] : null;
      $fkRoleUser = (isset($valores["fkRoleUser"])) ? $valores["fkRoleUser"] : null;
      $fkCountryUser = (isset($valores["fkCountryUser"])) ? $valores["fkCountryUser"] : null;
      $fkGenderUser = (isset($valores["fkGenderUser"])) ? $valores["fkGenderUser"] : null;
      $passwordUser = (isset($valores["passwordUser"])) ? $valores["passwordUser"] : null;

      // Validar
      $this->array_message ["value"]["nameUser"] = $this->nameUser($nameUser);
      $this->array_message ["value"]["lastnameUser"] = $this->lastnameUser($lastnameUser);
      $this->array_message ["value"]["loginUser"] = $this->loginUser($loginUser);
      $this->array_message ["value"]["emailUser"] = $this->emailUser($emailUser);
      $this->array_message ["value"]["birthdayUser"] = $this->birthdayUser($birthdayUser);
      $this->array_message ["value"]["addressUser"] = $this->addressUser($addressUser);
      $this->array_message ["value"]["celUser"] = $this->celUser($celUser);
      $this->array_message ["value"]["phoneUser"] = $this->phoneUser($phoneUser);
      $this->array_message ["value"]["referenceUser"] = (int) $this->referenceUser($referenceUser);
      $this->array_message ["value"]["fkRoleUser"] = (int) $this->fkRoleUser($fkRoleUser);
      $this->array_message ["value"]["fkCountryUser"] = (int) $this->fkCountryUser($fkCountryUser);
      $this->array_message ["value"]["fkGenderUser"] = (int) $this->fkGenderUser($fkGenderUser);
      $this->array_message ["value"]["passwordUser"] = $this->passwordUser($passwordUser);

      return $this->array_message;
   }

   public function loginAction($valores) {
      // Verificar si los campos estan seteados
      $loginUser = (isset($valores["loginUser"])) ? $valores["loginUser"] : null;
      $passwordUser = (isset($valores["passwordUser"])) ? $valores["passwordUser"] : null;
      $hash = (isset($valores["decodeToken"])) ? $valores["decodeToken"] : null;

      // Validar
      $this->array_message ["value"]["passwordUser"] = $this->passwordUser($passwordUser);
      $this->array_message ["value"]["loginUser"] = $this->loginEmailUser($loginUser);
      $this->array_message ["value"]["decodeToken"] = $hash;

      return $this->array_message;
   }

}
