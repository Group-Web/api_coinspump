<?php

namespace AppBundle\Validation;

//use Symfony\Component\Validator\Validation;
//use Symfony\Component\Validator\Constraints\Length;
//use Symfony\Component\Validator\Constraints\NotBlank;

class WalletValidation {

    public $array_message;

    public function __construct() {
//        $this->array_message = null;
    }

    private function cryptocurrencyWallet($cryptocurrencyWallet) {
        $error = [];
        if (!isset($cryptocurrencyWallet) or $cryptocurrencyWallet == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["cryptocurrencyWallet"] = $error;
        }
        return trim($cryptocurrencyWallet);
    }
    private function descriptionWallet($descriptionWallet) {
        $error = [];
        if (!isset($descriptionWallet) or $descriptionWallet == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["descriptionWallet"] = $error;
        }
        return trim($descriptionWallet);
    }
    private function nameWallet($nameWallet) {
        $error = [];
        if (!isset($nameWallet) or $nameWallet == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["nameWallet"] = $error;
        }
        return trim($nameWallet);
    }
    private function providerWallet($providerWallet) {
        $error = [];
        if (!isset($providerWallet) or $providerWallet == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["providerWallet"] = $error;
        }
        return trim($providerWallet);
    }
    private function uriWallet($uriWallet) {
        $error = [];
        if (!isset($uriWallet) or $uriWallet == "") {
            $error[] = "Campo Vacio";
        }
        if (!empty($error)) {
            $this->array_message ["error"]["uriWallet"] = $error;
        }
        return trim($uriWallet);
    }


    // Funciones
    public function newAction($valores) {

        // Verificar si los campos estan seteados
        $cryptocurrencyWallet = (isset($valores["cryptocurrencyWallet"])) ? $valores["cryptocurrencyWallet"] : null;
        $descriptionWallet = (isset($valores["descriptionWallet"])) ? $valores["descriptionWallet"] : null;
        $nameWallet = (isset($valores["nameWallet"])) ? $valores["nameWallet"] : null;
        $providerWallet = (isset($valores["providerWallet"])) ? $valores["providerWallet"] : null;
        $uriWallet = (isset($valores["uriWallet"])) ? $valores["uriWallet"] : null;
        // Validar
        $this->array_message ["value"]["cryptocurrencyWallet"] = $this->cryptocurrencyWallet($cryptocurrencyWallet);
        $this->array_message ["value"]["descriptionWallet"] = $this->descriptionWallet($descriptionWallet);
        $this->array_message ["value"]["nameWallet"] = $this->nameWallet($nameWallet);
        $this->array_message ["value"]["providerWallet"] = $this->providerWallet($providerWallet);
        $this->array_message ["value"]["uriWallet"] = $this->uriWallet($uriWallet);

        return $this->array_message;
    }

}
