<?php

namespace AppBundle\Services;

class Helper {

//    public $jwt_auth;

    public function __construct(/* JwtAuth $jwt_auth */) {
//        $this->jwt_auth = $jwt_auth;
    }

    public function JSONR($data, $code) {
        $normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
        $encoders = array("json" => new \Symfony\Component\Serializer\Encoder\JsonEncoder());

        $serializer = new \Symfony\Component\Serializer\Serializer($normalizers, $encoders);
        $json = $serializer->serialize($data, 'json');

        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setContent($json);
        $response->setStatusCode($code);
        $response->headers->set("Content-Type", "application/json");

        return $response;

        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        $response->setData([$data]);
        return $response;


//		  try {
//
//        // Your logic here...
//
//        return new JsonResponse([
//            'success' => true,
//            'data'    => [] // Your data here
//        ]);
//
//    } catch (\Exception $exception) {
//
//        return new JsonResponse([
//            'success' => false,
//            'code'    => $exception->getCode(),
//            'message' => $exception->getMessage(),
//        ]);
//
//    }
    }

    public function ApiCall($method, $url, $auth, $headers, $data = false) {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }

        if ($auth) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $auth);
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($curl);

        if (curl_errno($curl)) {
            $result = 'Error:' . curl_error($curl);
        }
        curl_close($curl);

        return $result;
    }

}
