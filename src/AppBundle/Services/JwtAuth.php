<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Firebase\JWT\JWT;

//use DbBundle\Entity\TbUser;
//use Symfony\Component\Finder\Finder;

class JwtAuth {

    public $manager;
    public $key;

    public function __construct(EntityManager $manager) {
        $this->manager = $manager;
        $this->key = "clave-bto-coins";
    }

    public function SignUpUser($email, $pass, $decodeToken = null) {
        $password = hash('sha256', $pass);
        $signup = false;
        $user = $this->manager->getRepository('DbBundle:TbUser')->findOneBy(array("loginUser" => $email, "passwordUser" => $password));
        if (count($user) > 0) {
            $signup = true;
        } else {
            $user = $this->manager->getRepository('DbBundle:TbUser')->findOneBy(array("emailUser" => $email, "passwordUser" => $password));
            if (count($user) > 0) {
                $signup = true;
            }
        }
        if ($signup == true) {
            $dataToken = [
                "idUser" => $user->getIdUser(),
                "nameUser" => $user->getNameUser(),
                "lastnameUser" => $user->getLastnameUser(),
                "loginUser" => $user->getLoginUser(),
                "emailUser" => $user->getEmailUser(),
                "birthdayUser" => $user->getBirthdayUser()->format('Y-m-d'),
                "addressUser" => $user->getAddressUser(),
                "celUser" => $user->getCelUser(),
                "phoneUser" => $user->getPhoneUser(),
                "referenceUser" => $user->getReferenceUser(),
                "idRoleUser" => $user->getFkRole()->getIdRole(),
                "nameRoleUser" => $user->getFkRole()->getNameRole(),
                "idCountryUser" => $user->getFkCountry()->getIdCountry(),
                "nameCountryUser" => $user->getFkCountry()->getNameCountry(),
                "idGenderUser" => $user->getFkGender()->getIdGender(),
                "nameGenderUser" => $user->getFkGender()->getNameGender(),
                "fkGenderUser" => $user->getFkGender()->getNameGender(),
                "validUser" => $user->getValidUser(),
                "codeUser" => $user->getCodeUser()
            ];
            $token = array(
                "data" => $dataToken,
                "iat" => time(),
                "exp" => time() + 60000,
            );
            $jwt = JWT::encode($token, $this->key, 'HS256');
            $token['tokenUser'] = $jwt;
            if ($decodeToken) {
                return $token;
            } else {
                return $jwt;
            }
        } else {
            return null;
        }
    }

    public function ValidateToken($token, $role, $getIdentity = false) {
        $auth = true;
        try {
            $decoded = JWT::decode($token, $this->key, array('HS256'));
            $user = $this->manager->getRepository('DbBundle:TbUser')->findOneBy(array("idUser" => $decoded->data->idUser));
            $data = [
                "idUser" => $user->getIdUser(),
                "nameUser" => $user->getNameUser(),
                "lastnameUser" => $user->getLastnameUser(),
                "loginUser" => $user->getLoginUser(),
                "emailUser" => $user->getEmailUser(),
                "birthdayUser" => $user->getBirthdayUser()->format('Y-m-d'),
                "addressUser" => $user->getAddressUser(),
                "celUser" => $user->getCelUser(),
                "phoneUser" => $user->getPhoneUser(),
                "referenceUser" => $user->getReferenceUser(),
                "idRoleUser" => $user->getFkRole()->getIdRole(),
                "nameRoleUser" => $user->getFkRole()->getNameRole(),
                "idCountryUser" => $user->getFkCountry()->getIdCountry(),
                "nameCountryUser" => $user->getFkCountry()->getNameCountry(),
                "idGenderUser" => $user->getFkGender()->getIdGender(),
                "nameGenderUser" => $user->getFkGender()->getNameGender(),
                "fkGenderUser" => $user->getFkGender()->getNameGender(),
                "validUser" => $user->getValidUser(),
                "codeUser" => $user->getCodeUser()
            ];
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }
        // Solo permite usuario administrador
        if ($role === true) {
            if (!isset($data['idRoleUser']) || $data['idRoleUser'] != 1) {
                $auth = false;
            }
        }

        if ($getIdentity == true && $auth == true) {
            return $data;
        } else {
            return $auth;
        }
    }

    public function DecodeToken($token) {
        $auth = true;
        try {
            $decoded = JWT::decode($token, $this->key, array('HS256'));
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }
        if ($auth) {
            return $decoded->data;
        } else {
            return $auth;
        }
    }

}
