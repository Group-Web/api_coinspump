<?php

namespace AppBundle\Services;

//use Symfony\Component\Asset\Package;
//use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

class SendMail {

   public $jwt_auth;
   private $emailSendCC = "alberto.dj.p@gmail.com";
   private $urlFront = "https://www.coins2pump.com/";

   public function __construct() {
      
   }

   public function confirmUserMail($email, $name, $code, $mailer) {
      $data = [
          'logo' => 'https://www.coins2pump.com/lib/common/img/coins2pump-logo.png',
          'img_header' => 'https://cdn0.iconfinder.com/data/icons/project-management-1-13/65/12-512.png',
          'header' => '¡Hola ' . $name . '!',
          'body' => 'Bienvenido a Coins2pump, para continuar con el registro y poder usar nuestra plataforma. Dale click en el siguiente boton: ',
          'btn_act' => 1,
          'btn_url' => $this->urlFront . 'Validate/' . $code,
          'btn_name' => 'Activar Usuario',
          'msj_footer' => 'Si no te has registrado en Coins2pump o consideras que has recibido este correo por error, no te preocupes, sólo ignóralo.'
      ];
      $message = (new \Swift_Message())
              ->setSubject('Confirma tu cuenta')
              ->setFrom(['noreply@coins2pump.com' => 'Equipo Coins2Pump'])
              ->setTo($email)
              ->addCc($this->emailSendCC)
              ->setBody($this->structureDefaultMail($data), 'text/html');
      $mailer->send($message);
   }

   public function welcomeUserMail($email, $name, $mailer) {
      $data = [
          'logo' => 'https://www.coins2pump.com/lib/common/img/coins2pump-logo.png',
//          'img_header' => 'https://cdn0.iconfinder.com/data/icons/project-management-1-13/65/12-512.png',
          'header' => '¡Hola ' . $name . '!',
          'body' => 'Bienvenido a Coins2pump, Ya eres un usuario registrado, disfruta ya mismo de nuestro portal web, donde podras hacer cosas asombrosas... ',
          'btn_act' => 1,
          'btn_url' => $this->urlFront . 'Login/',
          'btn_name' => 'Entrar en el portal',
          'msj_footer' => 'Si no te has registrado en Coins2pump o consideras que has recibido este correo por error, no te preocupes, sólo ignóralo.'
      ];
      $message = (new \Swift_Message())
              ->setSubject('Confirma tu cuenta')
              ->setFrom(['noreply@coins2pump.com' => 'Equipo Coins2Pump'])
              ->setTo($email)
              ->addCc($this->emailSendCC)
              ->setBody($this->structureDefaultMail($data), 'text/html');
      $mailer->send($message);
   }

   private function structureDefaultMail($data) {

      $html = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8' /><title>Coins2pump</title></head>" .
              "<body  style='font-family:Arial, Helvetica, sans-serif; ; line-height:24px;'>" .
              "<div style=' width:100%; height:100%;'>" .
              "<table width='600' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='padding: 10px; border: 2px solid #773377;border-radius: 10px;'>" .
              "<tr style='text-align:center;'>" .
              "<td style='padding: 20px;'><img src='" . $data['logo'] . "' style='max-width:60%;' alt='EMAIL LOGO'/></td>" .
              "</tr>";

      if (!empty($data['img_header'])) {
         $html .= "<tr><td height='' style='text-align: center;'>" .
                 "<img src='" . $data['img_header'] . "' style='max-width:150px;' alt='EMAIL Type'/>" .
                 "</td></tr>";
      }

      $html .= "<tr><td style='font-size:20px; font-weight:bold; color:#5b5b5b; padding-top:10px;' align='center'>" . $data['header'] . "</td></tr>";

      $html .= "<tr><td style='padding:20px 50px;font-size:17px; color:#8f8f8f' align='center' >" .
              "<p>" . $data['body'] . "</p>" .
              "</td></tr>";

      if ($data['btn_act'] == 1) {
         $html .= "<tr><td height = '15' bgcolor = '#fff' style = 'font-size:15px;color:#773377;text-align:center;font-weight: bold'>" .
                 "<a style='color:#773377!important;text-decoration:none;background:#02ea02;padding: 10px 20px;border-radius: 24px 24px 24px 0px;border: 2px solid #773377;' href='" . $data['btn_url'] . "' target='_blank'>" . $data['btn_name'] . "</a>" .
                 "</td></tr>";
      }

      $html .= "<tr><td height = '30' bgcolor = '#fff'>&nbsp;</td></tr>" .
              "</table>" .
              "<table width = '600' border = '0' align = 'center' cellpadding = '0' cellspacing = '0' bgcolor = '#ffffff' style = 'text-align: center;color:#8f8f8f'>" .
              "<tr><td height = '50' bgcolor = '#fff' style = 'padding: 10px;'>" .
              "<small>" . $data['msj_footer'] . "</small>" .
              "</td></tr></table></div></body></html>";
      return $html;
   }

}
