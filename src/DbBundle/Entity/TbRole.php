<?php

namespace DbBundle\Entity;

/**
 * TbRole
 */
class TbRole
{
    /**
     * @var integer
     */
    private $idRole;

    /**
     * @var string
     */
    private $nameRole;

    /**
     * @var boolean
     */
    private $statusRole = '1';


    /**
     * Get idRole
     *
     * @return integer
     */
    public function getIdRole()
    {
        return $this->idRole;
    }

    /**
     * Set nameRole
     *
     * @param string $nameRole
     *
     * @return TbRole
     */
    public function setNameRole($nameRole)
    {
        $this->nameRole = $nameRole;

        return $this;
    }

    /**
     * Get nameRole
     *
     * @return string
     */
    public function getNameRole()
    {
        return $this->nameRole;
    }

    /**
     * Set statusRole
     *
     * @param boolean $statusRole
     *
     * @return TbRole
     */
    public function setStatusRole($statusRole)
    {
        $this->statusRole = $statusRole;

        return $this;
    }

    /**
     * Get statusRole
     *
     * @return boolean
     */
    public function getStatusRole()
    {
        return $this->statusRole;
    }
}

