<?php

namespace DbBundle\Entity;

/**
 * TbProviderPayment
 */
class TbProviderPayment
{
    /**
     * @var integer
     */
    private $idProviderPayment;

    /**
     * @var string
     */
    private $nameProviderPayment;

    /**
     * @var string
     */
    private $descriptionProviderPayment;

    /**
     * @var string
     */
    private $uriProviderPayment;

    /**
     * @var boolean
     */
    private $statusProviderPayment = '1';


    /**
     * Get idProviderPayment
     *
     * @return integer
     */
    public function getIdProviderPayment()
    {
        return $this->idProviderPayment;
    }

    /**
     * Set nameProviderPayment
     *
     * @param string $nameProviderPayment
     *
     * @return TbProviderPayment
     */
    public function setNameProviderPayment($nameProviderPayment)
    {
        $this->nameProviderPayment = $nameProviderPayment;

        return $this;
    }

    /**
     * Get nameProviderPayment
     *
     * @return string
     */
    public function getNameProviderPayment()
    {
        return $this->nameProviderPayment;
    }

    /**
     * Set descriptionProviderPayment
     *
     * @param string $descriptionProviderPayment
     *
     * @return TbProviderPayment
     */
    public function setDescriptionProviderPayment($descriptionProviderPayment)
    {
        $this->descriptionProviderPayment = $descriptionProviderPayment;

        return $this;
    }

    /**
     * Get descriptionProviderPayment
     *
     * @return string
     */
    public function getDescriptionProviderPayment()
    {
        return $this->descriptionProviderPayment;
    }

    /**
     * Set uriProviderPayment
     *
     * @param string $uriProviderPayment
     *
     * @return TbProviderPayment
     */
    public function setUriProviderPayment($uriProviderPayment)
    {
        $this->uriProviderPayment = $uriProviderPayment;

        return $this;
    }

    /**
     * Get uriProviderPayment
     *
     * @return string
     */
    public function getUriProviderPayment()
    {
        return $this->uriProviderPayment;
    }

    /**
     * Set statusProviderPayment
     *
     * @param boolean $statusProviderPayment
     *
     * @return TbProviderPayment
     */
    public function setStatusProviderPayment($statusProviderPayment)
    {
        $this->statusProviderPayment = $statusProviderPayment;

        return $this;
    }

    /**
     * Get statusProviderPayment
     *
     * @return boolean
     */
    public function getStatusProviderPayment()
    {
        return $this->statusProviderPayment;
    }
}

