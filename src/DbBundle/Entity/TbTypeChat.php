<?php

namespace DbBundle\Entity;

/**
 * TbTypeChat
 */
class TbTypeChat
{
    /**
     * @var integer
     */
    private $idTypeChat;

    /**
     * @var string
     */
    private $nameTypeChat;

    /**
     * @var boolean
     */
    private $statusChat = '1';


    /**
     * Get idTypeChat
     *
     * @return integer
     */
    public function getIdTypeChat()
    {
        return $this->idTypeChat;
    }

    /**
     * Set nameTypeChat
     *
     * @param string $nameTypeChat
     *
     * @return TbTypeChat
     */
    public function setNameTypeChat($nameTypeChat)
    {
        $this->nameTypeChat = $nameTypeChat;

        return $this;
    }

    /**
     * Get nameTypeChat
     *
     * @return string
     */
    public function getNameTypeChat()
    {
        return $this->nameTypeChat;
    }

    /**
     * Set statusChat
     *
     * @param boolean $statusChat
     *
     * @return TbTypeChat
     */
    public function setStatusChat($statusChat)
    {
        $this->statusChat = $statusChat;

        return $this;
    }

    /**
     * Get statusChat
     *
     * @return boolean
     */
    public function getStatusChat()
    {
        return $this->statusChat;
    }
}

