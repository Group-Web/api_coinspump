<?php

namespace DbBundle\Entity;

/**
 * TbBan
 */
class TbBan
{
    /**
     * @var integer
     */
    private $idBan;

    /**
     * @var string
     */
    private $descriptionBan;

    /**
     * @var \DateTime
     */
    private $createdBan = 'CURRENT_TIMESTAMP';

    /**
     * @var \DbBundle\Entity\TbTypeBan
     */
    private $fkTypeBan;

    /**
     * @var \DbBundle\Entity\TbUser
     */
    private $fkUser;


    /**
     * Get idBan
     *
     * @return integer
     */
    public function getIdBan()
    {
        return $this->idBan;
    }

    /**
     * Set descriptionBan
     *
     * @param string $descriptionBan
     *
     * @return TbBan
     */
    public function setDescriptionBan($descriptionBan)
    {
        $this->descriptionBan = $descriptionBan;

        return $this;
    }

    /**
     * Get descriptionBan
     *
     * @return string
     */
    public function getDescriptionBan()
    {
        return $this->descriptionBan;
    }

    /**
     * Set createdBan
     *
     * @param \DateTime $createdBan
     *
     * @return TbBan
     */
    public function setCreatedBan($createdBan)
    {
        $this->createdBan = $createdBan;

        return $this;
    }

    /**
     * Get createdBan
     *
     * @return \DateTime
     */
    public function getCreatedBan()
    {
        return $this->createdBan;
    }

    /**
     * Set fkTypeBan
     *
     * @param \DbBundle\Entity\TbTypeBan $fkTypeBan
     *
     * @return TbBan
     */
    public function setFkTypeBan(\DbBundle\Entity\TbTypeBan $fkTypeBan = null)
    {
        $this->fkTypeBan = $fkTypeBan;

        return $this;
    }

    /**
     * Get fkTypeBan
     *
     * @return \DbBundle\Entity\TbTypeBan
     */
    public function getFkTypeBan()
    {
        return $this->fkTypeBan;
    }

    /**
     * Set fkUser
     *
     * @param \DbBundle\Entity\TbUser $fkUser
     *
     * @return TbBan
     */
    public function setFkUser(\DbBundle\Entity\TbUser $fkUser = null)
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    /**
     * Get fkUser
     *
     * @return \DbBundle\Entity\TbUser
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
}

