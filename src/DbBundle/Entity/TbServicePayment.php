<?php

namespace DbBundle\Entity;

/**
 * TbServicePayment
 */
class TbServicePayment
{
    /**
     * @var integer
     */
    private $idServicePayment;

    /**
     * @var string
     */
    private $nameServicePayment;

    /**
     * @var string
     */
    private $descriptionServicePayment;

    /**
     * @var string
     */
    private $costServicePayment = '0';

    /**
     * @var boolean
     */
    private $statusServicePayment = '1';


    /**
     * Get idServicePayment
     *
     * @return integer
     */
    public function getIdServicePayment()
    {
        return $this->idServicePayment;
    }

    /**
     * Set nameServicePayment
     *
     * @param string $nameServicePayment
     *
     * @return TbServicePayment
     */
    public function setNameServicePayment($nameServicePayment)
    {
        $this->nameServicePayment = $nameServicePayment;

        return $this;
    }

    /**
     * Get nameServicePayment
     *
     * @return string
     */
    public function getNameServicePayment()
    {
        return $this->nameServicePayment;
    }

    /**
     * Set descriptionServicePayment
     *
     * @param string $descriptionServicePayment
     *
     * @return TbServicePayment
     */
    public function setDescriptionServicePayment($descriptionServicePayment)
    {
        $this->descriptionServicePayment = $descriptionServicePayment;

        return $this;
    }

    /**
     * Get descriptionServicePayment
     *
     * @return string
     */
    public function getDescriptionServicePayment()
    {
        return $this->descriptionServicePayment;
    }

    /**
     * Set costServicePayment
     *
     * @param string $costServicePayment
     *
     * @return TbServicePayment
     */
    public function setCostServicePayment($costServicePayment)
    {
        $this->costServicePayment = $costServicePayment;

        return $this;
    }

    /**
     * Get costServicePayment
     *
     * @return string
     */
    public function getCostServicePayment()
    {
        return $this->costServicePayment;
    }

    /**
     * Set statusServicePayment
     *
     * @param boolean $statusServicePayment
     *
     * @return TbServicePayment
     */
    public function setStatusServicePayment($statusServicePayment)
    {
        $this->statusServicePayment = $statusServicePayment;

        return $this;
    }

    /**
     * Get statusServicePayment
     *
     * @return boolean
     */
    public function getStatusServicePayment()
    {
        return $this->statusServicePayment;
    }
}

