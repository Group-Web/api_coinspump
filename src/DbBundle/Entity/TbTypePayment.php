<?php

namespace DbBundle\Entity;

/**
 * TbTypePayment
 */
class TbTypePayment
{
    /**
     * @var integer
     */
    private $idTypePayment;

    /**
     * @var string
     */
    private $nameTypePayment;

    /**
     * @var string
     */
    private $descriptionTypePayment;

    /**
     * @var boolean
     */
    private $statusTypePayment = '1';

    /**
     * @var string
     */
    private $uriTypePayment;

    /**
     * @var \DbBundle\Entity\TbCryptocurrency
     */
    private $fkCryptocurrency;

    /**
     * @var \DbBundle\Entity\TbProviderPayment
     */
    private $fkProviderPayment;


    /**
     * Get idTypePayment
     *
     * @return integer
     */
    public function getIdTypePayment()
    {
        return $this->idTypePayment;
    }

    /**
     * Set nameTypePayment
     *
     * @param string $nameTypePayment
     *
     * @return TbTypePayment
     */
    public function setNameTypePayment($nameTypePayment)
    {
        $this->nameTypePayment = $nameTypePayment;

        return $this;
    }

    /**
     * Get nameTypePayment
     *
     * @return string
     */
    public function getNameTypePayment()
    {
        return $this->nameTypePayment;
    }

    /**
     * Set descriptionTypePayment
     *
     * @param string $descriptionTypePayment
     *
     * @return TbTypePayment
     */
    public function setDescriptionTypePayment($descriptionTypePayment)
    {
        $this->descriptionTypePayment = $descriptionTypePayment;

        return $this;
    }

    /**
     * Get descriptionTypePayment
     *
     * @return string
     */
    public function getDescriptionTypePayment()
    {
        return $this->descriptionTypePayment;
    }

    /**
     * Set statusTypePayment
     *
     * @param boolean $statusTypePayment
     *
     * @return TbTypePayment
     */
    public function setStatusTypePayment($statusTypePayment)
    {
        $this->statusTypePayment = $statusTypePayment;

        return $this;
    }

    /**
     * Get statusTypePayment
     *
     * @return boolean
     */
    public function getStatusTypePayment()
    {
        return $this->statusTypePayment;
    }

    /**
     * Set uriTypePayment
     *
     * @param string $uriTypePayment
     *
     * @return TbTypePayment
     */
    public function setUriTypePayment($uriTypePayment)
    {
        $this->uriTypePayment = $uriTypePayment;

        return $this;
    }

    /**
     * Get uriTypePayment
     *
     * @return string
     */
    public function getUriTypePayment()
    {
        return $this->uriTypePayment;
    }

    /**
     * Set fkCryptocurrency
     *
     * @param \DbBundle\Entity\TbCryptocurrency $fkCryptocurrency
     *
     * @return TbTypePayment
     */
    public function setFkCryptocurrency(\DbBundle\Entity\TbCryptocurrency $fkCryptocurrency = null)
    {
        $this->fkCryptocurrency = $fkCryptocurrency;

        return $this;
    }

    /**
     * Get fkCryptocurrency
     *
     * @return \DbBundle\Entity\TbCryptocurrency
     */
    public function getFkCryptocurrency()
    {
        return $this->fkCryptocurrency;
    }

    /**
     * Set fkProviderPayment
     *
     * @param \DbBundle\Entity\TbProviderPayment $fkProviderPayment
     *
     * @return TbTypePayment
     */
    public function setFkProviderPayment(\DbBundle\Entity\TbProviderPayment $fkProviderPayment = null)
    {
        $this->fkProviderPayment = $fkProviderPayment;

        return $this;
    }

    /**
     * Get fkProviderPayment
     *
     * @return \DbBundle\Entity\TbProviderPayment
     */
    public function getFkProviderPayment()
    {
        return $this->fkProviderPayment;
    }
}

