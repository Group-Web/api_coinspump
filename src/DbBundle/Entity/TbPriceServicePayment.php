<?php

namespace DbBundle\Entity;

/**
 * TbPriceServicePayment
 */
class TbPriceServicePayment
{
    /**
     * @var integer
     */
    private $idPriceServicePayment;

    /**
     * @var string
     */
    private $valuePriceServicePayment;

    /**
     * @var string
     */
    private $descriptionPriceServicePayment;

    /**
     * @var boolean
     */
    private $statusPriceServicePayment = '1';

    /**
     * @var \DateTime
     */
    private $createdPriceServicePayment = 'CURRENT_TIMESTAMP';

    /**
     * @var \DbBundle\Entity\TbServicePayment
     */
    private $fkServicePayment;

    /**
     * @var \DbBundle\Entity\TbCryptocurrency
     */
    private $fkCryptocurrency;


    /**
     * Get idPriceServicePayment
     *
     * @return integer
     */
    public function getIdPriceServicePayment()
    {
        return $this->idPriceServicePayment;
    }

    /**
     * Set valuePriceServicePayment
     *
     * @param string $valuePriceServicePayment
     *
     * @return TbPriceServicePayment
     */
    public function setValuePriceServicePayment($valuePriceServicePayment)
    {
        $this->valuePriceServicePayment = $valuePriceServicePayment;

        return $this;
    }

    /**
     * Get valuePriceServicePayment
     *
     * @return string
     */
    public function getValuePriceServicePayment()
    {
        return $this->valuePriceServicePayment;
    }

    /**
     * Set descriptionPriceServicePayment
     *
     * @param string $descriptionPriceServicePayment
     *
     * @return TbPriceServicePayment
     */
    public function setDescriptionPriceServicePayment($descriptionPriceServicePayment)
    {
        $this->descriptionPriceServicePayment = $descriptionPriceServicePayment;

        return $this;
    }

    /**
     * Get descriptionPriceServicePayment
     *
     * @return string
     */
    public function getDescriptionPriceServicePayment()
    {
        return $this->descriptionPriceServicePayment;
    }

    /**
     * Set statusPriceServicePayment
     *
     * @param boolean $statusPriceServicePayment
     *
     * @return TbPriceServicePayment
     */
    public function setStatusPriceServicePayment($statusPriceServicePayment)
    {
        $this->statusPriceServicePayment = $statusPriceServicePayment;

        return $this;
    }

    /**
     * Get statusPriceServicePayment
     *
     * @return boolean
     */
    public function getStatusPriceServicePayment()
    {
        return $this->statusPriceServicePayment;
    }

    /**
     * Set createdPriceServicePayment
     *
     * @param \DateTime $createdPriceServicePayment
     *
     * @return TbPriceServicePayment
     */
    public function setCreatedPriceServicePayment($createdPriceServicePayment)
    {
        $this->createdPriceServicePayment = $createdPriceServicePayment;

        return $this;
    }

    /**
     * Get createdPriceServicePayment
     *
     * @return \DateTime
     */
    public function getCreatedPriceServicePayment()
    {
        return $this->createdPriceServicePayment;
    }

    /**
     * Set fkServicePayment
     *
     * @param \DbBundle\Entity\TbServicePayment $fkServicePayment
     *
     * @return TbPriceServicePayment
     */
    public function setFkServicePayment(\DbBundle\Entity\TbServicePayment $fkServicePayment = null)
    {
        $this->fkServicePayment = $fkServicePayment;

        return $this;
    }

    /**
     * Get fkServicePayment
     *
     * @return \DbBundle\Entity\TbServicePayment
     */
    public function getFkServicePayment()
    {
        return $this->fkServicePayment;
    }

    /**
     * Set fkCryptocurrency
     *
     * @param \DbBundle\Entity\TbCryptocurrency $fkCryptocurrency
     *
     * @return TbPriceServicePayment
     */
    public function setFkCryptocurrency(\DbBundle\Entity\TbCryptocurrency $fkCryptocurrency = null)
    {
        $this->fkCryptocurrency = $fkCryptocurrency;

        return $this;
    }

    /**
     * Get fkCryptocurrency
     *
     * @return \DbBundle\Entity\TbCryptocurrency
     */
    public function getFkCryptocurrency()
    {
        return $this->fkCryptocurrency;
    }
}

