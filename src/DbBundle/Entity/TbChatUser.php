<?php

namespace DbBundle\Entity;

/**
 * TbChatUser
 */
class TbChatUser
{
    /**
     * @var integer
     */
    private $idChatUser;

    /**
     * @var \DateTime
     */
    private $createdChatUser = 'CURRENT_TIMESTAMP';

    /**
     * @var boolean
     */
    private $banChatUser = '0';

    /**
     * @var string
     */
    private $descriptionBanChatUser;

    /**
     * @var boolean
     */
    private $validChatUser = '0';

    /**
     * @var boolean
     */
    private $ownerChatUser = '0';

    /**
     * @var \DbBundle\Entity\TbChat
     */
    private $fkChat;

    /**
     * @var \DbBundle\Entity\TbUser
     */
    private $fkUser;


    /**
     * Get idChatUser
     *
     * @return integer
     */
    public function getIdChatUser()
    {
        return $this->idChatUser;
    }

    /**
     * Set createdChatUser
     *
     * @param \DateTime $createdChatUser
     *
     * @return TbChatUser
     */
    public function setCreatedChatUser($createdChatUser)
    {
        $this->createdChatUser = $createdChatUser;

        return $this;
    }

    /**
     * Get createdChatUser
     *
     * @return \DateTime
     */
    public function getCreatedChatUser()
    {
        return $this->createdChatUser;
    }

    /**
     * Set banChatUser
     *
     * @param boolean $banChatUser
     *
     * @return TbChatUser
     */
    public function setBanChatUser($banChatUser)
    {
        $this->banChatUser = $banChatUser;

        return $this;
    }

    /**
     * Get banChatUser
     *
     * @return boolean
     */
    public function getBanChatUser()
    {
        return $this->banChatUser;
    }

    /**
     * Set descriptionBanChatUser
     *
     * @param string $descriptionBanChatUser
     *
     * @return TbChatUser
     */
    public function setDescriptionBanChatUser($descriptionBanChatUser)
    {
        $this->descriptionBanChatUser = $descriptionBanChatUser;

        return $this;
    }

    /**
     * Get descriptionBanChatUser
     *
     * @return string
     */
    public function getDescriptionBanChatUser()
    {
        return $this->descriptionBanChatUser;
    }

    /**
     * Set validChatUser
     *
     * @param boolean $validChatUser
     *
     * @return TbChatUser
     */
    public function setValidChatUser($validChatUser)
    {
        $this->validChatUser = $validChatUser;

        return $this;
    }

    /**
     * Get validChatUser
     *
     * @return boolean
     */
    public function getValidChatUser()
    {
        return $this->validChatUser;
    }

    /**
     * Set ownerChatUser
     *
     * @param boolean $ownerChatUser
     *
     * @return TbChatUser
     */
    public function setOwnerChatUser($ownerChatUser)
    {
        $this->ownerChatUser = $ownerChatUser;

        return $this;
    }

    /**
     * Get ownerChatUser
     *
     * @return boolean
     */
    public function getOwnerChatUser()
    {
        return $this->ownerChatUser;
    }

    /**
     * Set fkChat
     *
     * @param \DbBundle\Entity\TbChat $fkChat
     *
     * @return TbChatUser
     */
    public function setFkChat(\DbBundle\Entity\TbChat $fkChat = null)
    {
        $this->fkChat = $fkChat;

        return $this;
    }

    /**
     * Get fkChat
     *
     * @return \DbBundle\Entity\TbChat
     */
    public function getFkChat()
    {
        return $this->fkChat;
    }

    /**
     * Set fkUser
     *
     * @param \DbBundle\Entity\TbUser $fkUser
     *
     * @return TbChatUser
     */
    public function setFkUser(\DbBundle\Entity\TbUser $fkUser = null)
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    /**
     * Get fkUser
     *
     * @return \DbBundle\Entity\TbUser
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
}

