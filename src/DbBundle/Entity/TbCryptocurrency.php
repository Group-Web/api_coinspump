<?php

namespace DbBundle\Entity;

/**
 * TbCryptocurrency
 */
class TbCryptocurrency
{
    /**
     * @var integer
     */
    private $idCryptocurrency;

    /**
     * @var string
     */
    private $nameCryptocurrency;

    /**
     * @var string
     */
    private $imageCryptocurrency;

    /**
     * @var string
     */
    private $webCryptocurrency;

    /**
     * @var boolean
     */
    private $statusCryptocurrency = '1';

    /**
     * @var \DateTime
     */
    private $createdCryptocurrency = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     */
    private $codeCryptocurrency;

    /**
     * @var boolean
     */
    private $isPaymentCryptocurrency = '1';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvCryptocurrency;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvSerCryptocurrency;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvChaCryptocurrency;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvTypePayCryptocurrency;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fkInvCryptocurrency = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fkInvSerCryptocurrency = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fkInvChaCryptocurrency = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fkInvTypePayCryptocurrency = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idCryptocurrency
     *
     * @return integer
     */
    public function getIdCryptocurrency()
    {
        return $this->idCryptocurrency;
    }

    /**
     * Set nameCryptocurrency
     *
     * @param string $nameCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function setNameCryptocurrency($nameCryptocurrency)
    {
        $this->nameCryptocurrency = $nameCryptocurrency;

        return $this;
    }

    /**
     * Get nameCryptocurrency
     *
     * @return string
     */
    public function getNameCryptocurrency()
    {
        return $this->nameCryptocurrency;
    }

    /**
     * Set imageCryptocurrency
     *
     * @param string $imageCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function setImageCryptocurrency($imageCryptocurrency)
    {
        $this->imageCryptocurrency = $imageCryptocurrency;

        return $this;
    }

    /**
     * Get imageCryptocurrency
     *
     * @return string
     */
    public function getImageCryptocurrency()
    {
        return $this->imageCryptocurrency;
    }

    /**
     * Set webCryptocurrency
     *
     * @param string $webCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function setWebCryptocurrency($webCryptocurrency)
    {
        $this->webCryptocurrency = $webCryptocurrency;

        return $this;
    }

    /**
     * Get webCryptocurrency
     *
     * @return string
     */
    public function getWebCryptocurrency()
    {
        return $this->webCryptocurrency;
    }

    /**
     * Set statusCryptocurrency
     *
     * @param boolean $statusCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function setStatusCryptocurrency($statusCryptocurrency)
    {
        $this->statusCryptocurrency = $statusCryptocurrency;

        return $this;
    }

    /**
     * Get statusCryptocurrency
     *
     * @return boolean
     */
    public function getStatusCryptocurrency()
    {
        return $this->statusCryptocurrency;
    }

    /**
     * Set createdCryptocurrency
     *
     * @param \DateTime $createdCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function setCreatedCryptocurrency($createdCryptocurrency)
    {
        $this->createdCryptocurrency = $createdCryptocurrency;

        return $this;
    }

    /**
     * Get createdCryptocurrency
     *
     * @return \DateTime
     */
    public function getCreatedCryptocurrency()
    {
        return $this->createdCryptocurrency;
    }

    /**
     * Set codeCryptocurrency
     *
     * @param string $codeCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function setCodeCryptocurrency($codeCryptocurrency)
    {
        $this->codeCryptocurrency = $codeCryptocurrency;

        return $this;
    }

    /**
     * Get codeCryptocurrency
     *
     * @return string
     */
    public function getCodeCryptocurrency()
    {
        return $this->codeCryptocurrency;
    }

    /**
     * Set isPaymentCryptocurrency
     *
     * @param boolean $isPaymentCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function setIsPaymentCryptocurrency($isPaymentCryptocurrency)
    {
        $this->isPaymentCryptocurrency = $isPaymentCryptocurrency;

        return $this;
    }

    /**
     * Get isPaymentCryptocurrency
     *
     * @return boolean
     */
    public function getIsPaymentCryptocurrency()
    {
        return $this->isPaymentCryptocurrency;
    }

    /**
     * Add fkInvCryptocurrency
     *
     * @param \DbBundle\Entity\TbChat $fkInvCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function addFkInvCryptocurrency(\DbBundle\Entity\TbChat $fkInvCryptocurrency)
    {
        $this->fkInvCryptocurrency[] = $fkInvCryptocurrency;

        return $this;
    }

    /**
     * Remove fkInvCryptocurrency
     *
     * @param \DbBundle\Entity\TbChat $fkInvCryptocurrency
     */
    public function removeFkInvCryptocurrency(\DbBundle\Entity\TbChat $fkInvCryptocurrency)
    {
        $this->fkInvCryptocurrency->removeElement($fkInvCryptocurrency);
    }

    /**
     * Get fkInvCryptocurrency
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvCryptocurrency()
    {
        return $this->fkInvCryptocurrency;
    }

    /**
     * Add fkInvSerCryptocurrency
     *
     * @param \DbBundle\Entity\TbPriceServicePayment $fkInvSerCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function addFkInvSerCryptocurrency(\DbBundle\Entity\TbPriceServicePayment $fkInvSerCryptocurrency)
    {
        $this->fkInvSerCryptocurrency[] = $fkInvSerCryptocurrency;

        return $this;
    }

    /**
     * Remove fkInvSerCryptocurrency
     *
     * @param \DbBundle\Entity\TbPriceServicePayment $fkInvSerCryptocurrency
     */
    public function removeFkInvSerCryptocurrency(\DbBundle\Entity\TbPriceServicePayment $fkInvSerCryptocurrency)
    {
        $this->fkInvSerCryptocurrency->removeElement($fkInvSerCryptocurrency);
    }

    /**
     * Get fkInvSerCryptocurrency
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvSerCryptocurrency()
    {
        return $this->fkInvSerCryptocurrency;
    }

    /**
     * Add fkInvChaCryptocurrency
     *
     * @param \DbBundle\Entity\TbPriceChat $fkInvChaCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function addFkInvChaCryptocurrency(\DbBundle\Entity\TbPriceChat $fkInvChaCryptocurrency)
    {
        $this->fkInvChaCryptocurrency[] = $fkInvChaCryptocurrency;

        return $this;
    }

    /**
     * Remove fkInvChaCryptocurrency
     *
     * @param \DbBundle\Entity\TbPriceChat $fkInvChaCryptocurrency
     */
    public function removeFkInvChaCryptocurrency(\DbBundle\Entity\TbPriceChat $fkInvChaCryptocurrency)
    {
        $this->fkInvChaCryptocurrency->removeElement($fkInvChaCryptocurrency);
    }

    /**
     * Get fkInvChaCryptocurrency
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvChaCryptocurrency()
    {
        return $this->fkInvChaCryptocurrency;
    }

    /**
     * Add fkInvTypePayCryptocurrency
     *
     * @param \DbBundle\Entity\TbTypePayment $fkInvTypePayCryptocurrency
     *
     * @return TbCryptocurrency
     */
    public function addFkInvTypePayCryptocurrency(\DbBundle\Entity\TbTypePayment $fkInvTypePayCryptocurrency)
    {
        $this->fkInvTypePayCryptocurrency[] = $fkInvTypePayCryptocurrency;

        return $this;
    }

    /**
     * Remove fkInvTypePayCryptocurrency
     *
     * @param \DbBundle\Entity\TbTypePayment $fkInvTypePayCryptocurrency
     */
    public function removeFkInvTypePayCryptocurrency(\DbBundle\Entity\TbTypePayment $fkInvTypePayCryptocurrency)
    {
        $this->fkInvTypePayCryptocurrency->removeElement($fkInvTypePayCryptocurrency);
    }

    /**
     * Get fkInvTypePayCryptocurrency
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvTypePayCryptocurrency()
    {
        return $this->fkInvTypePayCryptocurrency;
    }
}

