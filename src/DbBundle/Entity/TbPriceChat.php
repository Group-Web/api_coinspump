<?php

namespace DbBundle\Entity;

/**
 * TbPriceChat
 */
class TbPriceChat
{
    /**
     * @var integer
     */
    private $idPriceChat;

    /**
     * @var string
     */
    private $valuePriceChat;

    /**
     * @var string
     */
    private $descriptionPriceChat;

    /**
     * @var boolean
     */
    private $statusPriceChat = '1';

    /**
     * @var \DateTime
     */
    private $createdPriceChat = 'CURRENT_TIMESTAMP';

    /**
     * @var \DbBundle\Entity\TbCryptocurrency
     */
    private $fkCryptocurrency;

    /**
     * @var \DbBundle\Entity\TbChat
     */
    private $fkChat;


    /**
     * Get idPriceChat
     *
     * @return integer
     */
    public function getIdPriceChat()
    {
        return $this->idPriceChat;
    }

    /**
     * Set valuePriceChat
     *
     * @param string $valuePriceChat
     *
     * @return TbPriceChat
     */
    public function setValuePriceChat($valuePriceChat)
    {
        $this->valuePriceChat = $valuePriceChat;

        return $this;
    }

    /**
     * Get valuePriceChat
     *
     * @return string
     */
    public function getValuePriceChat()
    {
        return $this->valuePriceChat;
    }

    /**
     * Set descriptionPriceChat
     *
     * @param string $descriptionPriceChat
     *
     * @return TbPriceChat
     */
    public function setDescriptionPriceChat($descriptionPriceChat)
    {
        $this->descriptionPriceChat = $descriptionPriceChat;

        return $this;
    }

    /**
     * Get descriptionPriceChat
     *
     * @return string
     */
    public function getDescriptionPriceChat()
    {
        return $this->descriptionPriceChat;
    }

    /**
     * Set statusPriceChat
     *
     * @param boolean $statusPriceChat
     *
     * @return TbPriceChat
     */
    public function setStatusPriceChat($statusPriceChat)
    {
        $this->statusPriceChat = $statusPriceChat;

        return $this;
    }

    /**
     * Get statusPriceChat
     *
     * @return boolean
     */
    public function getStatusPriceChat()
    {
        return $this->statusPriceChat;
    }

    /**
     * Set createdPriceChat
     *
     * @param \DateTime $createdPriceChat
     *
     * @return TbPriceChat
     */
    public function setCreatedPriceChat($createdPriceChat)
    {
        $this->createdPriceChat = $createdPriceChat;

        return $this;
    }

    /**
     * Get createdPriceChat
     *
     * @return \DateTime
     */
    public function getCreatedPriceChat()
    {
        return $this->createdPriceChat;
    }

    /**
     * Set fkCryptocurrency
     *
     * @param \DbBundle\Entity\TbCryptocurrency $fkCryptocurrency
     *
     * @return TbPriceChat
     */
    public function setFkCryptocurrency(\DbBundle\Entity\TbCryptocurrency $fkCryptocurrency = null)
    {
        $this->fkCryptocurrency = $fkCryptocurrency;

        return $this;
    }

    /**
     * Get fkCryptocurrency
     *
     * @return \DbBundle\Entity\TbCryptocurrency
     */
    public function getFkCryptocurrency()
    {
        return $this->fkCryptocurrency;
    }

    /**
     * Set fkChat
     *
     * @param \DbBundle\Entity\TbChat $fkChat
     *
     * @return TbPriceChat
     */
    public function setFkChat(\DbBundle\Entity\TbChat $fkChat = null)
    {
        $this->fkChat = $fkChat;

        return $this;
    }

    /**
     * Get fkChat
     *
     * @return \DbBundle\Entity\TbChat
     */
    public function getFkChat()
    {
        return $this->fkChat;
    }
}

