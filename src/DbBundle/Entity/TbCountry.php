<?php

namespace DbBundle\Entity;

/**
 * TbCountry
 */
class TbCountry
{
    /**
     * @var integer
     */
    private $idCountry;

    /**
     * @var string
     */
    private $nameCountry;

    /**
     * @var string
     */
    private $codeCountry;

    /**
     * @var boolean
     */
    private $statusCountry = '1';


    /**
     * Get idCountry
     *
     * @return integer
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set nameCountry
     *
     * @param string $nameCountry
     *
     * @return TbCountry
     */
    public function setNameCountry($nameCountry)
    {
        $this->nameCountry = $nameCountry;

        return $this;
    }

    /**
     * Get nameCountry
     *
     * @return string
     */
    public function getNameCountry()
    {
        return $this->nameCountry;
    }

    /**
     * Set codeCountry
     *
     * @param string $codeCountry
     *
     * @return TbCountry
     */
    public function setCodeCountry($codeCountry)
    {
        $this->codeCountry = $codeCountry;

        return $this;
    }

    /**
     * Get codeCountry
     *
     * @return string
     */
    public function getCodeCountry()
    {
        return $this->codeCountry;
    }

    /**
     * Set statusCountry
     *
     * @param boolean $statusCountry
     *
     * @return TbCountry
     */
    public function setStatusCountry($statusCountry)
    {
        $this->statusCountry = $statusCountry;

        return $this;
    }

    /**
     * Get statusCountry
     *
     * @return boolean
     */
    public function getStatusCountry()
    {
        return $this->statusCountry;
    }
}

