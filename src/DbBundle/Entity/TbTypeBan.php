<?php

namespace DbBundle\Entity;

/**
 * TbTypeBan
 */
class TbTypeBan
{
    /**
     * @var integer
     */
    private $idTypeBan;

    /**
     * @var string
     */
    private $nameTypeBan;

    /**
     * @var string
     */
    private $descriptionTypeBan;

    /**
     * @var boolean
     */
    private $statusTypeBan;


    /**
     * Get idTypeBan
     *
     * @return integer
     */
    public function getIdTypeBan()
    {
        return $this->idTypeBan;
    }

    /**
     * Set nameTypeBan
     *
     * @param string $nameTypeBan
     *
     * @return TbTypeBan
     */
    public function setNameTypeBan($nameTypeBan)
    {
        $this->nameTypeBan = $nameTypeBan;

        return $this;
    }

    /**
     * Get nameTypeBan
     *
     * @return string
     */
    public function getNameTypeBan()
    {
        return $this->nameTypeBan;
    }

    /**
     * Set descriptionTypeBan
     *
     * @param string $descriptionTypeBan
     *
     * @return TbTypeBan
     */
    public function setDescriptionTypeBan($descriptionTypeBan)
    {
        $this->descriptionTypeBan = $descriptionTypeBan;

        return $this;
    }

    /**
     * Get descriptionTypeBan
     *
     * @return string
     */
    public function getDescriptionTypeBan()
    {
        return $this->descriptionTypeBan;
    }

    /**
     * Set statusTypeBan
     *
     * @param boolean $statusTypeBan
     *
     * @return TbTypeBan
     */
    public function setStatusTypeBan($statusTypeBan)
    {
        $this->statusTypeBan = $statusTypeBan;

        return $this;
    }

    /**
     * Get statusTypeBan
     *
     * @return boolean
     */
    public function getStatusTypeBan()
    {
        return $this->statusTypeBan;
    }
}

