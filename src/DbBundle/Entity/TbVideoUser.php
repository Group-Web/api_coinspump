<?php

namespace DbBundle\Entity;

/**
 * TbVideoUser
 */
class TbVideoUser
{
    /**
     * @var integer
     */
    private $idVideoUser;

    /**
     * @var string
     */
    private $codeVideoUser;

    /**
     * @var boolean
     */
    private $statusVideoUser = '1';

    /**
     * @var \DbBundle\Entity\TbUser
     */
    private $fkUser;


    /**
     * Get idVideoUser
     *
     * @return integer
     */
    public function getIdVideoUser()
    {
        return $this->idVideoUser;
    }

    /**
     * Set codeVideoUser
     *
     * @param string $codeVideoUser
     *
     * @return TbVideoUser
     */
    public function setCodeVideoUser($codeVideoUser)
    {
        $this->codeVideoUser = $codeVideoUser;

        return $this;
    }

    /**
     * Get codeVideoUser
     *
     * @return string
     */
    public function getCodeVideoUser()
    {
        return $this->codeVideoUser;
    }

    /**
     * Set statusVideoUser
     *
     * @param boolean $statusVideoUser
     *
     * @return TbVideoUser
     */
    public function setStatusVideoUser($statusVideoUser)
    {
        $this->statusVideoUser = $statusVideoUser;

        return $this;
    }

    /**
     * Get statusVideoUser
     *
     * @return boolean
     */
    public function getStatusVideoUser()
    {
        return $this->statusVideoUser;
    }

    /**
     * Set fkUser
     *
     * @param \DbBundle\Entity\TbUser $fkUser
     *
     * @return TbVideoUser
     */
    public function setFkUser(\DbBundle\Entity\TbUser $fkUser = null)
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    /**
     * Get fkUser
     *
     * @return \DbBundle\Entity\TbUser
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
}

