<?php

namespace DbBundle\Entity;

/**
 * TbPayment
 */
class TbPayment
{
    /**
     * @var integer
     */
    private $idPayment;

    /**
     * @var string
     */
    private $amountPayment;

    /**
     * @var string
     */
    private $referencePayment;

    /**
     * @var string
     */
    private $descriptionPayment;

    /**
     * @var boolean
     */
    private $validPayment = '0';

    /**
     * @var \DateTime
     */
    private $createdPayment = 'CURRENT_TIMESTAMP';

    /**
     * @var \DbBundle\Entity\TbChat
     */
    private $fkChat;

    /**
     * @var \DbBundle\Entity\TbUser
     */
    private $fkUser;

    /**
     * @var \DbBundle\Entity\TbTypePayment
     */
    private $fkTypePayment;

    /**
     * @var \DbBundle\Entity\TbServicePayment
     */
    private $fkServicePayment;


    /**
     * Get idPayment
     *
     * @return integer
     */
    public function getIdPayment()
    {
        return $this->idPayment;
    }

    /**
     * Set amountPayment
     *
     * @param string $amountPayment
     *
     * @return TbPayment
     */
    public function setAmountPayment($amountPayment)
    {
        $this->amountPayment = $amountPayment;

        return $this;
    }

    /**
     * Get amountPayment
     *
     * @return string
     */
    public function getAmountPayment()
    {
        return $this->amountPayment;
    }

    /**
     * Set referencePayment
     *
     * @param string $referencePayment
     *
     * @return TbPayment
     */
    public function setReferencePayment($referencePayment)
    {
        $this->referencePayment = $referencePayment;

        return $this;
    }

    /**
     * Get referencePayment
     *
     * @return string
     */
    public function getReferencePayment()
    {
        return $this->referencePayment;
    }

    /**
     * Set descriptionPayment
     *
     * @param string $descriptionPayment
     *
     * @return TbPayment
     */
    public function setDescriptionPayment($descriptionPayment)
    {
        $this->descriptionPayment = $descriptionPayment;

        return $this;
    }

    /**
     * Get descriptionPayment
     *
     * @return string
     */
    public function getDescriptionPayment()
    {
        return $this->descriptionPayment;
    }

    /**
     * Set validPayment
     *
     * @param boolean $validPayment
     *
     * @return TbPayment
     */
    public function setValidPayment($validPayment)
    {
        $this->validPayment = $validPayment;

        return $this;
    }

    /**
     * Get validPayment
     *
     * @return boolean
     */
    public function getValidPayment()
    {
        return $this->validPayment;
    }

    /**
     * Set createdPayment
     *
     * @param \DateTime $createdPayment
     *
     * @return TbPayment
     */
    public function setCreatedPayment($createdPayment)
    {
        $this->createdPayment = $createdPayment;

        return $this;
    }

    /**
     * Get createdPayment
     *
     * @return \DateTime
     */
    public function getCreatedPayment()
    {
        return $this->createdPayment;
    }

    /**
     * Set fkChat
     *
     * @param \DbBundle\Entity\TbChat $fkChat
     *
     * @return TbPayment
     */
    public function setFkChat(\DbBundle\Entity\TbChat $fkChat = null)
    {
        $this->fkChat = $fkChat;

        return $this;
    }

    /**
     * Get fkChat
     *
     * @return \DbBundle\Entity\TbChat
     */
    public function getFkChat()
    {
        return $this->fkChat;
    }

    /**
     * Set fkUser
     *
     * @param \DbBundle\Entity\TbUser $fkUser
     *
     * @return TbPayment
     */
    public function setFkUser(\DbBundle\Entity\TbUser $fkUser = null)
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    /**
     * Get fkUser
     *
     * @return \DbBundle\Entity\TbUser
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }

    /**
     * Set fkTypePayment
     *
     * @param \DbBundle\Entity\TbTypePayment $fkTypePayment
     *
     * @return TbPayment
     */
    public function setFkTypePayment(\DbBundle\Entity\TbTypePayment $fkTypePayment = null)
    {
        $this->fkTypePayment = $fkTypePayment;

        return $this;
    }

    /**
     * Get fkTypePayment
     *
     * @return \DbBundle\Entity\TbTypePayment
     */
    public function getFkTypePayment()
    {
        return $this->fkTypePayment;
    }

    /**
     * Set fkServicePayment
     *
     * @param \DbBundle\Entity\TbServicePayment $fkServicePayment
     *
     * @return TbPayment
     */
    public function setFkServicePayment(\DbBundle\Entity\TbServicePayment $fkServicePayment = null)
    {
        $this->fkServicePayment = $fkServicePayment;

        return $this;
    }

    /**
     * Get fkServicePayment
     *
     * @return \DbBundle\Entity\TbServicePayment
     */
    public function getFkServicePayment()
    {
        return $this->fkServicePayment;
    }
}

