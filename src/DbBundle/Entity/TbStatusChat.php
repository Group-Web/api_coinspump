<?php

namespace DbBundle\Entity;

/**
 * TbStatusChat
 */
class TbStatusChat
{
    /**
     * @var integer
     */
    private $idStatusChat;

    /**
     * @var string
     */
    private $nameStatusChat;

    /**
     * @var string
     */
    private $descriptionStatusChat;

    /**
     * @var boolean
     */
    private $statusStatusChat = '1';


    /**
     * Get idStatusChat
     *
     * @return integer
     */
    public function getIdStatusChat()
    {
        return $this->idStatusChat;
    }

    /**
     * Set nameStatusChat
     *
     * @param string $nameStatusChat
     *
     * @return TbStatusChat
     */
    public function setNameStatusChat($nameStatusChat)
    {
        $this->nameStatusChat = $nameStatusChat;

        return $this;
    }

    /**
     * Get nameStatusChat
     *
     * @return string
     */
    public function getNameStatusChat()
    {
        return $this->nameStatusChat;
    }

    /**
     * Set descriptionStatusChat
     *
     * @param string $descriptionStatusChat
     *
     * @return TbStatusChat
     */
    public function setDescriptionStatusChat($descriptionStatusChat)
    {
        $this->descriptionStatusChat = $descriptionStatusChat;

        return $this;
    }

    /**
     * Get descriptionStatusChat
     *
     * @return string
     */
    public function getDescriptionStatusChat()
    {
        return $this->descriptionStatusChat;
    }

    /**
     * Set statusStatusChat
     *
     * @param boolean $statusStatusChat
     *
     * @return TbStatusChat
     */
    public function setStatusStatusChat($statusStatusChat)
    {
        $this->statusStatusChat = $statusStatusChat;

        return $this;
    }

    /**
     * Get statusStatusChat
     *
     * @return boolean
     */
    public function getStatusStatusChat()
    {
        return $this->statusStatusChat;
    }
}

