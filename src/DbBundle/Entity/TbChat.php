<?php

namespace DbBundle\Entity;

/**
 * TbChat
 */
class TbChat
{
    /**
     * @var integer
     */
    private $idChat;

    /**
     * @var string
     */
    private $nameChat;

    /**
     * @var \DateTime
     */
    private $startDateChat;

    /**
     * @var \DateTime
     */
    private $endDateChat;

    /**
     * @var string
     */
    private $descriptionChat;

    /**
     * @var string
     */
    private $conclusionChat;

    /**
     * @var \DateTime
     */
    private $createdChat = 'CURRENT_TIMESTAMP';

    /**
     * @var boolean
     */
    private $validChat = '0';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvChat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvPriceChat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvChatPay;

    /**
     * @var \DbBundle\Entity\TbCryptocurrency
     */
    private $fkCryptocurrency;

    /**
     * @var \DbBundle\Entity\TbStatusChat
     */
    private $fkStatusChat;

    /**
     * @var \DbBundle\Entity\TbTypeChat
     */
    private $fkTypeChat;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fkInvChat = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fkInvPriceChat = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fkInvChatPay = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idChat
     *
     * @return integer
     */
    public function getIdChat()
    {
        return $this->idChat;
    }

    /**
     * Set nameChat
     *
     * @param string $nameChat
     *
     * @return TbChat
     */
    public function setNameChat($nameChat)
    {
        $this->nameChat = $nameChat;

        return $this;
    }

    /**
     * Get nameChat
     *
     * @return string
     */
    public function getNameChat()
    {
        return $this->nameChat;
    }

    /**
     * Set startDateChat
     *
     * @param \DateTime $startDateChat
     *
     * @return TbChat
     */
    public function setStartDateChat($startDateChat)
    {
        $this->startDateChat = $startDateChat;

        return $this;
    }

    /**
     * Get startDateChat
     *
     * @return \DateTime
     */
    public function getStartDateChat()
    {
        return $this->startDateChat;
    }

    /**
     * Set endDateChat
     *
     * @param \DateTime $endDateChat
     *
     * @return TbChat
     */
    public function setEndDateChat($endDateChat)
    {
        $this->endDateChat = $endDateChat;

        return $this;
    }

    /**
     * Get endDateChat
     *
     * @return \DateTime
     */
    public function getEndDateChat()
    {
        return $this->endDateChat;
    }

    /**
     * Set descriptionChat
     *
     * @param string $descriptionChat
     *
     * @return TbChat
     */
    public function setDescriptionChat($descriptionChat)
    {
        $this->descriptionChat = $descriptionChat;

        return $this;
    }

    /**
     * Get descriptionChat
     *
     * @return string
     */
    public function getDescriptionChat()
    {
        return $this->descriptionChat;
    }

    /**
     * Set conclusionChat
     *
     * @param string $conclusionChat
     *
     * @return TbChat
     */
    public function setConclusionChat($conclusionChat)
    {
        $this->conclusionChat = $conclusionChat;

        return $this;
    }

    /**
     * Get conclusionChat
     *
     * @return string
     */
    public function getConclusionChat()
    {
        return $this->conclusionChat;
    }

    /**
     * Set createdChat
     *
     * @param \DateTime $createdChat
     *
     * @return TbChat
     */
    public function setCreatedChat($createdChat)
    {
        $this->createdChat = $createdChat;

        return $this;
    }

    /**
     * Get createdChat
     *
     * @return \DateTime
     */
    public function getCreatedChat()
    {
        return $this->createdChat;
    }

    /**
     * Set validChat
     *
     * @param boolean $validChat
     *
     * @return TbChat
     */
    public function setValidChat($validChat)
    {
        $this->validChat = $validChat;

        return $this;
    }

    /**
     * Get validChat
     *
     * @return boolean
     */
    public function getValidChat()
    {
        return $this->validChat;
    }

    /**
     * Add fkInvChat
     *
     * @param \DbBundle\Entity\TbChatUser $fkInvChat
     *
     * @return TbChat
     */
    public function addFkInvChat(\DbBundle\Entity\TbChatUser $fkInvChat)
    {
        $this->fkInvChat[] = $fkInvChat;

        return $this;
    }

    /**
     * Remove fkInvChat
     *
     * @param \DbBundle\Entity\TbChatUser $fkInvChat
     */
    public function removeFkInvChat(\DbBundle\Entity\TbChatUser $fkInvChat)
    {
        $this->fkInvChat->removeElement($fkInvChat);
    }

    /**
     * Get fkInvChat
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvChat()
    {
        return $this->fkInvChat;
    }

    /**
     * Add fkInvPriceChat
     *
     * @param \DbBundle\Entity\TbPriceChat $fkInvPriceChat
     *
     * @return TbChat
     */
    public function addFkInvPriceChat(\DbBundle\Entity\TbPriceChat $fkInvPriceChat)
    {
        $this->fkInvPriceChat[] = $fkInvPriceChat;

        return $this;
    }

    /**
     * Remove fkInvPriceChat
     *
     * @param \DbBundle\Entity\TbPriceChat $fkInvPriceChat
     */
    public function removeFkInvPriceChat(\DbBundle\Entity\TbPriceChat $fkInvPriceChat)
    {
        $this->fkInvPriceChat->removeElement($fkInvPriceChat);
    }

    /**
     * Get fkInvPriceChat
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvPriceChat()
    {
        return $this->fkInvPriceChat;
    }

    /**
     * Add fkInvChatPay
     *
     * @param \DbBundle\Entity\TbPayment $fkInvChatPay
     *
     * @return TbChat
     */
    public function addFkInvChatPay(\DbBundle\Entity\TbPayment $fkInvChatPay)
    {
        $this->fkInvChatPay[] = $fkInvChatPay;

        return $this;
    }

    /**
     * Remove fkInvChatPay
     *
     * @param \DbBundle\Entity\TbPayment $fkInvChatPay
     */
    public function removeFkInvChatPay(\DbBundle\Entity\TbPayment $fkInvChatPay)
    {
        $this->fkInvChatPay->removeElement($fkInvChatPay);
    }

    /**
     * Get fkInvChatPay
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvChatPay()
    {
        return $this->fkInvChatPay;
    }

    /**
     * Set fkCryptocurrency
     *
     * @param \DbBundle\Entity\TbCryptocurrency $fkCryptocurrency
     *
     * @return TbChat
     */
    public function setFkCryptocurrency(\DbBundle\Entity\TbCryptocurrency $fkCryptocurrency = null)
    {
        $this->fkCryptocurrency = $fkCryptocurrency;

        return $this;
    }

    /**
     * Get fkCryptocurrency
     *
     * @return \DbBundle\Entity\TbCryptocurrency
     */
    public function getFkCryptocurrency()
    {
        return $this->fkCryptocurrency;
    }

    /**
     * Set fkStatusChat
     *
     * @param \DbBundle\Entity\TbStatusChat $fkStatusChat
     *
     * @return TbChat
     */
    public function setFkStatusChat(\DbBundle\Entity\TbStatusChat $fkStatusChat = null)
    {
        $this->fkStatusChat = $fkStatusChat;

        return $this;
    }

    /**
     * Get fkStatusChat
     *
     * @return \DbBundle\Entity\TbStatusChat
     */
    public function getFkStatusChat()
    {
        return $this->fkStatusChat;
    }

    /**
     * Set fkTypeChat
     *
     * @param \DbBundle\Entity\TbTypeChat $fkTypeChat
     *
     * @return TbChat
     */
    public function setFkTypeChat(\DbBundle\Entity\TbTypeChat $fkTypeChat = null)
    {
        $this->fkTypeChat = $fkTypeChat;

        return $this;
    }

    /**
     * Get fkTypeChat
     *
     * @return \DbBundle\Entity\TbTypeChat
     */
    public function getFkTypeChat()
    {
        return $this->fkTypeChat;
    }
}

