<?php

namespace DbBundle\Entity;

/**
 * TbGender
 */
class TbGender
{
    /**
     * @var integer
     */
    private $idGender;

    /**
     * @var string
     */
    private $nameGender;

    /**
     * @var boolean
     */
    private $statusGender;


    /**
     * Get idGender
     *
     * @return integer
     */
    public function getIdGender()
    {
        return $this->idGender;
    }

    /**
     * Set nameGender
     *
     * @param string $nameGender
     *
     * @return TbGender
     */
    public function setNameGender($nameGender)
    {
        $this->nameGender = $nameGender;

        return $this;
    }

    /**
     * Get nameGender
     *
     * @return string
     */
    public function getNameGender()
    {
        return $this->nameGender;
    }

    /**
     * Set statusGender
     *
     * @param boolean $statusGender
     *
     * @return TbGender
     */
    public function setStatusGender($statusGender)
    {
        $this->statusGender = $statusGender;

        return $this;
    }

    /**
     * Get statusGender
     *
     * @return boolean
     */
    public function getStatusGender()
    {
        return $this->statusGender;
    }
}

