<?php

namespace DbBundle\Entity;

/**
 * TbUser
 */
class TbUser
{
    /**
     * @var integer
     */
    private $idUser;

    /**
     * @var string
     */
    private $nameUser;

    /**
     * @var string
     */
    private $lastnameUser;

    /**
     * @var string
     */
    private $emailUser;

    /**
     * @var string
     */
    private $loginUser;

    /**
     * @var string
     */
    private $passwordUser;

    /**
     * @var string
     */
    private $addressUser;

    /**
     * @var string
     */
    private $celUser;

    /**
     * @var string
     */
    private $phoneUser;

    /**
     * @var \DateTime
     */
    private $birthdayUser;

    /**
     * @var boolean
     */
    private $validUser = '0';

    /**
     * @var string
     */
    private $codeUser;

    /**
     * @var \DateTime
     */
    private $createdUser = 'CURRENT_TIMESTAMP';

    /**
     * @var boolean
     */
    private $statusUser = '1';

    /**
     * @var integer
     */
    private $referenceUser;

    /**
     * @var string
     */
    private $descriptionUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvUserBan;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvUserPay;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fkInvUserVideo;

    /**
     * @var \DbBundle\Entity\TbCountry
     */
    private $fkCountry;

    /**
     * @var \DbBundle\Entity\TbGender
     */
    private $fkGender;

    /**
     * @var \DbBundle\Entity\TbRole
     */
    private $fkRole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fkInvUserBan = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fkInvUserPay = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fkInvUserVideo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set nameUser
     *
     * @param string $nameUser
     *
     * @return TbUser
     */
    public function setNameUser($nameUser)
    {
        $this->nameUser = $nameUser;

        return $this;
    }

    /**
     * Get nameUser
     *
     * @return string
     */
    public function getNameUser()
    {
        return $this->nameUser;
    }

    /**
     * Set lastnameUser
     *
     * @param string $lastnameUser
     *
     * @return TbUser
     */
    public function setLastnameUser($lastnameUser)
    {
        $this->lastnameUser = $lastnameUser;

        return $this;
    }

    /**
     * Get lastnameUser
     *
     * @return string
     */
    public function getLastnameUser()
    {
        return $this->lastnameUser;
    }

    /**
     * Set emailUser
     *
     * @param string $emailUser
     *
     * @return TbUser
     */
    public function setEmailUser($emailUser)
    {
        $this->emailUser = $emailUser;

        return $this;
    }

    /**
     * Get emailUser
     *
     * @return string
     */
    public function getEmailUser()
    {
        return $this->emailUser;
    }

    /**
     * Set loginUser
     *
     * @param string $loginUser
     *
     * @return TbUser
     */
    public function setLoginUser($loginUser)
    {
        $this->loginUser = $loginUser;

        return $this;
    }

    /**
     * Get loginUser
     *
     * @return string
     */
    public function getLoginUser()
    {
        return $this->loginUser;
    }

    /**
     * Set passwordUser
     *
     * @param string $passwordUser
     *
     * @return TbUser
     */
    public function setPasswordUser($passwordUser)
    {
        $this->passwordUser = $passwordUser;

        return $this;
    }

    /**
     * Get passwordUser
     *
     * @return string
     */
    public function getPasswordUser()
    {
        return $this->passwordUser;
    }

    /**
     * Set addressUser
     *
     * @param string $addressUser
     *
     * @return TbUser
     */
    public function setAddressUser($addressUser)
    {
        $this->addressUser = $addressUser;

        return $this;
    }

    /**
     * Get addressUser
     *
     * @return string
     */
    public function getAddressUser()
    {
        return $this->addressUser;
    }

    /**
     * Set celUser
     *
     * @param string $celUser
     *
     * @return TbUser
     */
    public function setCelUser($celUser)
    {
        $this->celUser = $celUser;

        return $this;
    }

    /**
     * Get celUser
     *
     * @return string
     */
    public function getCelUser()
    {
        return $this->celUser;
    }

    /**
     * Set phoneUser
     *
     * @param string $phoneUser
     *
     * @return TbUser
     */
    public function setPhoneUser($phoneUser)
    {
        $this->phoneUser = $phoneUser;

        return $this;
    }

    /**
     * Get phoneUser
     *
     * @return string
     */
    public function getPhoneUser()
    {
        return $this->phoneUser;
    }

    /**
     * Set birthdayUser
     *
     * @param \DateTime $birthdayUser
     *
     * @return TbUser
     */
    public function setBirthdayUser($birthdayUser)
    {
        $this->birthdayUser = $birthdayUser;

        return $this;
    }

    /**
     * Get birthdayUser
     *
     * @return \DateTime
     */
    public function getBirthdayUser()
    {
        return $this->birthdayUser;
    }

    /**
     * Set validUser
     *
     * @param boolean $validUser
     *
     * @return TbUser
     */
    public function setValidUser($validUser)
    {
        $this->validUser = $validUser;

        return $this;
    }

    /**
     * Get validUser
     *
     * @return boolean
     */
    public function getValidUser()
    {
        return $this->validUser;
    }

    /**
     * Set codeUser
     *
     * @param string $codeUser
     *
     * @return TbUser
     */
    public function setCodeUser($codeUser)
    {
        $this->codeUser = $codeUser;

        return $this;
    }

    /**
     * Get codeUser
     *
     * @return string
     */
    public function getCodeUser()
    {
        return $this->codeUser;
    }

    /**
     * Set createdUser
     *
     * @param \DateTime $createdUser
     *
     * @return TbUser
     */
    public function setCreatedUser($createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \DateTime
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set statusUser
     *
     * @param boolean $statusUser
     *
     * @return TbUser
     */
    public function setStatusUser($statusUser)
    {
        $this->statusUser = $statusUser;

        return $this;
    }

    /**
     * Get statusUser
     *
     * @return boolean
     */
    public function getStatusUser()
    {
        return $this->statusUser;
    }

    /**
     * Set referenceUser
     *
     * @param integer $referenceUser
     *
     * @return TbUser
     */
    public function setReferenceUser($referenceUser)
    {
        $this->referenceUser = $referenceUser;

        return $this;
    }

    /**
     * Get referenceUser
     *
     * @return integer
     */
    public function getReferenceUser()
    {
        return $this->referenceUser;
    }

    /**
     * Set descriptionUser
     *
     * @param string $descriptionUser
     *
     * @return TbUser
     */
    public function setDescriptionUser($descriptionUser)
    {
        $this->descriptionUser = $descriptionUser;

        return $this;
    }

    /**
     * Get descriptionUser
     *
     * @return string
     */
    public function getDescriptionUser()
    {
        return $this->descriptionUser;
    }

    /**
     * Add fkInvUserBan
     *
     * @param \DbBundle\Entity\TbBan $fkInvUserBan
     *
     * @return TbUser
     */
    public function addFkInvUserBan(\DbBundle\Entity\TbBan $fkInvUserBan)
    {
        $this->fkInvUserBan[] = $fkInvUserBan;

        return $this;
    }

    /**
     * Remove fkInvUserBan
     *
     * @param \DbBundle\Entity\TbBan $fkInvUserBan
     */
    public function removeFkInvUserBan(\DbBundle\Entity\TbBan $fkInvUserBan)
    {
        $this->fkInvUserBan->removeElement($fkInvUserBan);
    }

    /**
     * Get fkInvUserBan
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvUserBan()
    {
        return $this->fkInvUserBan;
    }

    /**
     * Add fkInvUserPay
     *
     * @param \DbBundle\Entity\TbPayment $fkInvUserPay
     *
     * @return TbUser
     */
    public function addFkInvUserPay(\DbBundle\Entity\TbPayment $fkInvUserPay)
    {
        $this->fkInvUserPay[] = $fkInvUserPay;

        return $this;
    }

    /**
     * Remove fkInvUserPay
     *
     * @param \DbBundle\Entity\TbPayment $fkInvUserPay
     */
    public function removeFkInvUserPay(\DbBundle\Entity\TbPayment $fkInvUserPay)
    {
        $this->fkInvUserPay->removeElement($fkInvUserPay);
    }

    /**
     * Get fkInvUserPay
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvUserPay()
    {
        return $this->fkInvUserPay;
    }

    /**
     * Add fkInvUserVideo
     *
     * @param \DbBundle\Entity\TbVideoUser $fkInvUserVideo
     *
     * @return TbUser
     */
    public function addFkInvUserVideo(\DbBundle\Entity\TbVideoUser $fkInvUserVideo)
    {
        $this->fkInvUserVideo[] = $fkInvUserVideo;

        return $this;
    }

    /**
     * Remove fkInvUserVideo
     *
     * @param \DbBundle\Entity\TbVideoUser $fkInvUserVideo
     */
    public function removeFkInvUserVideo(\DbBundle\Entity\TbVideoUser $fkInvUserVideo)
    {
        $this->fkInvUserVideo->removeElement($fkInvUserVideo);
    }

    /**
     * Get fkInvUserVideo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFkInvUserVideo()
    {
        return $this->fkInvUserVideo;
    }

    /**
     * Set fkCountry
     *
     * @param \DbBundle\Entity\TbCountry $fkCountry
     *
     * @return TbUser
     */
    public function setFkCountry(\DbBundle\Entity\TbCountry $fkCountry = null)
    {
        $this->fkCountry = $fkCountry;

        return $this;
    }

    /**
     * Get fkCountry
     *
     * @return \DbBundle\Entity\TbCountry
     */
    public function getFkCountry()
    {
        return $this->fkCountry;
    }

    /**
     * Set fkGender
     *
     * @param \DbBundle\Entity\TbGender $fkGender
     *
     * @return TbUser
     */
    public function setFkGender(\DbBundle\Entity\TbGender $fkGender = null)
    {
        $this->fkGender = $fkGender;

        return $this;
    }

    /**
     * Get fkGender
     *
     * @return \DbBundle\Entity\TbGender
     */
    public function getFkGender()
    {
        return $this->fkGender;
    }

    /**
     * Set fkRole
     *
     * @param \DbBundle\Entity\TbRole $fkRole
     *
     * @return TbUser
     */
    public function setFkRole(\DbBundle\Entity\TbRole $fkRole = null)
    {
        $this->fkRole = $fkRole;

        return $this;
    }

    /**
     * Get fkRole
     *
     * @return \DbBundle\Entity\TbRole
     */
    public function getFkRole()
    {
        return $this->fkRole;
    }
}

